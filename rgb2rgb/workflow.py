"""Train dense features using color information only.
"""

import rflow
# Hack: prevent segfault with SummaryWriter
import pytorch_lightning as pl

from slamfeat.train import TrainConfig

# pylint: disable=too-many-statements


@rflow.graph()
def val_data(g):
    """Prepare the data for the other training workflows.
    """
    from slamfeat.data.nodes import (
        CreateCorrespondenceDataset, SaveDatasetToDB)
    from slamfeat.viz.nodes import ViewCorrespondenceDataset

    scenenn_g = rflow.open_graph("../data/scenenn", "scenenn")

    g.val_dataset = CreateCorrespondenceDataset()
    with g.val_dataset as args:
        args.point_dist_thresh = 0.003
        args.ds1 = scenenn_g["045.dataset"]
        args.ds1_vis_graph = scenenn_g["045.visibility_graph"]

    g.view_val_dataset = ViewCorrespondenceDataset()
    with g.view_val_dataset as args:
        args.dataset = g.val_dataset

    g.val_cache = SaveDatasetToDB(rflow.FSResource("val_cache.lmdb"))
    with g.val_cache as args:
        args.dataset = g.val_dataset
        args.num_workers = 0
        args.num_samples = 200


def _create_syn_train_dataset():
    from slamfeat.data.nodes import CreateCorrespondenceDataset

    replica_g = rflow.open_graph("../data/replica", "replica")
    train_dataset = CreateCorrespondenceDataset()

    with train_dataset as args:
        args.point_dist_thresh = 0.003

        args.ds1 = replica_g["apartment_0.dataset"]
        args.ds1_bg_ids = replica_g["apartment_0.bg_ids"]
        args.ds1_vis_graph = replica_g["apartment_0.visibility_graph"]

        args.ds2 = replica_g["hotel_0.dataset"]
        args.ds2_bg_ids = replica_g["hotel_0.bg_ids"]
        args.ds2_vis_graph = replica_g["hotel_0.visibility_graph"]

    return train_dataset


def _create_small_train_dataset():
    from slamfeat.data.nodes import CreateCorrespondenceDataset

    scenenn_g = rflow.open_graph("../data/scenenn", "scenenn")

    train_dataset = CreateCorrespondenceDataset()
    with train_dataset as args:
        args.point_dist_thresh = 0.003

        args.ds1 = scenenn_g["016.dataset"]
        args.ds1_bg_ids = scenenn_g["016.bg_ids"]
        args.ds1_vis_graph = scenenn_g["016.visibility_graph"]

    return train_dataset


def _create_real_train_dataset():
    from slamfeat.data.nodes import CreateCorrespondenceDataset

    scenenn_g = rflow.open_graph("../data/scenenn", "scenenn")

    train_dataset = CreateCorrespondenceDataset()
    with train_dataset as args:
        args.point_dist_thresh = 0.003

        # args.ds1 = scenenn_g["016.dataset"]
        # args.ds1_bg_ids = scenenn_g["016.bg_ids"]
        # args.ds1_vis_graph = scenenn_g["016.visibility_graph"]

        args.ds2 = scenenn_g["021.dataset"]
        args.ds2_bg_ids = scenenn_g["021.bg_ids"]
        args.ds2_vis_graph = scenenn_g["021.visibility_graph"]

        # Too big visibility_graph graph !
        # args.ds3 = scenenn_g["087.dataset"]
        # args.ds3_bg_ids = scenenn_g["087.bg_ids"]
        # args.ds3_vis_graph = scenenn_g["087.visibility_graph"]

        args.ds4 = scenenn_g["030.dataset"]
        args.ds4_bg_ids = scenenn_g["030.bg_ids"]
        args.ds4_vis_graph = scenenn_g["030.visibility_graph"]

        args.ds5 = scenenn_g["032.dataset"]
        args.ds5_bg_ids = scenenn_g["032.bg_ids"]
        args.ds5_vis_graph = scenenn_g["032.visibility_graph"]

        #args.ds6 = scenenn_g["621.dataset"]
        #args.ds6_bg_ids = scenenn_g["621.bg_ids"]
        #args.ds6_vis_graph = scenenn_g["621.visibility_graph"]

        args.ds7 = scenenn_g["623.dataset"]
        args.ds7_bg_ids = scenenn_g["623.bg_ids"]
        args.ds7_vis_graph = scenenn_g["623.visibility_graph"]

        args.ds8 = scenenn_g["700.dataset"]
        args.ds8_bg_ids = scenenn_g["700.bg_ids"]
        args.ds8_vis_graph = scenenn_g["700.visibility_graph"]

    return train_dataset


def _create_train_graph(g, model, train_config, train_dataset, num_train_samples=5000):
    from slamfeat.metrics.nodes import (EvaluateFeatureMatching,
                                        EvaluateFeatureDistance)
    from slamfeat.viz.nodes import (
        ViewFeatures, ViewFeatureDifferences,
        ViewCorrespondenceDataset)
    from slamfeat.train.nodes import ContrastiveTrain

    val_data_g = rflow.open_graph(".", "val_data")
    match_data_g = rflow.open_graph("../exps/matching", "data")
    ilrgbd_g = rflow.open_graph("../data/ilrgbd", "ilrgbd")

    g.view_train_dataset = ViewCorrespondenceDataset()
    with g.view_train_dataset as args:
        args.dataset = g.train_dataset

    g.train = ContrastiveTrain(rflow.FSResource(f"{g.name}.torch"))
    with g.train as args:
        args.model = model
        args.train_config = train_config
        args.train_dataset = train_dataset
        args.val_dataset = val_data_g.val_dataset
        args.num_workers = 12
        args.num_train_samples = num_train_samples
        args.num_val_samples = 50
        args.fast_dev_run = False

    scenenn_g = rflow.open_graph("../data/scenenn", "scenenn")
    # test_scene = scenenn_g["045.dataset"]
    test_scene = ilrgbd_g["apartment.dataset"]

    inspect_frames = [1902, 2002]
    for prefix, curr_model in [("untrain", model),
                               ("train", g.train)]:
        with g.prefix(prefix + '.') as sub:
            sub.viz_feats = ViewFeatures()
            with sub.viz_feats as args:
                args.dataset = test_scene
                args.model = curr_model
                args.frames = inspect_frames
                args.num_pca_components = 3
                args.num_color_clusters = None
                args.title = f"{prefix} features of {g.name}"

            sub.viz_tsne = ViewFeatures()
            with sub.viz_tsne as args:
                args.dataset = test_scene
                args.model = curr_model
                args.frames = inspect_frames
                args.num_pca_components = None
                args.num_tsne_components = None
                args.num_color_clusters = 10
                args.title = f"Features after training {g.name}"

            sub.viz_diff = ViewFeatureDifferences()
            with sub.viz_diff as args:
                args.dataset = test_scene
                args.model = curr_model
                args.frames = inspect_frames
                args.title = f"{prefix} feature differences of {g.name}"

            sub.matching_metrics = EvaluateFeatureMatching(
                rflow.FSResource(f"{g.name}-{prefix}-match-metrics.jpg"))
            with sub.matching_metrics as args:
                args.test_dataset = match_data_g.test_dataset
                args.feature_extractor = curr_model
                args.alg_name = f"{prefix}-{g.name}"

            sub.distance_metrics = EvaluateFeatureDistance(
                rflow.FSResource(f"{g.name}-{prefix}-dist-metrics.jpg"))
            with sub.distance_metrics as args:
                args.test_dataset = match_data_g.test_dataset
                args.feature_extractor = curr_model
                args.alg_name = f"{prefix}-{g.name}"


@rflow.graph()
def rnd_syn_3(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=3, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_syn_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=4000)


@rflow.graph()
def rnd_real_3(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=3, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_small_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=2000)


@rflow.graph()
def rnd_syn_8(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=8, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_syn_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            30, 1, 1e-2, [5, 10, 15, 25], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=3000)


@rflow.graph()
def rnd_real_8(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=8, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_real_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=4000)


@rflow.graph()
def rnd32s_real_8(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=8, resnet_kind="Resnet34_32s",
                                   pretrained=True)

    exp.train_dataset = _create_real_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=4000)


@rflow.graph()
def rnd_real_16(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=16, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_real_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=4000)


@rflow.graph()
def rnd_real_32(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=32, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_real_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=4000)


@rflow.graph()
def rnd_real_64(exp):
    """Resnet Dilated training with synthetic and 3 channels
    """
    from slamfeat.model.rgbsegmentation import DilatedResnet

    exp.model = rflow.make_factory(DilatedResnet, feature_size=64, resnet_kind="Resnet34_8s",
                                   pretrained=True)

    exp.train_dataset = _create_real_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            50, 1, 1e-2, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=4000)


@rflow.graph()
def unet32(exp):
    from slamfeat.model.rgbsegmentation import SMP

    exp.model = rflow.make_factory(SMP, net_type="Unet",
                                   feature_size=32, encoder_name="vgg11",
                                   encoder_weights="imagenet")

    exp.train_dataset = _create_real_train_dataset()

    _create_train_graph(exp, exp.model,
                        TrainConfig(
                            30, 1, 1e-4, [10, 25, 45, 70, 90], 1e-4,
                            loss_func='contrastive'),
                        exp.train_dataset,
                        num_train_samples=1000)
