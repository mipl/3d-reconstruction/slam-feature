#include <limits>
#include <vector>

#include <torch/torch.h>

#include "slamfeat.hpp"

namespace slamfeat {
void AccuracyImpl(const torch::TensorAccessor<float, 2> &feats_a,
                  const torch::TensorAccessor<float, 2> &feats_b,
                  const torch::TensorAccessor<int64_t, 2> &nn_index,
                  torch::TensorAccessor<int32_t, 1> accuracy) {
  for (int b_index = 0; b_index < nn_index.size(0); ++b_index) {
    std::vector<float> distances;
    distances.reserve(nn_index.size(1));

    for (int i = 0; i < nn_index.size(1); ++i) {
      const int a_index = nn_index[b_index][i];
      if (a_index == feats_a.size(1)) {
        break;
      }
      
      float dist = 0.0f;
      for (int k = 0; k < feats_a.size(0); ++k) {
        const float diff = feats_a[k][a_index] - feats_b[k][b_index];
        dist += diff * diff;
      }
      distances.push_back(std::sqrt(dist));
    }

    if (distances.size() < 2) {
      accuracy[b_index] = -1;
      continue;
    }
      
    const size_t arg_min =
        std::distance(distances.begin(),
                      std::min_element(distances.begin(), distances.end()));
    accuracy[b_index] = 1 ? arg_min == 0 : 0;
  }
}

void SlamFeatOp::NNFeatureAccuracy(const torch::Tensor &feats_a,
                                   const torch::Tensor &feats_b,
                                   const torch::Tensor &nn_index,
                                   torch::Tensor accuracy) {
  AccuracyImpl(feats_a.accessor<float, 2>(), feats_b.accessor<float, 2>(),
               nn_index.accessor<int64_t, 2>(),
               accuracy.accessor<int32_t, 1>());
}
}  // namespace slamfeat
