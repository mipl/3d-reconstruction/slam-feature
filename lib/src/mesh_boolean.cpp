/**
 * Wrapper around CGAL for boolean mesh operators
 */

#include <torch/torch.h>

#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include <CGAL/Surface_mesh.h>

#include "slamfeat.hpp"

namespace slamfeat {
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef Kernel::Point_3 Point;
typedef CGAL::Surface_mesh<Point> Mesh;

namespace {
void ConvertTorchToCGAL(const torch::Tensor &verts_tensor,
                        const torch::Tensor &trigs_tensor, Mesh &mesh) {
  const auto verts = verts_tensor.accessor<float, 2>();
  const auto trigs = trigs_tensor.accessor<int32_t, 2>();
  mesh.reserve(verts.size(0), 0, trigs.size(0));
  for (int i = 0; i < verts.size(0); ++i) {
    mesh.add_vertex(Point(verts[i][0], verts[i][1], verts[i][2]));
  }

  for (int i = 0; i < trigs.size(0); ++i) {
    mesh.add_face(Mesh::Vertex_index(trigs[i][0]),
                  Mesh::Vertex_index(trigs[i][1]),
                  Mesh::Vertex_index(trigs[i][2]));
  }
}

void ConvertCGALToTorch(const Mesh &mesh, torch::Tensor &verts,
                        torch::Tensor &trigs) {
  verts = torch::empty({mesh.number_of_vertices(), 3}, torch::kFloat);
  auto verts_acc = verts.accessor<float, 2>();

  std::vector<int> reindex;
  reindex.resize(mesh.num_vertices());
  int n = 0;
  BOOST_FOREACH (Mesh::Vertex_index v, mesh.vertices()) {
    const auto &point = mesh.point(Mesh::Vertex_index(v));
    verts_acc[v][0] = point.x();
    verts_acc[v][1] = point.y();
    verts_acc[v][2] = point.z();

    reindex[v] = n++;
  }


  trigs = torch::empty({mesh.number_of_faces(), 3}, torch::kInt32);
#if 1
  auto trigs_acc = trigs.accessor<int32_t, 2>();
  BOOST_FOREACH (Mesh::Face_index f, mesh.faces()) {
    int degree = mesh.degree(f);
    int j = 0;
    BOOST_FOREACH (Mesh::Vertex_index v,
                   CGAL::vertices_around_face(mesh.halfedge(f), mesh)) {
      trigs_acc[f][j++] = reindex[v];
    }
  }
#endif
}
}  // namespace

std::pair<torch::Tensor, torch::Tensor> SlamFeatOp::MeshIntersection(
    const torch::Tensor &a_verts, const torch::Tensor &a_trigs,
    const torch::Tensor &b_verts, const torch::Tensor &b_trigs) {
  namespace PMP = CGAL::Polygon_mesh_processing;

  Mesh a_mesh, b_mesh, out_mesh;
  ConvertTorchToCGAL(a_verts, a_trigs, a_mesh);
  ConvertTorchToCGAL(b_verts, b_trigs, b_mesh);

  PMP::corefine_and_compute_intersection(a_mesh, b_mesh, out_mesh);

  torch::Tensor out_verts, out_trigs;
  ConvertCGALToTorch(out_mesh, out_verts, out_trigs);

  return std::pair<torch::Tensor, torch::Tensor>(out_verts, out_trigs);
}

}  // namespace slamfeat
