#include "slamfeat.hpp"

#include <random>

namespace slamfeat {

struct PointCloudAccessor {
  torch::TensorAccessor<bool, 2> mask;
  torch::TensorAccessor<float, 3> points;
  torch::TensorAccessor<uint8_t, 3> colors;
  torch::TensorAccessor<int32_t, 2> depths;

  PointCloudAccessor(PointCloud pcl)
      : mask(pcl.mask.accessor<bool, 2>()),
        points(pcl.points.accessor<float, 3>()),
        colors(pcl.colors.accessor<uint8_t, 3>()),
        depths(pcl.depths.accessor<int32_t, 2>()) {}

  int get_width() const { return points.size(1); }

  int get_height() const { return points.size(0); }

  bool is_valid(int row, int col) const {
    if (row < 0 || row >= get_height() || col < 0 || col >= get_width())
      return false;

    return mask[row][col];
  }

  Eigen::Vector3f get_point(int row, int col) const {
    return Eigen::Vector3f(points[row][col][0], points[row][col][1],
                           points[row][col][2]);
  }

  Eigen::Vector3i get_color(int row, int col) const {
    return Eigen::Vector3i(colors[row][col][0], colors[row][col][1],
                           colors[row][col][2]);
  }

  int get_depth(int row, int col) const { return depths[row][col]; }
};

struct FrameCropAccessor {
  torch::TensorAccessor<uint8_t, 3> colors;
  torch::TensorAccessor<int32_t, 2> depths;

  FrameCropAccessor(FrameCrop crop)
      : colors(crop.colors.accessor<uint8_t, 3>()),
        depths(crop.depths.accessor<int32_t, 2>()) {}

  void set_color(int row, int col, const Eigen::Vector3i &color) {
    colors[row][col][0] = color[0];
    colors[row][col][1] = color[1];
    colors[row][col][2] = color[2];
  }

  void set_depth(int row, int col, int depth) { depths[row][col] = depth; }
};

namespace {
int rand_int(int start, int end) { return start + std::rand() % (end - start); }

int rand_int_except(int start, int end, int except) {
  while (true) {
    const int val = rand_int(start, end);
    if (val != except) return val;
  }
}

bool dice(float chance) {
  const double roll = double(rand() % 1000) / 1000.0;
  return roll < chance;
}

}  // namespace

namespace {
int ExtractIndexedTupleImpl(const PointCloudAccessor &anch_pcl,
                            const RigidTransform &anch_cam_to_world,
                            const PointCloudAccessor &posv_pcl,
                            const KCamera posv_kcam,
                            const RigidTransform &posv_cam_to_world,
                            const RigidTransform &posv_world_to_cam,
                            float point_dist_thresh,
                            torch::TensorAccessor<int64_t, 2> anch_pos_index,
                            torch::TensorAccessor<int64_t, 2> dock_pos_index,
                            torch::TensorAccessor<int64_t, 2> anch_neg_index,
                            torch::TensorAccessor<int64_t, 2> dock_neg_index) {
  int mask_count;
  for (int row = 0; row < anch_pcl.get_height(); ++row) {
    for (int col = 0; col < anch_pcl.get_width(); ++col) {
      anch_pos_index[row][col] = -1;
      dock_pos_index[row][col] = -1;
      anch_neg_index[row][col] = -1;
      dock_neg_index[row][col] = -1;

      if (!anch_pcl.is_valid(row, col)) {
        continue;
      }

      const Eigen::Vector3f anc_point(anch_pcl.get_point(row, col));
      const Eigen::Vector3f world_point(anch_cam_to_world.Transform(anc_point));
      const Eigen::Vector3f pred_pos_point(
          posv_world_to_cam.Transform(world_point));
      int u, v;
      posv_kcam.Projecti(pred_pos_point, u, v);
      if (posv_pcl.is_valid(v, u)) {
        const Eigen::Vector3f pos_point(
            posv_cam_to_world.Transform(posv_pcl.get_point(v, u)));
        const float sqr_point_dist = (pos_point - world_point).squaredNorm();

        int64_t anch_index = row * anch_pcl.get_width() + col;
        int64_t dock_index = v * posv_pcl.get_width() + u;

        if (sqr_point_dist <= point_dist_thresh * point_dist_thresh) {
          anch_pos_index[row][col] = anch_index;
          dock_pos_index[row][col] = dock_index;
          ++mask_count;
        } else {
          anch_neg_index[row][col] = anch_index;
          dock_neg_index[row][col] = dock_index;
        }
      }
    }
  }
  return mask_count;
}

}  // namespace

int SlamFeatOp::ExtractIndexedTuple(
    const PointCloud &anch_pcl, const torch::Tensor &anch_cam_to_world,
    const PointCloud &posv_pcl, const torch::Tensor &posv_kcam,
    const torch::Tensor &posv_cam_to_world, float point_dist_thresh,
    torch::Tensor anch_pos_index, torch::Tensor dock_pos_index,
    torch::Tensor anch_neg_index, torch::Tensor dock_neg_index) {
  return ExtractIndexedTupleImpl(
      PointCloudAccessor(anch_pcl), RigidTransform(anch_cam_to_world),
      PointCloudAccessor(posv_pcl), KCamera(posv_kcam),
      RigidTransform(posv_cam_to_world),
      RigidTransform(posv_cam_to_world.inverse()), point_dist_thresh,
      anch_pos_index.accessor<int64_t, 2>(),
      dock_pos_index.accessor<int64_t, 2>(),
      anch_neg_index.accessor<int64_t, 2>(),
      dock_neg_index.accessor<int64_t, 2>());
}

namespace {
int randint(int start, int end) { return start + rand() % (end - start); }

void MineNegativesImpl(
    const torch::TensorAccessor<int64_t, 1> &anch_pos_index,
    const torch::TensorAccessor<int64_t, 1> &dock_foreground_index,
    const torch::TensorAccessor<int64_t, 1> &dock_background_index,
    int num_foreground_negs, int num_background_negs,
    torch::TensorAccessor<int64_t, 1> aug_anch_neg_index,
    torch::TensorAccessor<int64_t, 1> aug_dock_neg_index) {
  const int counter_per_match = num_foreground_negs + num_background_negs;

  for (int i = 0; i < anch_pos_index.size(0); ++i) {
    int offset = i * counter_per_match;

    if (dock_foreground_index.size(0) > 0) {
      for (int k = 0; k < num_foreground_negs; ++k) {
        aug_anch_neg_index[offset] = anch_pos_index[i];
        aug_dock_neg_index[offset] =
            dock_foreground_index[randint(0, dock_foreground_index.size(0))];
        ++offset;
      }
    }

    if (dock_background_index.size(0) > 0) {
      for (int k = 0; k < num_background_negs; ++k) {
        aug_anch_neg_index[offset] = anch_pos_index[i];
        aug_dock_neg_index[offset] =
            dock_background_index[randint(0, dock_background_index.size(0))];
        ++offset;
      }
    }
  }
}

}  // namespace

void SlamFeatOp::MineNegatives(const torch::Tensor &anch_pos_index,
                               const torch::Tensor &dock_foreground_index,
                               const torch::Tensor &dock_background_index,
                               int num_foreground_negs, int num_background_negs,
                               torch::Tensor aug_anch_neg_index,
                               torch::Tensor aug_dock_neg_index) {
  MineNegativesImpl(anch_pos_index.accessor<int64_t, 1>(),
                    dock_foreground_index.accessor<int64_t, 1>(),
                    dock_background_index.accessor<int64_t, 1>(),
                    num_foreground_negs, num_background_negs,
                    aug_anch_neg_index.accessor<int64_t, 1>(),
                    aug_dock_neg_index.accessor<int64_t, 1>());
}

PYBIND11_MODULE(_cslamfeat, m) {
  pybind11::class_<SlamFeatOp>(m, "SlamFeatOp")
      .def_static("extract_indexed_tuple", &SlamFeatOp::ExtractIndexedTuple)
      .def_static("mine_negatives", &SlamFeatOp::MineNegatives)
      .def_static("mesh_intersection", &SlamFeatOp::MeshIntersection)
      .def_static("nn_feature_accuracy", &SlamFeatOp::NNFeatureAccuracy);

  pybind11::class_<PointCloud>(m, "PointCloud")
      .def(pybind11::init())
      .def_readwrite("mask", &PointCloud::mask)
      .def_readwrite("points", &PointCloud::points)
      .def_readwrite("colors", &PointCloud::colors)
      .def_readwrite("depths", &PointCloud::depths);

  pybind11::class_<FrameCrop>(m, "FrameCrop")
      .def(pybind11::init())
      .def_readwrite("colors", &FrameCrop::colors)
      .def_readwrite("depths", &FrameCrop::depths);
}
}  // namespace slamfeat
