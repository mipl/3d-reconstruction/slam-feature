#pragma once

#include <torch/torch.h>
#include <Eigen/Eigen>

#include <torch/csrc/utils/pybind.h>

namespace slamfeat {
struct KCamera {
  const typename torch::TensorAccessor<float, 2> matrix;

  /**
   * Initializer.
   *
   * @param matrix The [3x3] intrinsic camera matrix. Must be in the
   * same device kind as the `dev` template argument.
   */
  KCamera(const torch::Tensor &matrix) : matrix(matrix.accessor<float, 2>()) {}

  /**
   * Forward project a 3D point into a 2D one, and round to integer. V2
   */
  inline void Projecti(const Eigen::Vector3f point, int &x, int &y) const {
    const float img_x = matrix[0][0] * point[0] / point[2] + matrix[0][2];
    const float img_y = matrix[1][1] * point[1] / point[2] + matrix[1][2];

    x = int(round(img_x));
    y = int(round(img_y));
  }
};

struct RigidTransform {
  Eigen::Matrix<float, 3, 4, Eigen::DontAlign> rt_matrix;

  RigidTransform(const torch::Tensor &matrix) {
    const torch::TensorAccessor<float, 2> acc = matrix.accessor<float, 2>();
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 4; ++j) {
        rt_matrix(i, j) = acc[i][j];
      }
    }
  }

  inline Eigen::Vector3f Transform(const Eigen::Vector3f &point) const {
    const auto mtx = rt_matrix;
    const float px = mtx(0, 0) * point[0] + mtx(0, 1) * point[1] +
                     mtx(0, 2) * point[2] + mtx(0, 3);
    const float py = mtx(1, 0) * point[0] + mtx(1, 1) * point[1] +
                     mtx(1, 2) * point[2] + mtx(1, 3);
    const float pz = mtx(2, 0) * point[0] + mtx(2, 1) * point[1] +
                     mtx(2, 2) * point[2] + mtx(2, 3);

    return Eigen::Vector3f(px, py, pz);
  }
};

struct PointCloud {
  torch::Tensor mask, points, colors, depths;
};

struct FrameCrop {
  torch::Tensor colors, depths;
};

struct SlamFeatOp {
  static int ExtractIndexedTuple(
      const PointCloud &anch_pcl, const torch::Tensor &anch_cam_to_world,
      const PointCloud &posv_pcl, const torch::Tensor &posv_kcam,
      const torch::Tensor &posv_cam_to_world, float point_dist_thresh,
      torch::Tensor anch_pos_index, torch::Tensor dock_pos_index,
      torch::Tensor anch_neg_index, torch::Tensor dock_neg_index);

  static void MineNegatives(const torch::Tensor &anch_pos_index,
                            const torch::Tensor &dock_foreground_index,
                            const torch::Tensor &dock_background_index,
                            int num_foreground_negs, int num_background_negs,
                            torch::Tensor aug_anch_neg_index,
                            torch::Tensor aug_dock_neg_index);

  static std::pair<torch::Tensor, torch::Tensor> MeshIntersection(
      const torch::Tensor &a_verts, const torch::Tensor &a_trigs,
      const torch::Tensor &b_verts, const torch::Tensor &b_trigs);

  static void NNFeatureAccuracy(const torch::Tensor &feats_a,
                                const torch::Tensor &feats_b,
                                const torch::Tensor &nn_index,
                                torch::Tensor accuracy);

  static void RegisterPybind(pybind11::module &m);
};

}  // namespace slamfeat
