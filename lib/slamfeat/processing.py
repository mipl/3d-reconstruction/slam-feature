"""Preprocessing stuff.
"""
import random

import cv2
import torch
import numpy as np
import fire

from slamtb.processing import bilateral_depth_filter, estimate_normals
from slamtb.frame import Frame


class FrameProcessing:
    """Frame preprocessing callable.
    """

    def __init__(self, filter_depth=True, compute_normals=True,
                 filter_depth_normals=True,
                 blur=False, max_depth=None):
        self.filter_depth = filter_depth
        self.compute_normals = compute_normals
        self.filter_depth_normals = filter_depth_normals
        self.blur = blur
        self.max_depth = max_depth

    def __call__(self, frame, device="cpu:0"):

        if self.max_depth is None:
            mask = torch.from_numpy(frame.depth_image > 0)
        else:
            max_depth = int(self.max_depth / frame.info.depth_scale)
            mask = torch.from_numpy((frame.depth_image > 0)
                                    & (frame.depth_image <= max_depth))
        mask = mask.to(device)

        if self.filter_depth:
            depth_image = torch.from_numpy(frame.depth_image).to(device)
            depth_image = bilateral_depth_filter(
                depth_image, mask, filter_width=13)
        else:
            depth_image = torch.from_numpy(frame.depth_image.copy())

        if self.compute_normals:
            normal_depth_image = depth_image
            if self.filter_depth_normals:
                if self.filter_depth:
                    normal_depth_image = depth_image.to(device)
                else:
                    normal_depth_image = torch.from_numpy(
                        frame.depth_image).to(device)
                    normal_depth_image = bilateral_depth_filter(
                        normal_depth_image,
                        mask, filter_width=13)
            else:
                normal_depth_image = depth_image.to(device)

            normal_image = estimate_normals(
                normal_depth_image, frame.info, mask,
            )
        else:
            normal_image = None

        depth_image = depth_image.cpu().numpy()

        if self.blur:
            rgb_image = cv2.blur(frame.rgb_image, (5, 5))
        else:
            rgb_image = frame.rgb_image.copy()

        return Frame(frame.info.clone(), depth_image, rgb_image, normal_image=normal_image)


class RandomBackgroud:
    """Generates random background
    """

    def __init__(self, aug_prob=0.5, grad_prob=0.5):
        self.aug_prob = aug_prob
        self.grad_prob = grad_prob

    def __call__(self, rgb_image, mask):
        if random.random() > self.aug_prob:
            return rgb_image

        random_image = self._make_random_image(rgb_image.shape)

        rgb_image = rgb_image.clone()
        random_image[mask, :] = 0
        rgb_image[~mask, :] = 0

        return rgb_image + random_image

    def _make_random_image(self, shape):
        if random.random() > self.grad_prob:
            return self._make_solid_random_color(shape)

        rgb1 = RandomBackgroud._make_solid_random_color(shape)
        rgb2 = RandomBackgroud._make_solid_random_color(shape)
        rand_image = RandomBackgroud._make_gradient_image(
            rgb1, rgb2, vertical=bool(np.random.uniform() > 0.5))

        if random.random() < 0.5:
            return rand_image

        return self._add_noise(rand_image)

    @staticmethod
    def _make_solid_random_color(shape):
        random_rgb = torch.from_numpy(
            np.random.uniform(size=3) * 255).byte()
        return torch.ones(shape, dtype=torch.uint8)*random_rgb

    def _add_noise(self, rgb_image):
        max_noise_to_add_or_subtract = 50

        def random_image():
            return torch.from_numpy(
                np.random.uniform(size=rgb_image.shape)*max_noise_to_add_or_subtract).byte()

        return rgb_image + random_image() - random_image()

    @staticmethod
    def _make_gradient_image(rgb1, rgb2, vertical):
        bitmap = np.zeros_like(rgb1)
        h, w = rgb1.shape[0], rgb1.shape[1]
        if vertical:
            p = np.tile(np.linspace(0, 1, h)[:, None], (1, w))
        else:
            p = np.tile(np.linspace(0, 1, w), (h, 1))

        for i in range(3):
            bitmap[:, :, i] = rgb2[:, :, i] * p + rgb1[:, :, i] * (1.0 - p)

        return torch.from_numpy(bitmap)


class _Testing:
    @staticmethod
    def random_background():
        """Tests the RandomBackgroud class"""
        from pathlib import Path
        import matplotlib.pyplot as plt
        import rflow

        data_g = rflow.open_graph(
            Path(__file__).parent / "../../data/pdc", "catepillar")
        frame = data_g["s11.dataset"].call()[5]

        rgb_image = torch.from_numpy(frame.rgb_image)
        mask = torch.from_numpy(frame.depth_image > 0)

        rand_bg = RandomBackgroud(aug_prob=1.0, grad_prob=1.0)

        rand_image = rand_bg(rgb_image, mask)
        plt.imshow(rand_image)
        plt.show()


if __name__ == '__main__':
    fire.Fire(_Testing)
