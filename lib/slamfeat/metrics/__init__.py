import numpy as np
from sklearn.metrics import roc_curve as _roc_curve
import matplotlib.pyplot as plt


def acc_curve(ground_truth, pred_distances):
    threshs = np.linspace(0, pred_distances.max(), 100)
    accs = []
    for thresh in threshs:
        pred = pred_distances < thresh
        acc = (ground_truth == pred).sum() / pred_distances.shape[0]
        accs.append(acc)

    return accs, threshs


def feature_match_acc_curve(ground_truth, pred_distances, algorithm_title):
    accs, threshs = acc_curve(ground_truth, pred_distances)

    return accs, threshs


def feature_match_roc(ground_truth, pred_distances, algorithm_title=""):
    min_dist = pred_distances.min()
    max_dist = pred_distances.max()

    pred_distances = 1.0 - (pred_distances - min_dist) / (max_dist - min_dist)

    fpr, tpr, _ = _roc_curve(ground_truth, pred_distances)

    return fpr, tpr
