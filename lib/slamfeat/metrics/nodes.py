"""Node for evaluating metrics.
"""
import rflow

import torch

from slamtb.feature import SIFT


class EvaluateFeatureMatching(rflow.Interface):
    """Evaluate a feature extractor using ROC.
    """

    def evaluate(self, resource, test_dataset,
                 feature_extractor, alg_name):
        import matplotlib.pyplot as plt
        from tqdm import tqdm
        import numpy as np

        from slamfeat.metrics import feature_match_roc, feature_match_acc_curve

        preds = []
        gts = []

        sift = SIFT()
        for i in tqdm(range(len(test_dataset))):
            rgbd_pair = test_dataset[i]

            keypoints = sift.extract_features(
                rgbd_pair.anc_rgb.numpy()).keypoints
            keypoints = set((x.item(), y.item()) for x, y in keypoints)

            anc_features = feature_extractor.extract_features(
                rgbd_pair.anc_rgb, rgbd_pair.anc_depth)
            dock_features = feature_extractor.extract_features(
                rgbd_pair.dock_rgb, rgbd_pair.dock_depth)

            image_shape = tuple(rgbd_pair.anc_rgb.shape[:2])
            for (anc_y, anc_x, dok_y, dok_x) in zip(
                    *np.unravel_index(rgbd_pair.anc_posv_index, image_shape),
                    *np.unravel_index(rgbd_pair.dock_posv_index, image_shape)):
                anc_x, anc_y = anc_x.item(), anc_y.item()
                dok_x, dok_y = dok_x.item(), dok_y.item()
                if (anc_x, anc_y) not in keypoints:
                    continue

                anc_feat = anc_features[:, anc_y, anc_x]
                dock_feat = dock_features[:, dok_y, dok_x]

                preds.append((dock_feat - anc_feat).norm())
                gts.append(1)

            for (anc_y, anc_x, dok_y, dok_x) in zip(
                    *np.unravel_index(rgbd_pair.anc_negv_index, image_shape),
                    *np.unravel_index(rgbd_pair.dock_negv_index, image_shape)):
                anc_x, anc_y = anc_x.item(), anc_y.item()
                dok_x, dok_y = dok_x.item(), dok_y.item()
                if (anc_x, anc_y) not in keypoints:
                    continue

                anc_feat = anc_features[:, anc_y, anc_x]
                dock_feat = dock_features[:, dok_y, dok_x]

                preds.append((dock_feat - anc_feat).norm())
                gts.append(0)

        preds = np.array(preds)
        gts = np.array(gts)

        fig = plt.figure()
        roc = fig.add_subplot(121)
        fpr, tpr = feature_match_roc(gts, preds, alg_name)

        roc.plot(fpr, tpr, label="ROC curve")
        roc.set_xlabel("False positive rate")
        roc.set_ylabel("True positive rate")
        roc.set_title("{} ROC".format(alg_name))

        accs, threshs = feature_match_acc_curve(gts, preds, alg_name)
        acc = fig.add_subplot(122)
        acc.plot(threshs, accs)
        acc.set_xlabel("Distance")
        acc.set_ylabel("Accuracy")
        acc.set_title("{} Accuracy vs distance".format(alg_name))

        plt.show()
        fig.savefig(resource.filepath)

    def load(self, resource):
        """Load"""
        print("Checkout figure {}".format(resource.filepath))


class EvaluateFeatureDistance(rflow.Interface):
    """Evaluate a feature extractor using ROC.
    """

    def evaluate(self, resource, test_dataset, feature_extractor, alg_name):
        import matplotlib.pyplot as plt
        from tqdm import tqdm
        import numpy as np
        import seaborn as sns
        from pytablewriter import MarkdownTableWriter

        positive_dists = []
        negative_dists = []
        for i in tqdm(range(len(test_dataset))):
            triplet = test_dataset[i]
            anchor_rgb, anchor_depth = triplet[0][0], triplet[0][1]
            positive_rgb, positive_depth = triplet[1][0], triplet[1][1]
            negative_rgb, negative_depth = triplet[2][0], triplet[2][1]

            anchor_feat = feature_extractor.extract_features(
                anchor_rgb, anchor_depth)
            positive_feat = feature_extractor.extract_features(
                positive_rgb, positive_depth)
            negative_feat = feature_extractor.extract_features(
                negative_rgb, negative_depth)

            positive_dists.append(
                feature_extractor.distance(anchor_feat, positive_feat))
            negative_dists.append(
                feature_extractor.distance(anchor_feat, negative_feat))

        positive_dists = np.array(positive_dists)
        negative_dists = np.array(negative_dists)

        writer = MarkdownTableWriter()
        writer.headers = ["", "mean", "std"]
        writer.value_matrix = [["Positive", positive_dists.mean(), positive_dists.std()],
                               ["Negative", negative_dists.mean(), negative_dists.std()]]
        print(writer.dumps())

        fig = plt.figure()
        plt.title(
            f"{alg_name}: Pixelwise positive vs negative matching distance distribution")
        ax = sns.distplot(positive_dists, hist=False, label="Positive matchs")
        sns.distplot(negative_dists, hist=False, label="Negative matchs")

        ax.set(xlabel="Distance value", ylabel="Density")
        plt.show()
        fig.savefig(resource.filepath)

    def load(self, resource):
        """Load"""
        print("Checkout figure {}".format(resource.filepath))


class InitialAlign(rflow.Interface):
    def evaluate(self, mov_pcl, transform=None):
        if transform is not None:
            return mov_pcl.transform(torch.tensor(transform).float())
        return mov_pcl


class ManualAlign(rflow.Interface):
    def evaluate(self, resource, fixed_geo, mov_pcl):
        from slamfeat.viz.recviewer import AlignTool

        align_tool = AlignTool(fixed_geo, mov_pcl)
        align_tool.run()

        torch.save(align_tool.transformation, resource.filepath)

        return mov_pcl.transform(align_tool.transformation)

    def load(self, resource, mov_pcl):
        transformation = torch.load(resource.filepath)
        print(transformation)
        return mov_pcl.transform(transformation)


class ICPRegistration(rflow.Interface):
    """Use the Open3D ICP algorithm to align two point clouds that are already close."""

    def evaluate(self, resource, fixed_pcl, mov_pcl, distance_threshold=10.0):
        import open3d
        import numpy as np

        has_normals = fixed_pcl.normals is not None

        fixed_pcl = fixed_pcl.to_open3d()
        mov_pcl_o3d = mov_pcl.to_open3d()

        trans_init = np.eye(4)
        distance_threshold = 0.05
        result = open3d.pipelines.registration.registration_icp(
            mov_pcl_o3d, fixed_pcl, distance_threshold, trans_init,
            (open3d.pipelines.registration.TransformationEstimationPointToPlane()
             if has_normals else
             open3d.pipelines.registration.TransformationEstimationPointToPoint()),
            open3d.pipelines.registration.ICPConvergenceCriteria(
                relative_fitness=1e-8,
                relative_rmse=1e-8, max_iteration=1000))

        transform = torch.from_numpy(result.transformation.copy()).float()
        torch.save(transform, resource.filepath)

        return mov_pcl.transform(transform)

    def load(self, resource, mov_pcl):
        transformation = torch.load(resource.filepath)
        print(transformation)
        return mov_pcl.transform(transformation)


class ViewAlignment(rflow.Interface):
    """View two point clouds.
    """

    def evaluate(self, fixed_geo, mov_geo):
        """
        Args:

            fixed_model_path: fixed model path.

            mov_model_path: moveable model path.
        """
        from slamfeat.viz.recviewer import ReconstructionViewer

        viewer = ReconstructionViewer(fixed_geo, mov_geo)
        viewer.run()


class ChamferMetric(rflow.Interface):
    """Calculates the Chamfer distance between two point clouds
    """

    def evaluate(self, rec_pcl, gt_pcl):
        """Args:

        source_path: reconstruction point cloud file path.

        gt_path: ground truth point cloud file path
        """
        from slamtb.metrics import chamfer_score

        score = chamfer_score(rec_pcl.points, gt_pcl.points)
        result = {"Chamfer": score}
        self.save_measurement(result)
        print(result)


class MeshNearestPoints(rflow.Interface):
    def evaluate(self, resource, rec_pcl, gt_mesh):
        import tenviz

        from slamtb.spatial.trigoctree import TrigOctree

        faces = tenviz.geometry.to_triangles(gt_mesh.faces)
        tree = TrigOctree(gt_mesh.verts,
                          faces.long(), 1024)
        gt_points, _ = tree.query_closest_points(rec_pcl.points)

        torch.save(gt_points, resource.filepath)
        return gt_points

    def load(self, resource):
        return torch.load(resource.filepath)


class PCLNearestPoints(rflow.Interface):
    def evaluate(self, resource, rec_pcl, gt_pcl):
        from scipy.spatial.ckdtree import cKDTree
        import tenviz

        tree = cKDTree(gt_pcl.points)
        _, index = tree.query(rec_pcl.points)

        gt_points = gt_pcl.points[index]

        torch.save(gt_points, resource.filepath)
        return gt_points

    def load(self, resource):
        return torch.load(resource.filepath)


class AccuracyMetric(rflow.Interface):
    def evaluate(self, rec_pcl, nearest_gt_points, thresh_dist):
        from slamtb.metrics.geometry import reconstruction_accuracy

        score = reconstruction_accuracy(
            rec_pcl.points, nearest_gt_points, thresh_dist)
        result = {"Accuracy": score}
        self.save_measurement(result)
        print(result)


class HeatMapMesh(rflow.Interface):
    def evaluate(self, resource, nearest_points, rec_pcl):
        from matplotlib.pyplot import get_cmap
        from matplotlib.colors import Normalize
        import numpy as np

        from tenviz.io import write_3dobject

        distances = (rec_pcl.points - nearest_points).norm(dim=1)
        distances = np.array(Normalize()(distances.numpy()))

        cmap = get_cmap('plasma', distances.size)
        colors = cmap(distances)

        colors = (colors[:, :3]*255).astype(np.uint8)
        rec_pcl.colors = torch.from_numpy(colors)
        write_3dobject(resource.filepath, rec_pcl.points, normals=rec_pcl.normals,
                       colors=colors)

        return rec_pcl

    def load(self, resource):
        from tenviz.io import read_3dobject

        return read_3dobject(resource.filepath).torch()


def create_evaluation_graph(eval_g, rec_node, gt_mesh_node, gt_pcl_node,
                            output_prefix, init_matrix=None,
                            show_only_end_nodes=False):

    eval_g.initial = InitialAlign()
    with eval_g.initial as args:
        args.transform = init_matrix
        args.mov_pcl = rec_node

    gt_target_node = (gt_mesh_node if gt_mesh_node is not None
                      else gt_pcl_node)
    with eval_g.prefix("manual_") as sub:
        sub.align = ManualAlign(rflow.FSResource(
            "{}manual-align.torch".format(output_prefix)),
            show=not show_only_end_nodes)
        with sub.align as args:
            args.fixed_geo = gt_target_node
            args.mov_pcl = eval_g.initial

        sub.view = ViewAlignment(show=not show_only_end_nodes)
        with sub.view as args:
            args.fixed_geo = gt_target_node
            args.mov_geo = sub.align

    with eval_g.prefix("icp_") as sub:
        sub.align = ICPRegistration(rflow.FSResource(
            "{}local-reg.torch".format(output_prefix)),
            show=not show_only_end_nodes)
        with sub.align as args:
            args.fixed_pcl = gt_pcl_node
            args.mov_pcl = eval_g.manual_align

        sub.view = ViewAlignment(show=not show_only_end_nodes)
        with sub.view as args:
            args.fixed_geo = gt_pcl_node
            args.mov_geo = eval_g.icp_align

    eval_g.chamfer = ChamferMetric()
    with eval_g.chamfer as args:
        args.gt_pcl = gt_pcl_node
        args.rec_pcl = eval_g.icp_align

    if gt_mesh_node is not None:
        eval_g.nearest_points = MeshNearestPoints(rflow.FSResource(
            "{}-nearest-points.torch".format(output_prefix)),
            show=not show_only_end_nodes)
        with eval_g.nearest_points as args:
            args.gt_mesh = gt_mesh_node
            args.rec_pcl = eval_g.icp_align
    else:
        eval_g.nearest_points = PCLNearestPoints(rflow.FSResource(
            "{}-nearest-points.torch".format(output_prefix)),
            show=not show_only_end_nodes)
        with eval_g.nearest_points as args:
            args.gt_pcl = gt_pcl_node
            args.rec_pcl = eval_g.icp_align

    eval_g.accuracy = AccuracyMetric()
    with eval_g.accuracy as args:
        args.nearest_gt_points = eval_g.nearest_points
        args.rec_pcl = eval_g.icp_align
        args.thresh_dist = 0.1

    eval_g.heat_map = HeatMapMesh(rflow.FSResource(
        "{}-hmap.ply".format(output_prefix)),
        show=not show_only_end_nodes)
    with eval_g.heat_map as args:
        args.nearest_points = eval_g.nearest_points
        args.rec_pcl = eval_g.icp_align

    eval_g.heat_map_view = ViewAlignment()
    with eval_g.heat_map_view as args:
        args.fixed_geo = gt_pcl_node
        args.mov_geo = eval_g.heat_map
