"""Indexed RGBD Triplet dataset.
"""
import random

import torch
import numpy as np

from slamtb.frame import FramePointCloud

from slamfeat.data.cache import FrameCache
from slamfeat.data.datatype import create_point_cloud

from slamfeat._cslamfeat import SlamFeatOp


class RGBDCorrespondencePair:
    def __init__(self, *, anc_rgb, anc_depth,
                 dock_rgb, dock_depth,
                 anc_posv_index,
                 dock_posv_index,
                 anc_negv_index,
                 dock_negv_index):
        self.anc_rgb = anc_rgb
        self.anc_depth = anc_depth

        self.dock_rgb = dock_rgb
        self.dock_depth = dock_depth

        self.anc_posv_index = anc_posv_index
        self.dock_posv_index = dock_posv_index

        self.anc_negv_index = anc_negv_index
        self.dock_negv_index = dock_negv_index

    def sample_correspondences(self, max_positive_samples : int, max_negative_samples:int):

        if self.anc_posv_index.size(0) > max_positive_samples:
            posv_sample = torch.tensor(
                random.choices(
                    range(self.anc_posv_index.size(0)), k=max_positive_samples))
            anc_posv_index = self.anc_posv_index[posv_sample]
            dock_posv_index = self.dock_posv_index[posv_sample]
        else:
            anc_posv_index = self.anc_posv_index
            dock_posv_index = self.dock_posv_index

        if self.anc_negv_index.size(0) > max_negative_samples:
            negv_sample = torch.tensor(
                random.choices(
                    range(self.anc_negv_index.size(0)), k=max_negative_samples))
            anc_negv_index = self.anc_negv_index[negv_sample]
            dock_negv_index = self.dock_negv_index[negv_sample]
        else:
            anc_negv_index = self.anc_negv_index
            dock_negv_index = self.dock_negv_index

        return RGBDCorrespondencePair(
            anc_rgb=self.anc_rgb, anc_depth=self.anc_depth,
            dock_rgb=self.dock_rgb, dock_depth=self.dock_depth,
            anc_posv_index=anc_posv_index,
            dock_posv_index=dock_posv_index,
            anc_negv_index=anc_negv_index,
            dock_negv_index=dock_negv_index)


class RGBDCorrespondenceDataset:
    """Dataset of positive and negative correspodence between two random RGBD images.
    """

    def __init__(self, rgbd_dataset, visibility_graph, point_dist_thresh=0.05,
                 scale=None, bg_ids=None, augmentation_fn=None, max_samples=5000):
        self.rgbd_dataset = FrameCache(rgbd_dataset)
        self.visibility_graph = visibility_graph
        self.nodes = list(node_id for node_id in visibility_graph.keys()
                          if visibility_graph.get_edge_count(node_id) > 0)

        self.point_dist_thresh = point_dist_thresh
        self.scale = scale
        self.bg_ids = bg_ids
        self.augmentation_fn = augmentation_fn

    def _get_dock_idx(self, anchor_idx):
        anchor_edges = self.visibility_graph.graph[anchor_idx]
        return random.choice(list(anchor_edges))

    def __getitem__(self, anchor_idx, dock_idx=None):
        anchor_idx = self.nodes[anchor_idx]
        anch_frame = self.rgbd_dataset[anchor_idx]

        if dock_idx is None:
            dock_idx = self._get_dock_idx(anchor_idx)

        if self.scale is not None:
            anch_frame = anch_frame.scaled(self.scale)

        anch_pcl = FramePointCloud.from_frame(anch_frame)
        if anch_frame.seg_image is not None and self.bg_ids is not None:
            seg_mask = ~np.isin(anch_frame.seg_image, self.bg_ids)
            anch_pcl.mask = torch.from_numpy(
                np.logical_and(anch_pcl.mask.numpy(), seg_mask))

        dock_frame = self.rgbd_dataset[dock_idx]
        if self.scale is not None:
            dock_frame = dock_frame.scaled(self.scale)

        dock_pcl = FramePointCloud.from_frame(dock_frame)

        anch_posv_indices = torch.empty(
            anch_pcl.height, anch_pcl.width, dtype=torch.int64)
        dock_posv_indices = torch.empty(
            dock_pcl.height, dock_pcl.width, dtype=torch.int64)

        anch_neg_indices = torch.empty(
            anch_pcl.height, anch_pcl.width, dtype=torch.int64)
        dock_neg_indices = torch.empty(
            dock_pcl.height, dock_pcl.width, dtype=torch.int64)

        count = SlamFeatOp.extract_indexed_tuple(
            create_point_cloud(anch_pcl, anch_frame.depth_image),
            anch_pcl.rt_cam.cam_to_world.float(),
            create_point_cloud(dock_pcl, dock_frame.depth_image),
            dock_pcl.kcam.matrix.float(),
            dock_pcl.rt_cam.cam_to_world.float(),
            self.point_dist_thresh,
            anch_posv_indices, dock_posv_indices,
            anch_neg_indices, dock_neg_indices)

        valid_pos_idxs = anch_posv_indices > -1
        valid_neg_idxs = anch_neg_indices > -1

        item = RGBDCorrespondencePair(
            anc_rgb=torch.from_numpy(anch_frame.rgb_image),
            anc_depth=torch.from_numpy(
                anch_frame.depth_image).float() * anch_frame.info.depth_scale,
            dock_rgb=torch.from_numpy(dock_frame.rgb_image),
            dock_depth=torch.from_numpy(
                dock_frame.depth_image).float() * dock_frame.info.depth_scale,
            anc_posv_index=anch_posv_indices[valid_pos_idxs],
            dock_posv_index=dock_posv_indices[valid_pos_idxs],
            anc_negv_index=anch_neg_indices[valid_neg_idxs],
            dock_negv_index=dock_neg_indices[valid_neg_idxs])

        if self.augmentation_fn is not None:
            item = self.augmentation_fn(item, anch_pcl.mask, dock_pcl.mask)
        return item

    def __len__(self):
        return len(self.nodes)


class _Testing:

    @staticmethod
    def _load_dataset(with_augmentation):
        from pathlib import Path
        import rflow

        from slamfeat.data.augmentation import CorrespondenceAugmentation

        dataset_g = rflow.open_graph(
            Path(__file__).parent / "../../../data/scenenn", "scenenn")

        dataset = dataset_g['016.dataset'].call()
        vis_graph = dataset_g['016.visibility_graph'].call()

        augmentation = None
        if with_augmentation:
            augmentation = CorrespondenceAugmentation()

        dataset = IndexedCorrespondenceDataset(dataset, vis_graph, scale=1.0,
                                               point_dist_thresh=0.003,
                                               augmentation_fn=augmentation)

        return dataset

    @staticmethod
    def correspondence():
        import matplotlib.pyplot as plt
        from slamtb.viz.feature import draw_matchings_2d
        dataset = _Testing._load_dataset(False)
        random.seed(10)

        corresp_pair = dataset[874]

        image_shape = tuple(corresp_pair.anc_rgb.shape[:2])

        anc_yx = np.vstack(np.unravel_index(
            corresp_pair.anc_posv_index.numpy(), image_shape)).transpose()
        dock_yx = np.vstack(np.unravel_index(
            corresp_pair.dock_posv_index.numpy(), image_shape)).transpose()
        selected_points = random.choices(
            range(corresp_pair.anc_posv_index.size(0)), k=25)
        matchings = torch.tensor(list(zip(selected_points, selected_points)),
                                 dtype=torch.int64)
        plt.figure()
        plt.title("Positive matchings")
        draw_matchings_2d(corresp_pair.anc_rgb.numpy(),
                          corresp_pair.dock_rgb.numpy(),
                          anc_yx, dock_yx,
                          matchings)

        plt.figure()
        plt.title("Negative matchings")
        anc_yx = np.vstack(np.unravel_index(
            corresp_pair.anc_negv_index.numpy(), image_shape)).transpose()
        dock_yx = np.vstack(np.unravel_index(
            corresp_pair.dock_negv_index.numpy(), image_shape)).transpose()
        selected_points = random.choices(
            range(corresp_pair.anc_negv_index.size(0)), k=25)
        neg_matchings = torch.tensor(list(zip(selected_points, selected_points)),
                                     dtype=torch.int64)
        draw_matchings_2d(corresp_pair.anc_rgb.numpy(),
                          corresp_pair.dock_rgb.numpy(),
                          anc_yx, dock_yx,
                          neg_matchings)

        plt.show()

    @staticmethod
    def view(with_augmentation=False):
        """View some dataset patches. The patches will look strange because
        this dataset does not produce pairwise aligned ones.
        """

        from slamfeat.viz.dataset import CorrespondenceDatasetViewer

        dataset = _Testing._load_dataset(with_augmentation)

        CorrespondenceDatasetViewer(dataset, 3).run()

    @staticmethod
    def profile(with_augmentation=True):
        """Profile the dataset+negative mining"""
        from pathlib import Path

        from slamtb._utils import profile

        dataset = _Testing._load_dataset(with_augmentation)
        with profile(Path(__file__).parent / "negative_mining.cprof"):
            for i in range(0, 40):
                _ = dataset[i]


if __name__ == '__main__':
    from fire import Fire
    Fire(_Testing)
