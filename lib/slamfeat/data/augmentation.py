"""Training data augmentation functions.
"""
import torch

from slamfeat.processing import RandomBackgroud
from slamfeat._cslamfeat import SlamFeatOp


class CorrespondenceAugmentation:
    """
    """

    def __init__(self, num_foreground_negatives=10,
                 num_background_negatives=10): # 75
        self.num_foreground_negatives = num_foreground_negatives
        self.num_background_negatives = num_background_negatives
        self.rand_bg = RandomBackgroud()

    def __call__(self, item, anch_match_mask, dock_match_mask):
        dock_background = torch.nonzero(~dock_match_mask, as_tuple=False).squeeze()

        total_new_negs = ((self.num_background_negatives + self.num_foreground_negatives)
                          *item.anc_posv_index.size(0))

        aug_anc_neg_index = torch.full((total_new_negs,), -1, dtype=torch.int64)
        aug_dock_neg_index = aug_anc_neg_index.clone()

        SlamFeatOp.mine_negatives(item.anc_posv_index,
                                  item.dock_negv_index,
                                  torch.nonzero(dock_background > 0, as_tuple=False).flatten(),
                                  self.num_foreground_negatives,
                                  self.num_background_negatives,
                                  aug_anc_neg_index,
                                  aug_dock_neg_index)

        valid = aug_anc_neg_index > -1
        item.anc_negv_index = aug_anc_neg_index[valid]
        item.dock_negv_index = aug_dock_neg_index[valid]

        item.anc_rgb = self.rand_bg(item.anc_rgb, anch_match_mask)
        item.dock_rgb = self.rand_bg(item.dock_rgb, dock_match_mask)
        return item
