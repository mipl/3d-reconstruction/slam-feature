"""Handle pose graph creation from ground truth dataset.
"""
import subprocess
from collections import defaultdict

import torch
import torch.multiprocessing as multiprocessing
import numpy as np
import trimesh
from tqdm import tqdm

from slamtb.posegraph import PoseGraphNode, PoseGraphEdge
from slamfeat._cslamfeat import SlamFeatOp
from slamtb.camera import RTCamera


def _np_rigid_transform(matrix, points):
    points = matrix[:3, :3] @ points.reshape(-1, 3, 1)
    points += matrix[:3, 3].reshape(3, 1)
    return points.reshape(-1, 3)


class Frustum:
    """Camera frustum
    """

    def __init__(self, points, view_dir, is_opengl=False):
        self.points = points
        self.view_dir = view_dir
        self.is_opengl = is_opengl

    @classmethod
    def create_from_opengl_cam(cls, proj, pose_matrix):
        """Create from a OpenGL projection and camera matrix.

        Args:

            proj (:obj:`tenviz.Projection`): Projection params

            pose_matrix (:obj:`numpy.ndarray`): View matrix inverse.

        """
        points = np.array([
            [proj.left, proj.top, -proj.near],
            [proj.left, proj.bottom, -proj.near],
            [proj.right, proj.bottom, -proj.near],
            [proj.right, proj.top, -proj.near],
            [proj.far_left, proj.far_top, -proj.far],
            [proj.far_left, proj.far_bottom, -proj.far],
            [proj.far_right, proj.far_bottom, -proj.far],
            [proj.far_right, proj.far_top, -proj.far]],
            dtype=np.float32)

        points = _np_rigid_transform(pose_matrix, points)

        view_dir = pose_matrix[:3, 2]
        return cls(points, view_dir / np.linalg.norm(view_dir), True)

    @classmethod
    def create_from_camera(cls, kcam, rt_cam, near_clip, far_clip):
        """Create from slamtb camera and its pose.

        Args:

            kcam (:obj:`slamtb.camera.KCamera`): Intrinsic camera.

            rt_cam (:obj:`slamtb.camera.RTCamera`): Extrinsic camera.

        """
        proj = kcam.get_projection_params(near_clip, far_clip)

        points = np.array([
            [proj.left, proj.top, proj.near],
            [proj.left, proj.bottom, proj.near],
            [proj.right, proj.bottom, proj.near],
            [proj.right, proj.top, proj.near],
            [proj.far_left, proj.far_top, proj.far],
            [proj.far_left, proj.far_bottom, proj.far],
            [proj.far_right, proj.far_bottom, proj.far],
            [proj.far_right, proj.far_top, proj.far]],
            dtype=np.float32)

        points = _np_rigid_transform(
            rt_cam.cam_to_world.float().numpy(), points)

        view_dir = rt_cam.cam_to_world[:3, 2].numpy()
        return cls(points, view_dir / np.linalg.norm(view_dir), False)

    def to_mesh(self):
        """Converts the frustum into a watertight mesh.

        Returns: (:obj:`trimesh.Trimesh`):
            Frustum mesh.
        """
        if self.is_opengl:
            faces = torch.tensor([[0, 1, 2],
                                  [2, 3, 0],
                                  [4, 7, 6],
                                  [6, 5, 4],

                                  [0, 4, 5],
                                  [5, 1, 0],
                                  [3, 2, 6],
                                  [6, 7, 3],

                                  [0, 3, 7],
                                  [7, 4, 0],
                                  [1, 5, 6],
                                  [6, 2, 1]],
                                 dtype=torch.int32)
        else:
            faces = torch.tensor([[2, 1, 0],
                                  [0, 3, 2],
                                  [6, 7, 4],
                                  [4, 5, 6],

                                  [5, 4, 0],
                                  [0, 1, 5],
                                  [6, 2, 3],
                                  [3, 7, 6],

                                  [7, 3, 0],
                                  [0, 4, 7],
                                  [6, 5, 1],
                                  [1, 2, 6]],
                                 dtype=torch.int32)
        return torch.from_numpy(self.points), faces

    def to_trimesh(self):
        """Converts the frustum into a watertight mesh.

        Returns: (:obj:`trimesh.Trimesh`):
            Frustum mesh.
        """
        verts, faces = self.to_mesh()
        return trimesh.Trimesh(vertices=verts,
                               faces=faces)

    def get_aabb(self):
        minp = self.points.min(axis=0)
        maxp = self.points.max(axis=0)

        return minp, maxp


def test_frustum_aabb_intersection(frus1, frus2):
    min1, max1 = frus1.get_aabb()
    min2, max2 = frus2.get_aabb()

    return ((min1[0] <= max2[0] and max1[0] >= min2[0]) and
            (min1[1] <= max2[1] and max1[1] >= min2[1]) and
            (min1[2] <= max2[2] and max1[2] >= min2[2]))


def is_frustums_angle_good(frus1, frus2, dot_thresh=0.25):
    """Compare two frustum angles.
    """
    return np.dot(frus1.view_dir, frus2.view_dir) > dot_thresh


def calculate_frustum_intersection_volume(frus1, frus2, engine='cgal'):
    """Returns the volume of the intersection between two frustums.
    """

    mesh1 = frus1.to_trimesh()
    mesh2 = frus2.to_trimesh()
    max_volume = max(mesh1.volume, mesh2.volume)
    if engine == 'scad':
        try:
            intersec = trimesh.boolean.intersection(
                [mesh1, mesh2], debug=True, engine='scad')
        except subprocess.CalledProcessError:
            return 0.0  # No intersection
    elif engine == 'cgal':
        verts1, trigs1 = frus1.to_mesh()
        verts2, trigs2 = frus2.to_mesh()

        intersec_verts, intersec_trigs = SlamFeatOp.mesh_intersection(
            verts1, trigs1,
            verts2, trigs2)
        intersec = trimesh.Trimesh(vertices=intersec_verts.numpy(),
                                   faces=intersec_trigs.numpy())
        if intersec.volume > max_volume:
            print(intersec.volume, max_volume)
            return 0.0  # No intersection
    else:
        raise RuntimeError(f"Unknow engine {engine}")

    return intersec.volume / max_volume


def _process_intersection(args):
    i = args[0]
    j = args[1]
    frustum_i = args[2]
    frustum_j = args[3]
    frustum_intersection_ratio = args[4]
    camera_max_angle = args[5]

    if not is_frustums_angle_good(frustum_i, frustum_j, camera_max_angle):
        return False, (i, j)

    if not test_frustum_aabb_intersection(frustum_i, frustum_j):
        return False, (i, j)

    volume = calculate_frustum_intersection_volume(
        frustum_i, frustum_j, engine='cgal')

    if volume < frustum_intersection_ratio:
        return False, (i, j)

    return True, (i, j)


class VisibilityGraph:
    def __init__(self):
        self.graph = defaultdict(set)

    def add_node(self, node_id):
        self.graph[node_id] = set()

    def add_edge(self, node_i, node_j):
        self.graph[node_i].add(node_j)
        self.graph[node_j].add(node_i)

    def has_edge(self, node_i, node_j):
        return node_j in self.graph[node_i]

    def get_edge_count(self, node_i):
        return len(self.graph[node_i])

    def keys(self):
        return self.graph.keys()


def create_visibility_graph_from_dataset(dataset,
                                         frustum_intersection_ratio=0.5,
                                         camera_max_angle=0.25,
                                         num_processes=12,
                                         max_frames=5000) -> VisibilityGraph:
    """Creates a bidirectional graph where each edge connects two frames
    according to ratio of intersection of their frustums.

    Args:
        dataset: A RGB-D dataset

       frustum_intersection_ratio: Ratio of frustum intersection
        between two frames.

       camera_max_angle: The maximum angle between the two frames'
        cameras.

       num_processes: Number of cores to use during the processing.

       max_frames: Max frames to processes. Those dataset with more
       frames than this value have their frames distribuited along
       this size.

    Returns:
        The graph for the input dataset.

    """
    visibility_graph = VisibilityGraph()
    frustums = {}

    dataset_len = len(dataset)
    # dataset_len = 50
    if dataset_len < max_frames:
        frames = range(dataset_len)
    else:
        frames = np.linspace(0, dataset_len - 1,
                             max_frames).astype(np.int64).tolist()

    for i in tqdm(frames, total=len(frames), desc="1 of 2 Reading the dataset"):
        frame_i = dataset[i]
        max_depth = (frame_i.depth_image * frame_i.info.depth_scale).max()

        frustums[i] = Frustum.create_from_camera(frame_i.info.kcam,
                                                 frame_i.info.rt_cam,
                                                 0.1, max_depth)

    combinations = []
    for i in range(0, len(frames) - 1):
        for j in range(i+1, len(frames)):
            frame1 = frames[i]
            frame2 = frames[j]
            combinations.append((frame1, frame2, frustums[frame1], frustums[frame2],
                                 frustum_intersection_ratio, camera_max_angle))

    desc = "2 of 2 Checking frustum intersections"
    if num_processes > 1:
        with multiprocessing.Pool(processes=num_processes) as pool:
            for is_edge, ij in tqdm(pool.imap_unordered(_process_intersection,
                                                        combinations),
                                    total=len(combinations), desc=desc):
                if not is_edge:
                    continue
                i, j = ij
                visibility_graph.add_edge(i, j)
    else:
        for item in tqdm(combinations, total=len(combinations), desc=desc):
            is_edge, ij = _process_intersection(item)
            if not is_edge:
                continue

            i, j = ij
            visibility_graph.add_edge(i, j)

    return visibility_graph
