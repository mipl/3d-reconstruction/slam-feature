import json

import torch
import numpy as np
import quaternion

from tenviz.pose import PoseDict, Pose


def write_pose_json(pose_dict, json_file):
    json_dict = {}

    for time, pose in pose_dict.items():
        quat = pose.get_quaternion()
        trans = pose.get_translation()
        json_dict[time] = {
            'qw': quat[0],
            'qx': quat[1],
            'qy': quat[2],
            'qz': quat[3],
            'tx': trans[0],
            'ty': trans[1],
            'tz': trans[2]
        }

    json.dump(json_dict, json_file, indent=2)


def read_pose_json(json_file):
    pose_dict = PoseDict()

    json_dict = json.load(json_file)

    for time, pose in json_dict.items():
        pose_dict[float(time)] = Pose(
            pose['qw'], pose['qx'], pose['qy'], pose['qz'],
            pose['tx'], pose['ty'], pose['tz'])

    return pose_dict
