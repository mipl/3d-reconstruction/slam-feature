"""RFlow nodes for dealing with data stuff.
"""

import torch
import rflow

# pylint: disable=no-self-use


class CreateDatasetVisibilityGraph(rflow.Interface):
    """Create a visibility graph from a dataset
    """

    def evaluate(self, resource, dataset, frustum_intersection_ratio=0.25,
                 camera_max_angle=0.25,
                 max_frames=5000):
        """Evaluate."""
        from slamfeat.data.visibility_graph import (
            create_visibility_graph_from_dataset)

        visibility_graph = create_visibility_graph_from_dataset(
            dataset,
            frustum_intersection_ratio=frustum_intersection_ratio,
            camera_max_angle=camera_max_angle,
            max_frames=max_frames)

        return resource.pickle_dump(visibility_graph)

    def load(self, resource):
        """Load"""
        return resource.pickle_load()


class CreateCorrespondenceDataset(rflow.Interface):
    """Creates a dataset of concatened `corresp_dataset_class` instances.
    """

    def evaluate(self, point_dist_thresh=0.01,
                 scale=None,
                 ds1=None, ds1_vis_graph=None, ds1_bg_ids=None,
                 ds2=None, ds2_vis_graph=None, ds2_bg_ids=None,
                 ds3=None, ds3_vis_graph=None, ds3_bg_ids=None,
                 ds4=None, ds4_vis_graph=None, ds4_bg_ids=None,
                 ds5=None, ds5_vis_graph=None, ds5_bg_ids=None,
                 ds6=None, ds6_vis_graph=None, ds6_bg_ids=None,
                 ds7=None, ds7_vis_graph=None, ds7_bg_ids=None,
                 ds8=None, ds8_vis_graph=None, ds8_bg_ids=None,
                 ds9=None, ds9_vis_graph=None, ds9_bg_ids=None):
        """Eval"""
        from torch.utils.data import ConcatDataset

        from slamfeat.data.augmentation import CorrespondenceAugmentation
        from slamfeat.data.rgbdcorrespondencedataset import RGBDCorrespondenceDataset

        datasets = [ds1, ds2, ds3, ds4, ds5,
                    ds6, ds7, ds8, ds9]
        bg_ids_list = [ds1_bg_ids, ds2_bg_ids, ds3_bg_ids, ds4_bg_ids,
                       ds5_bg_ids, ds6_bg_ids, ds7_bg_ids, ds8_bg_ids,
                       ds9_bg_ids]
        vis_graph_list = [ds1_vis_graph, ds2_vis_graph, ds3_vis_graph, ds4_vis_graph,
                          ds5_vis_graph, ds6_vis_graph, ds7_vis_graph, ds8_vis_graph,
                          ds9_vis_graph]

        # TODO: make a parameter
        augmentation = CorrespondenceAugmentation()
        datasets = [(ds, vis_graph, bg_ids) for ds, vis_graph, bg_ids in
                    zip(datasets, vis_graph_list, bg_ids_list)
                    if ds is not None]

        corresp_datasets = [
            RGBDCorrespondenceDataset(dset, vis_graph, point_dist_thresh=point_dist_thresh,
                                      scale=scale, bg_ids=bg_ids,
                                      augmentation_fn=augmentation)
            for dset, vis_graph, bg_ids in datasets]

        return ConcatDataset(corresp_datasets)


class SaveDatasetToDB(rflow.Interface):
    """Node for saving a dataset to LMDB.
    """

    def non_collateral(self):
        """num_workers has no side effect on this task.
        """
        return ["num_workers"]

    def evaluate(self, resource, dataset, num_workers=1, num_samples=None):
        """eval"""
        from shutil import rmtree
        from slamfeat.data.cache import save_dataset_to_db

        rmtree(resource.filepath, ignore_errors=True)

        save_dataset_to_db(resource.filepath, dataset, num_workers,
                           num_samples=num_samples)

        return self.load(resource)

    def load(self, resource):
        """load"""
        from slamfeat.data.cache import LMDBDataset
        return LMDBDataset(resource.filepath)


class LoadGeo(rflow.Interface):
    """Load a 3D object from a file.
    """

    def evaluate(self, resource, transform=None, center=False, is_point_cloud=False):
        """Eval
        """
        return self.load(resource, transform, center, is_point_cloud)

    def load(self, resource, transform, center, is_point_cloud):
        """Load
        """
        import tenviz.io
        from slamtb.pointcloud import PointCloud
        from slamtb.camera import RigidTransform

        mesh = tenviz.io.read_3dobject(resource.filepath)

        if center:
            mesh.verts -= mesh.verts.mean(0)

        if transform is not None:
            transform = RigidTransform(torch.tensor(transform,
                                                    dtype=torch.float32))
            transform.inplace(mesh.verts)
            transform.inplace_normals(mesh.normals)

        if is_point_cloud:
            mesh = PointCloud(
                mesh.verts, normals=mesh.normals, colors=mesh.colors)

        return mesh


class SamplePCLFromMesh(rflow.Interface):
    def evaluate(self, resource, mesh, num_points):
        from slamtb.geometry import sample_points_in_mesh
        from slamtb.pointcloud import PointCloud
        import tenviz

        points, normals = sample_points_in_mesh(mesh.verts, mesh.normals,
                                                mesh.faces, num_points)

        tenviz.io.write_3dobject(resource.filepath,
                                 verts=points.numpy(),
                                 normals=normals.numpy())
        return PointCloud(points, normals=normals)

    def load(self, resource):
        import tenviz
        from slamtb.pointcloud import PointCloud

        geo = tenviz.io.read_3dobject(resource.filepath)
        return PointCloud(geo.verts, normals=geo.normals)


class SamplePCLFromDataset(rflow.Interface):
    """Sample a set of points from the point clouds of each frame of dataset.
    """

    def evaluate(self, resource, dataset, num_samples, sample_per_snap):
        """Args:

            resource (:obj:`rflow.FSResource`): 3D file to write the object.

            dataset: The dataset

            num_samples (int): number of frames to extract.

            sample_per_snap (int): number of points to take from one
             frame point cloud.

        """
        import numpy as np
        from tqdm import tqdm
        import tenviz

        from slamtb.frame import FramePointCloud
        from slamtb.pointcloud import stack_pcl

        pcl_list = []

        for idx in tqdm(np.linspace(0, len(dataset) - 1, num_samples)):
            frame = dataset[int(idx)]

            pcl = FramePointCloud.from_frame(
                frame).unordered_point_cloud(world_space=True)
            which_points = np.random.choice(
                pcl.size, sample_per_snap, replace=False)

            pcl_list.append(pcl.index_select(which_points))

        dense_pcl = stack_pcl(pcl_list)

        tenviz.io.write_3dobject(
            resource.filepath, verts=dense_pcl.points.numpy(),
            colors=dense_pcl.colors.numpy(),
            normals=dense_pcl.normals.numpy())

        return dense_pcl

    def load(self, resource):
        """Load the sampling"""
        import tenviz
        from slamtb.pointcloud import PointCloud

        geo = tenviz.io.read_3dobject(resource.filepath)
        return PointCloud(geo.verts, geo.colors, geo.normals)
