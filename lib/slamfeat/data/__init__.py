"""Loading and writing data. Triplet datasets.
"""

# pylint: disable=unused-import

from .rgbdcorrespondencedataset import RGBDCorrespondenceDataset
