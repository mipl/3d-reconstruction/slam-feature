"""Caching data utilities.
"""

import pickle
from multiprocessing import pool

import cachetools
import lmdb
from tqdm import tqdm


class FrameCache:
    """Simple cache from frames.
    """
    def __init__(self, dataset, cache_size=42):
        self.frame_cache = cachetools.LFUCache(cache_size)
        self.dataset = dataset

    def __getitem__(self, idx):
        if idx in self.frame_cache:
            return self.frame_cache[idx]

        frame = self.dataset[idx]
        self.frame_cache[idx] = frame
        return frame

    def __len__(self):
        return len(self.dataset)

class LMDBDataset:
    """A dataset saved into a LMDB.
    """
    def __init__(self, filepath):
        self.dbenv = lmdb.open(str(filepath), readonly=True)
        self.txn = self.dbenv.begin(write=False)
        self._size = self.txn.stat()['entries']

    def __getitem__(self, idx):
        value = self.txn.get(str(idx).encode())
        entry = pickle.loads(value)
        return entry

    def __len__(self):
        return self._size


_DATASET = None


class _LMDBWriter:
    """Auxiliary writer for commiting periodically to the database.
    """

    def __init__(self, db_env, commit_count=1024):
        self.db_env = db_env
        self.commit_count = commit_count

        self._txn = db_env.begin(write=True)
        self._count = 0

    def put(self, idx, entry):
        """Adds a `entry` at the index (`idx`).
        """
        self._txn.put(str(idx).encode(),
                      pickle.dumps(entry, pickle.HIGHEST_PROTOCOL))

        self._count += 1
        if self._count >= self.commit_count:
            self._txn.commit()
            self._txn = self.db_env.begin(write=True)
            self._count = 0

    def close(self):
        """Closes the database connection.
        """
        self._txn.commit()
        self.db_env.close()


def _read_dataset(i):
    try:
        return _DATASET[i]
    except Exception as exp:
        print("Caught exception {}".format(exp))
    return None


def save_dataset_to_db(filepath, dataset, num_workers=None, num_samples=None):
    """Writes a dataset into a LMDB database file. Use LMDBDataset to read
    it.
    """
    global _DATASET

    db_env = lmdb.open(str(filepath), map_size=1024*1024*1024*10)
    _DATASET = dataset

    writer = _LMDBWriter(db_env)

    size = len(dataset) if num_samples is None else num_samples
    if num_workers is None or num_workers > 1:
        with pool.Pool(num_workers) as workers:
            wmap = workers.imap(_read_dataset, range(size))
            pbar = tqdm(total=size)
            idx = 0
            while True:
                try:
                    entry = next(wmap)
                except StopIteration:
                    break

                if entry is None:
                    continue
                writer.put(idx, entry)
                idx += 1
                pbar.update()
    else:
        idx = 0
        for i in tqdm(range(size), total=size):
            try:
                entry = dataset[i]
                if entry is not None:
                    writer.put(idx, entry)
                    idx += 1
            except Exception as exp:
                print(exp)

    writer.close()
