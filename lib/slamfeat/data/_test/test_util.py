import unittest

import slamfeat.data.util


class TestUtil(unittest.TestCase):
    def test_concated_dataset(self):
        dataset = slamfeat.data.util.ConcatedDataset(
            [list(range(10)), list(range(10, 20))])

        self.assertEqual(20, len(dataset))
        dataset[10]
        entries = [dataset[i] for i in range(len(dataset))]
        self.assertEqual(list(range(0, 20)), entries)
