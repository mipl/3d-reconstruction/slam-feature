import torch

from slamfeat._cslamfeat import PointCloud, FrameCrop


def create_point_cloud(pcl, depth_image):
    sf_frame = PointCloud()
    sf_frame.mask = pcl.mask
    sf_frame.points = pcl.points
    sf_frame.colors = pcl.colors
    sf_frame.depths = torch.from_numpy(depth_image)

    return sf_frame


def create_frame_crop(crop_width, crop_height):
    crop = FrameCrop()
    crop.colors = torch.zeros(crop_height, crop_width, 3, dtype=torch.uint8)
    crop.depths = torch.zeros(crop_height, crop_width, dtype=torch.int32)

    return crop
