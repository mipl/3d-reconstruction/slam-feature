from pathlib import Path
import copy
from collections import namedtuple

import numpy as np
import tenviz
from slamtb.frame import Frame

_SHADER_DIR = Path(__file__).parent / "shaders"


RasterResult = namedtuple("RasterResult",
                          ["positions", "normals", "colors", "mask", "primitives",
                           "segmentation"])


class MeshRaster:
    """Render a given mesh with the same camera properties as real one.
    """

    def __init__(self, geo):
        self.context = tenviz.Context()

        with self.context.current():
            self.draw = tenviz.DrawProgram(tenviz.DrawMode.Triangles,
                                           _SHADER_DIR / "raster.vert",
                                           _SHADER_DIR / "raster.frag")

            self.draw['Modelview'] = tenviz.MatPlaceholder.Modelview
            self.draw['NormalModelview'] = tenviz.MatPlaceholder.NormalModelview
            self.draw['ProjModelview'] = tenviz.MatPlaceholder.ProjectionModelview
            self.draw['in_position'] = tenviz.buffer_from_tensor(geo.verts)
            self.draw['in_normal'] = tenviz.buffer_from_tensor(geo.normals)
            self.draw['in_color'] = tenviz.buffer_from_tensor(
                geo.colors, normalize=True)
            if geo.object_ids is not None:
                self.draw['in_object_id'] = tenviz.buffer_from_tensor(
                    geo.object_ids, integer_attrib=True)

            self.draw.indices.from_tensor(geo.faces)
            self.draw.set_bounds(geo.verts)

            self.framebuffer = tenviz.create_framebuffer(
                {0: tenviz.FramebufferTarget.RGBFloat,
                 1: tenviz.FramebufferTarget.RGBFloat,
                 2: tenviz.FramebufferTarget.RGBUint8,
                 3: tenviz.FramebufferTarget.RGBInt32,
                 })

    def raster(self, frame_info, width, height):
        proj = tenviz.Projection.from_intrinsics(
            frame_info.kcam.matrix, 0.1, 5.0)
        proj_mtx = proj.to_matrix().astype(np.float32)

        view = frame_info.rt_cam.opengl_view_cam.float()

        self.context.resize(width, height)
        self.context.clear_color = np.array([0, 0, 0, 0])
        self.context.render(proj_mtx, view.numpy(),
                            self.framebuffer, [self.draw])

        attaches = self.framebuffer.get_attachs()
        with self.context.current():
            pos = attaches[0].to_tensor()
            normals = attaches[1].to_tensor()
            colors = attaches[2].to_tensor()
            mask_prim_segm = attaches[3].to_tensor()

        mask = mask_prim_segm[:, :, 0].bool()
        prim = mask_prim_segm[:, :, 1]
        segm = mask_prim_segm[:, :, 2]

        self.context.collect_garbage()
        return RasterResult(pos.cpu(), normals.cpu(), colors.cpu(),
                            mask.cpu(), prim.cpu(), segm.cpu())

    def raster_to_frame(self, frame_info, width, height, depth_scale=1.0, depth_bias=0.0):
        rresult = self.raster(frame_info, width, height)

        depth_image = rresult.positions[:, :, 2].abs().numpy()
        depth_image[rresult.mask.squeeze() == 0] = 0.0
        depth_image = (depth_image*depth_scale).astype(np.int32)

        color_image = rresult.colors.numpy()

        normal_image = rresult.normals.numpy()
        normal_image[:, :, 2] *= -1  # Verify this

        frame_info = copy.copy(frame_info)
        frame_info.depth_scale = 1.0/depth_scale
        frame_info.depth_bias = depth_bias

        frame = Frame(frame_info, depth_image, color_image,
                      normal_image=normal_image,
                      seg_image=rresult.segmentation.numpy())

        return frame


class GroundTruthDepthDataset:
    """Wraps an another dataset, replacing the original depth image with
    one from rendering its ground truth mesh.
    """

    def __init__(self, gt_mesh, original_dataset, depth_scale=5000.0, depth_bias=0.0):
        self._raster = MeshRaster(gt_mesh)
        self.gt_mesh = gt_mesh
        self.original_dataset = original_dataset
        self.depth_scale = depth_scale
        self.depth_bias = depth_bias

        self.ground_truth_model_path = None

    def __getitem__(self, idx):
        original_frame = self.original_dataset[idx]

        rgb_image = original_frame.rgb_image
        frame = self._raster.raster_to_frame(original_frame.info,
                                             rgb_image.shape[1], rgb_image.shape[0],
                                             depth_scale=self.depth_scale,
                                             depth_bias=self.depth_bias)

        frame.rgb_image = rgb_image
        return frame

    def __len__(self):
        return len(self.original_dataset)


def _test_segmentation():
    from slamtb.data.scenenn import load_scenenn

    here = Path(__file__).parent

    dataset = load_scenenn(here / "../../../data/scenenn/SceneNN/oni/045.oni",
                           here / "../../../data/scenenn/SceneNN/045/trajectory.log")
    mesh = tenviz.io.read_3dobject(
        here / "../../../data/scenenn/SceneNN/045/045.ply")
    gt_dataset = GroundTruthDepthDataset(mesh, dataset, depth_scale=5000.0,
                                         depth_bias=0.0)


if __name__ == '__main__':
    from fire import Fire

    Fire(_test_segmentation, name="segmentation")
