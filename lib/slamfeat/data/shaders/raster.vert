#version 420

in vec4 in_position;
in vec3 in_normal;
in vec3 in_color;
in int in_object_id;

uniform mat4 Modelview;
uniform mat3 NormalModelview;
uniform mat4 ProjModelview;

out Point {
  vec3 position;
  vec3 normal;
  vec3 color;
  flat int object_id;
} point;

void main() {
  gl_Position = ProjModelview*in_position;
  point.position = (Modelview*in_position).xyz;
  point.normal = NormalModelview*in_normal;
  point.color = in_color;
  point.object_id = in_object_id;
}
