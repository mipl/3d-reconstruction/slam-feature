#version 420

in Point {
  vec3 position;
  vec3 normal;
  vec3 color;
  flat int object_id;
} point;

layout (location=0) out vec3 frag_pos;
layout (location=1) out vec3 frag_normal;
layout (location=2) out vec3 frag_color;
layout (location=3) out ivec3 mask_primitive_object;

void main() {
  frag_pos = point.position;
  frag_normal = point.normal;
  frag_color = point.color;
  mask_primitive_object.x = 1;
  mask_primitive_object.y = gl_PrimitiveID;
  mask_primitive_object.z = point.object_id;
}
