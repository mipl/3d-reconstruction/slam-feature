"""Visualization RFlow nodes.
"""
# pylint: disable=no-self-use

import torch

import rflow


class ViewRGBDDataset(rflow.Interface):
    """View a RGBD dataset from slamtb
    """

    def evaluate(self, dataset, title, identify=False):
        """Eval"""
        from slamtb.viz.datasetviewer import DatasetViewer
        from slamtb.data import set_start_at_eye

        if identify:
            dataset = set_start_at_eye(dataset)

        viewer = DatasetViewer(dataset, title, 100)

        viewer.run()


class ViewCorrespondenceDataset(rflow.Interface):
    """View a correspondence dataset
    """

    def evaluate(self, dataset, title="Correspondence Dataset", random_sample=True):
        """Eval"""
        from slamfeat.viz.dataset import CorrespondenceDatasetViewer

        viewer = CorrespondenceDatasetViewer(dataset, 3, title=title,
                                             random_sample=random_sample)
        viewer.run()


class ViewPCL(rflow.Interface):
    """View a point cloud and, optionally, add a mesh."""

    def evaluate(self, pcl, ref_mesh=None, title="Viewer", screenshort_prefix="pcl-",
                 invert_y=True):
        """Eval"""
        import torch
        from slamtb.viz import geoshow

        geos = [pcl]
        if ref_mesh is not None:
            geos.append(ref_mesh)

        geoshow(geos, point_size=2, title=title,
                screenshot_prefix=screenshort_prefix,
                invert_y=invert_y,
                background=torch.tensor(
                    [0xd2, 0xd3, 0xc9, 0xff]).float()/255.0)


class ViewTrajectory(rflow.Interface):
    """View camera trajectory"""

    def evaluate(self, dataset, trajectory=None, title=None):
        from slamtb.metrics.trajectory import (
            align_trajectories, set_start_at_identity)
        from slamtb.viz.trajectoryviewer import TrajectoryViewer

        gt_trajectory = {dataset.get_info(i).timestamp: dataset.get_info(
            i).rt_cam for i in range(len(dataset))}

        if trajectory is not None:
            gt_trajectory = dict(list(gt_trajectory.items())[:len(trajectory)])
            # trajectory = set_start_at_identity(trajectory)
            # trajectory = align_trajectories(gt_trajectory, trajectory, 0.0001)
            trajs = [gt_trajectory, trajectory]
            colors = ["Greens", "Reds"]
        else:
            gt_trajectory = dict(list(gt_trajectory.items())[::20])
            trajs = [gt_trajectory]
            colors = ["Greens"]
        viewer = TrajectoryViewer(trajs,
                                  colormaps=colors,
                                  title=title)

        print("Ground truth in blue")
        print("Estimated in red")
        viewer.run()


class ViewPoseGraphTrajectory(rflow.Interface):
    """View camera trajectory"""

    def evaluate(self, pose_graph1, pose_graph2, title=None):
        from slamtb.trajectory import Trajectory
        from slamtb.viz.trajectoryviewer import TrajectoryViewer

        trajectory1 = Trajectory.from_posegraph(pose_graph1)
        trajectory2 = Trajectory.from_posegraph(pose_graph2)
        # trajectory2 = trajectory2.aligned_to(trajectory1)

        viewer = TrajectoryViewer([trajectory1, trajectory2],
                                  colormaps=["Blues", "Reds"],
                                  title=title)

        print("Ground truth in blue")
        print("Estimated in red")
        viewer.run()


class ViewFeatures(rflow.Interface):
    """View features attached to a point cloud.
    """

    def evaluate(self, dataset, model, frames,
                 num_pca_components=3,
                 num_tsne_components=None,
                 num_color_clusters=None,
                 num_color_fit_samples=None,
                 title="Feature as colors"):
        """
        Args:

            dataset (): Input dataset
        """
        import numpy as np

        from slamtb.pointcloud import PointCloud
        from slamtb.viz import geoshow

        from slamfeat.viz.color import FeatureColorizer

        colorizer = FeatureColorizer(num_pca_components=num_pca_components,
                                     num_tsne_components=num_tsne_components,
                                     num_color_clusters=num_color_clusters)
        frame0 = dataset[frames[0]]
        pcl0, mask0 = PointCloud.from_frame(frame0)
        feats0 = model.extract_frame_features(frame0)
        feats0 = feats0[:, mask0].transpose(1, 0).cpu().numpy()

        frame1 = dataset[frames[1]]
        pcl1, mask1 = PointCloud.from_frame(frame1)
        feats1 = model.extract_frame_features(frame1)
        feats1 = feats1[:, mask1].transpose(1, 0).cpu().numpy()

        feats = np.vstack((feats0, feats1))
        colorizer.fit(feats, num_color_fit_samples)

        pcl0_a = pcl0.clone()
        pcl0_a.colors = torch.from_numpy(colorizer.colorize(feats0))

        pcl1_a = pcl1.clone()
        pcl1_a.colors = torch.from_numpy(colorizer.colorize(feats1))

        geoshow([pcl0, pcl1, pcl0_a, pcl1_a], invert_y=True, title=title)


class ViewFeatureDifferences(rflow.Interface):
    def evaluate(self, dataset, model, frames, title="Feature as colors"):
        import matplotlib.pyplot as plt
        from scipy.spatial.ckdtree import cKDTree

        from slamtb.pointcloud import PointCloud
        from slamtb.viz import geoshow

        frame0 = dataset[frames[0]]
        frame1 = dataset[frames[1]]

        pcl0, mask0 = PointCloud.from_frame(frame0, world_space=True)
        pcl1, mask1 = PointCloud.from_frame(frame1, world_space=True)

        feats0 = model.extract_frame_features(frame0).cpu()
        feats0 = feats0[:, mask0].transpose(1, 0)

        feats1 = model.extract_frame_features(frame1).cpu()
        feats1 = feats1[:, mask1].transpose(1, 0)

        tree = cKDTree(pcl0.points.cpu().numpy())
        _, index = tree.query(pcl1.points.numpy(), k=1,
                              distance_upper_bound=0.05)

        valid = index < tree.n

        feat_diff = (feats0[index[valid], :] - feats1[valid, :]).norm(2, dim=1)
        print("Mean difference: ", feat_diff.mean())
        print("Std difference: ", feat_diff.std())
        print("Min difference: ", feat_diff.min())
        print("Max difference: ", feat_diff.max())
        feat_diff /= feat_diff.max()

        cmap = plt.get_cmap("plasma", 1000)

        colors = torch.from_numpy(cmap(feat_diff.numpy())[:, :3]*255).byte()

        pcl0_a = pcl0.clone()
        pcl0_a.colors[index[valid], :] = colors

        pcl1_a = pcl1.clone()
        pcl1_a.colors[valid, :] = colors

        geoshow([pcl0_a, pcl1_a], invert_y=True, title=title)


class ViewPoseGraph(rflow.Interface):
    """View pose graph.
    """

    def evaluate(self, pose_graph, submaps=None, traverse_nodes=None,
                 invert_y=None,
                 title="Pose graph"):
        """Evaluate"""
        import math
        from slamtb.camera import RigidTransform
        from slamtb.viz.posegraphviewer import PoseGraphViewer

        if submaps is not None:
            for node_id, node in pose_graph.items():
                node.geometry = submaps[node_id].geometry
        viewer = PoseGraphViewer(pose_graph, title=title, node_traverse=traverse_nodes,
                                 kcam_far=0.1, show_grid=False)
        viewer.ctx.clear_color = torch.tensor(
            [0xd2, 0xd3, 0xc9, 0xff]).float()/255.0
        viewer.run()


class ViewPoseGraphFeatures(rflow.Interface):
    """Show the features from fragments using kmeans and PCA.
    """

    def evaluate(self, pose_graph, nodes,
                 num_pca_components=None, num_color_clusters=None,
                 title="Feature as colors"):
        """Evaluate
        """
        import tenviz
        from slamtb.viz.surfelrender import show_surfels

        from slamfeat.viz.color import FeatureColorizer
        from slamtb.posegraph import traverse_pose_graph

        fragments = [pose_graph[node].geometry for node in nodes]
        features = torch.cat([
            frag.features.transpose(1, 0) for frag in fragments])
        device = features.device
        colorizer = FeatureColorizer(num_pca_components=num_pca_components,
                                     num_color_clusters=num_color_clusters)

        colorizer.fit(features.cpu().numpy())

        for frag in fragments:
            frag.colors = torch.from_numpy(
                colorizer.colorize(frag.features)).to(device)

        scene = []

        def _add_to_scene(_, i, transform, __):
            i_frag = fragments[i]
            i_frag.itransform(transform.float())
            scene.append(i_frag)

        traverse_pose_graph(pose_graph, _add_to_scene, nodes=nodes)

        gl_context = tenviz.Context()
        show_surfels(gl_context, scene, title=title, invert_y=True)


class ViewPoseGraphFeatureDifferences(rflow.Interface):
    def evaluate(self, fragments, pose_graph, nodes):
        from slamfeat.viz.posegraph import view_pose_graph_feature_differences

        view_pose_graph_feature_differences(fragments, pose_graph, nodes)


class ViewPoseGraphDenseMatching(rflow.Interface):
    """View matchings between dense features by
    coloring the same across pose graph nodes.
    """

    def evaluate(self, pose_graph, nodes, title="Feature Matchings"):
        """
        Args:

            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             Pose graph with ground truth transformations.

            nodes (List[int]): Nodes to show.
        """
        from slamfeat.viz.posegraph import view_pose_graph_dense_features

        view_pose_graph_dense_features(
            pose_graph, nodes, title=title)


class ViewPoseGraphSparseMatching(rflow.Interface):
    def evaluate(self, pose_graph, nodes, title="Feature Matchings", weight_threshold=0,
                 # max_feat_distance=200
                 # max_feat_distance=0.8
                 max_feat_distance=None
                 ):
        import numpy as np
        from slamfeat.viz.posegraph import view_pose_graph_sparse_features

        if max_feat_distance is None:
            max_feat_distance = np.inf
        view_pose_graph_sparse_features(
            pose_graph, nodes, title=title, weight_threshold=weight_threshold,
            max_feat_distance=max_feat_distance)
