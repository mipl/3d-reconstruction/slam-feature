"""Pose graph extra visualization functions
"""

import matplotlib.pyplot as plt
import numpy as np
import torch
from scipy.spatial import cKDTree

from slamtb.viz.posegraphviewer import PoseGraphViewer


def view_pose_graph_sparse_features(pose_graph, nodes, title="Feature Matchings",
                                    weight_threshold=8, max_feat_distance=np.inf):
    """Draw matching between sparse features with colors.
    """
    frag_i = pose_graph[nodes[0]].geometry

    device = frag_i.device
    selected_i = frag_i.sparse_features.weights().numpy() > weight_threshold
    sparse_index_i = frag_i.sparse_features.keys()[selected_i]

    cmap = plt.get_cmap("tab20", selected_i.sum())

    def _query_cmap(x):
        return torch.from_numpy((cmap(x)[:, :3]*255).astype(np.uint8)).to(device)

    color_i = _query_cmap(range(selected_i.sum()))
    frag_i.colors[sparse_index_i] = color_i
    frag_i.radii[sparse_index_i] *= 5.0

    for j in nodes[1:]:
        frag_j = pose_graph[j].geometry

        tree_i = cKDTree(
            frag_i.sparse_features.descriptors().numpy()[selected_i, :])

        selected_j = frag_j.sparse_features.weights().numpy() > weight_threshold
        dist, index = tree_i.query(
            frag_j.sparse_features.descriptors().numpy()[selected_j],
            distance_upper_bound=max_feat_distance)

        valid = index < tree_i.n
        print("Mean distance:", dist[valid].mean(), "N:", valid.sum())

        sparse_index_j = frag_j.sparse_features.keys()[selected_j]
        frag_j.radii[sparse_index_j] *= 5.0
        frag_j.colors[sparse_index_j[valid], :] = _query_cmap(index[valid])

        frag_i = frag_j
        selected_i = selected_j

    PoseGraphViewer(
        pose_graph, title=title, node_traverse=nodes,
    ).run()


def view_pose_graph_dense_features(pose_graph, nodes, title="Feature Matchings"):
    """Draw matching between dense features with colors.
    """
    frag_i = pose_graph[nodes[0]].geometry
    device = frag_i.device

    cmap = plt.get_cmap("hsv", frag_i.size)

    def _query_cmap(x):
        return torch.from_numpy((cmap(x)[:, :3]*255).astype(np.uint8)).to(device)

    frag_i.colors = _query_cmap(range(frag_i.size))
    for j in nodes[1:]:
        frag_j = pose_graph[j].geometry

        tree_i = cKDTree(frag_i.features.transpose(1, 0).cpu().numpy())

        _, index = tree_i.query(frag_j.features.transpose(1, 0).cpu().numpy())
        valid = index < tree_i.n

        frag_j.colors[valid, :] = _query_cmap(index[valid])

        frag_i = frag_j

    PoseGraphViewer(pose_graph, title=title, node_traverse=nodes).run()
