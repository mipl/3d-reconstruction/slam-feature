"""Viewer tools for dataset.
"""
import random

import cv2
import numpy as np
import torch

from torchvision.utils import make_grid
from torchvision.transforms.functional import to_tensor, to_pil_image

from slamfeat.data.datatype import RGBDCorrespondencePair


class CorrespondenceDatasetViewer:
    """Viewer for correspondence datasets
    """

    def __init__(self, dataset, rows, depth_max=5,
                 title="Correspondence Viewer", random_sample=False):
        self.dataset = dataset
        self.rows = rows
        self.depth_max = depth_max
        self.title = title
        self.show_mask = False
        self.random_sample = random_sample

    def _ensure_rgb(self, img, mask=None):
        img = img.numpy()
        if img.ndim == 2:
            # depth image
            img = img / self.depth_max * 255.0
            img = img.astype(np.uint8)
            img = np.dstack([img, img, img])

        if self.show_mask and mask is not None:
            return to_tensor(img) * mask

        return to_tensor(img)

    def _render_matches(self, item):
        valid_posv_idx = item.anc_posv_index >= 0
        anc_idx = item.anc_posv_index[valid_posv_idx]
        dock_idx = item.dock_posv_index[valid_posv_idx]

        out_anc_rgb = torch.zeros_like(item.anc_rgb)
        out_anc_rgb.view(-1, 3)[anc_idx] = item.dock_rgb.view(-1, 3)[dock_idx]

        out_dock_rgb = torch.zeros_like(item.dock_rgb)
        out_dock_rgb.view(-1, 3)[dock_idx] = item.anc_rgb.view(-1, 3)[anc_idx]

        return self._ensure_rgb(out_anc_rgb), self._ensure_rgb(out_dock_rgb)

    def _render_non_matches(self, item):
        valid_negv_idx = item.anc_negv_index >= 0
        anc_idx = item.anc_negv_index[valid_negv_idx]
        dock_idx = item.dock_negv_index[valid_negv_idx]

        out_anc_rgb = torch.zeros_like(item.anc_rgb)
        out_anc_rgb.view(-1, 3)[dock_idx] = item.dock_rgb.view(-1, 3)[dock_idx]

        out_dock_rgb = torch.zeros_like(item.dock_rgb)
        out_dock_rgb.view(-1, 3)[anc_idx] = item.anc_rgb.view(-1, 3)[anc_idx]

        return self._ensure_rgb(out_anc_rgb), self._ensure_rgb(out_dock_rgb)

    def _render_corresp_pair(self, item):
        anc_pos_rgb, dock_pos_rgb = self._render_matches(item)
        anc_neg_rgb, dock_neg_rgb = self._render_non_matches(item)
        patches = [
            self._ensure_rgb(item.anc_rgb),
            self._ensure_rgb(item.dock_rgb),
            anc_pos_rgb,
            dock_pos_rgb,
            anc_neg_rgb,
            dock_neg_rgb
        ]

        return make_grid(patches, len(patches))

    def _draw_positive_sample(self, item):
        if item.pos_index is None:
            return self._ensure_rgb(item.pos_rgb, item.pos_mask)

        valid_idxs = item.pos_index[item.pos_index >= 0].long()
        out_rgb = torch.zeros_like(item.pos_rgb)
        out_rgb.view(-1, 3)[valid_idxs] = item.pos_rgb.view(-1, 3)[valid_idxs]

        out_depth = torch.zeros_like(item.pos_depth)
        out_depth.view(-1)[valid_idxs] = item.pos_depth.view(-1)[valid_idxs]

        return self._ensure_rgb(out_rgb, None), self._ensure_rgb(out_depth, None)

    def _render_corresp_triplet(self, item):
        pos_rgb, pos_depth = self._draw_positive_sample(item)
        patches = [
            self._ensure_rgb(item.anc_rgb, item.pos_mask),
            self._ensure_rgb(item.anc_depth, item.pos_mask),
            pos_rgb,
            pos_depth,
            self._ensure_rgb(item.neg_rgb, item.neg_mask),
            self._ensure_rgb(item.neg_depth, item.neg_mask)
        ]

        return make_grid(patches, len(patches))

    def run(self):
        """Execute the viewer"""
        dataset_size = len(self.dataset)

        print("Keys:")
        print("n: next page")
        print("b: previous page")
        print("m: toggle masking")

        cv2.namedWindow(self.title, cv2.WINDOW_NORMAL)

        page_idx = 0
        prev_page_idx = -1

        _ensure_rgb = self._ensure_rgb
        while True:
            if prev_page_idx != page_idx:
                start = page_idx * self.rows
                images = []

                for i in range(start, min(start + self.rows, dataset_size)):
                    if not self.random_sample:
                        item = self.dataset[i]
                    else:
                        item = self.dataset[random.randint(0, dataset_size)]

                    if isinstance(item, RGBDCorrespondencePair):
                        img = self._render_corresp_pair(item)
                    else:
                        img = self._render_corresp_triplet(item)

                    images.append(img)

                page = make_grid(images, 1)
                page = np.array(to_pil_image(page))

                page = cv2.cvtColor(page, cv2.COLOR_RGB2BGR)
                prev_page_idx = page_idx

            cv2.imshow(self.title, page)

            key = cv2.waitKey(-1)
            if key == 27:
                break

            key = chr(key & 0xff)
            if key == 'n':
                page_idx = min(page_idx + 1, int(dataset_size / self.rows) - 1)
            elif key == 'b':  # "brevious"
                page_idx = max(page_idx - 1, 0)
            elif key == 'm':
                self.show_mask = not self.show_mask
