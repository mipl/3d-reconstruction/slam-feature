from pathlib import Path

import torch
import numpy as np
import quaternion
from tqdm import tqdm
import tenviz


_SHADER_DIR = Path(__file__).parent / "shaders"


def create_draw_prg(mesh_obj, segmentation=False):
    program = tenviz.DrawProgram(tenviz.DrawMode.Quads,
                                 vert_shader_file=_SHADER_DIR / "mesh.vert",
                                 frag_shader_file=_SHADER_DIR / "mesh.frag")
    program['ProjModelview'] = tenviz.MatPlaceholder.ProjectionModelview

    program.indices.from_tensor(mesh_obj.faces)
    program['in_position'] = mesh_obj.verts
    program['in_color'] = mesh_obj.colors

    if segmentation:
        vert_obj_ids = torch.zeros(mesh_obj.verts.size(0), dtype=torch.int32)

        for face_idx, face in tqdm(enumerate(mesh_obj.faces), total=mesh_obj.faces.size(0)):
            for vert_idx in face:
                vert_obj_ids[vert_idx] = mesh_obj.object_ids[face_idx]

        torch.save(vert_obj_ids, "vert_obj_ids.th")
        
        program['in_object_id'] = vert_obj_ids

    program.transform = torch.tensor([[1, 0, 0, 0],
                                      [0, 0, 1, 0],
                                      [0, 1, 0, 0],
                                      [0, 0, 0, 1]],
                                     dtype=torch.float)

    return program
