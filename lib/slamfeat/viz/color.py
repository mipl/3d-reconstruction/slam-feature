"""Coloring utilities for visualization.
"""

import torch
import numpy as np

from MulticoreTSNE import MulticoreTSNE as TSNE
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt


def _as_numpy_features(features):
    if torch.is_tensor(features):
        return features.cpu().permute(1, 0).numpy()
    return features


def _to_normalized_rgb(colors):
    colors = colors.copy()

    for k in range(3):
        minv = colors[:, k].min()
        maxv = colors[:, k].max()
        colors[:, k] = (colors[:, k] - minv)/(maxv - minv)

    return (colors*255).astype(np.uint8)


class FeatureColorizer:
    """Converts multi-channel features into RGB using either PCA,
    Clustering or normalizing between 0-255.
    """

    def __init__(self, num_pca_components=3,
                 num_tsne_components=None,
                 num_color_clusters=None):
        """Specify the color settings. If all parameters are `None´, then only
        number normalization is applied.

        Args:

            num_pca_components (int): Number of PCA components. None to disable it.

            num_color_clusters (int): Number of cluster features. None to disable it.

        """
        self.pca = None
        if num_pca_components is not None:
            self.pca = PCA(n_components=num_pca_components)

        self.tsne = None
        self._tsne_features = None
        if num_tsne_components is not None:
            self.tsne = TSNE(n_components=num_tsne_components, n_jobs=4)

        self.kmeans = None
        self.cmap = None
        if num_color_clusters is not None:
            self.kmeans = KMeans(num_color_clusters)
            self.cmap = plt.get_cmap("tab20", num_color_clusters)

    def fit(self, features, num_samples=None):
        """Fit the PCA and KMeans (if enabled) with a set of features.

        Args:

            features (:obj:`numpy.ndarray` or :obj:`torch.Tensor`):
             Float tensor. If type is numpy then expected shape is
             [NxF]; if torch then it's [FxN].
        """

        features = _as_numpy_features(features)
        if num_samples is not None:
            sampling = np.random.choice(features.shape[0],
                                        num_samples, replace=False)
            features = features[sampling, :]

        if features.shape[1] == 3:
            return

        if self.pca is not None:
            features = self.pca.fit(features).transform(features)

        if self.tsne is not None:
            features = self.tsne.fit_transform(features)
            self._tsne_features = features

        if self.kmeans is not None:
            self.kmeans.fit(features)

    def colorize(self, features):
        """Convert the features into RGB color.

        Args:

            features (:obj:`numpy.ndarray` or :obj:`torch.Tensor`):
             Float tensor. If type is numpy then expected shape is
             [NxF]; if torch then it's [FxN].

        Returns: (:obj:`numpy.ndarray`):
            RGB color array, uint8 [Nx3].
        """

        features = _as_numpy_features(features)

        if features.shape[1] == 3:
            return _to_normalized_rgb(features)

        if self.pca is not None:
            features = self.pca.transform(features)

        if self.tsne is not None:
            if self._tsne_features is None:
                raise RuntimeError("Fit doesn't computed the TSNE")
            features = self._tsne_features

        if self.kmeans is not None:
            features = self.kmeans.predict(features)
            colors = (self.cmap(features)[:, :3]*255).astype(np.uint8)
            return colors

        return _to_normalized_rgb(features)
