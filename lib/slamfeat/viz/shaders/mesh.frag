#version 420

in vec3 frag_color;
in flat int object_id;

layout(location = 0) out vec4 out_frag_color;
layout(location = 1) out ivec3 out_object_id;

void main() {
  out_frag_color = vec4(frag_color/255, 1.0);
  out_object_id = ivec3(object_id, object_id, object_id);
}
