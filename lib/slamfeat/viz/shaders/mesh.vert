#version 420

in vec4 in_position;
in vec3 in_color;
in int in_object_id;

uniform mat4 ProjModelview;

out vec3 frag_color;
out flat int object_id;

void main() {
  gl_Position = ProjModelview*in_position;
  frag_color = in_color;
  object_id = in_object_id;
}
