"""Format color image as dense features.
"""
from enum import Enum

import cv2
from torchvision.transforms.functional import to_tensor
import rflow


class ColorMode(Enum):
    """Available color modes.
    """
    LAB = 0
    GRAY = 1
    RGB = 2


class ColorFeature:
    """Frame feature extractor.
    """

    def __init__(self, color_type=ColorMode.LAB, blur=False, device="cpu:0"):
        self.color_type = color_type
        self.blur = blur
        self.device = device

    def extract_frame_features(self, frame):
        """Extracts the features from the frame.
        """
        features = frame.rgb_image

        if self.blur:
            features = cv2.blur(features, (5, 5))

        if self.color_type == ColorMode.LAB:
            features = cv2.cvtColor(features, cv2.COLOR_RGB2LAB)
        elif self.color_type == ColorMode.GRAY:
            features = cv2.cvtColor(features, cv2.COLOR_RGB2GRAY)

        return to_tensor(features).to(self.device)

    def to(self, device):
        """Pseudo set device.
        """
        self.device = device
        return self

    @property
    def feature_size(self):
        """Channel feature size.
        """
        if self.color_type == ColorMode.GRAY:
            return 1
        return 3


def color_feature_factory(color_type, blur=False, device="cpu:0"):
    """Return a Rflow factory to create :obj:`ColorFeature` instances.
    """
    return rflow.make_factory(ColorFeature, color_type=color_type, blur=blur,
                              device=device)
