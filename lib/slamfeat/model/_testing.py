"""Testing for models.
"""
from pathlib import Path

import torch
import onnx
from onnx import shape_inference

from matplotlib import pyplot as plt
from rflow import open_graph



def _dump_model_info(model, input_channels=3, export_onnx=True):
    x = torch.zeros(1, input_channels, 640, 480)

    base_path = Path(__file__).parent / "reports"
    base_path.mkdir(exist_ok=True)
    outpath = str(base_path / model.__class__.__name__)

    params = model.get_parameters(0.1)

    if not isinstance(params, list):
        params = [{'params': params}]

    print("Learnable param count: ",
          sum(sum(pp.numel() for pp in p['params']) for p in params))
    print("Total param count: ",
          sum(pp.numel() for pp in model.parameters()))

    if export_onnx:
        onnx_outpath = outpath + ".onnx"
        torch.onnx.export(model, x, onnx_outpath)
        onnx.save(onnx.shape_inference.infer_shapes(onnx.load(onnx_outpath)), onnx_outpath)
        print(f"saved {outpath}.onnx")


def _check_feedforwarding(model, input_channels=8):
    model = model.train().cuda()
    print("Forward pass")
    x = torch.zeros((1, input_channels, 480, 640), requires_grad=True).cuda()
    y1 = model(x)
    y2 = model(x)
    y3 = model(x)

    print("{} input output shapes: {} = {}".format(
        model.__class__.__name__, x.shape, y1.shape))

    print("Backward pass")
    (y1 + y2 - y3).norm().backward()


def _check_preprocess(model):
    dataset_g = open_graph(
        Path(__file__).parent / "../../../data/scenenn", "scenenn")

    scene = dataset_g['045.dataset'].call()
    frame = scene[500]

    x = model.preprocess(
        torch.from_numpy(frame.rgb_image),
        torch.from_numpy(frame.depth_image*frame.info.depth_scale))
    x = model.inv_preprocess(x)

    plt.figure()
    plt.title("Inverse preprocessing")
    plt.imshow(x)
    plt.show()


def _check_extract_features(model):
    dataset_g = open_graph(
        Path(__file__).parent / "../../../data/scenenn", "scenenn")

    scene = dataset_g['045.dataset'].call()
    frame = scene[500]

    model = model.eval()
    segm = model.extract_frame_features(frame)

    segm = segm.argmax(0)

    plt.figure()
    plt.imshow(frame.rgb_image)

    plt.figure()
    plt.imshow(segm.cpu().numpy())
    plt.show()


class ModelSuite:
    """Verifies the network definition contracts.
    """

    def __init__(self, model, input_channels=3, export_onnx=True):
        self._model = model
        self.input_channels = input_channels
        self.export_onnx = export_onnx

    def forward(self):
        """Check forward
        """
        _check_feedforwarding(self._model(), self.input_channels)

    def dump(self):
        """Dumps network diagram and dumps its parameter count.
        """
        _dump_model_info(self._model(), self.input_channels, self.export_onnx)

    def preprocess(self):
        """Tests the processing
        """
        _check_preprocess(self._model())

    def feat_extract(self):
        """Tests it as a feature extractor.
        """
        _check_extract_features(self._model())
