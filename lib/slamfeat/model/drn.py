
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models.segmentation import fcn_resnet50

from .preprocessing import (preprocess_torchvision,
                            inv_preprocess_torchvision,
                            MeanStdPreprocess)


class _MyFCNHead(nn.Sequential):
    def __init__(self, in_channels, channels):
        # pylint: disable=unused-variable
        inter_channels = in_channels // 4
        layers = [
            nn.Conv2d(in_channels, channels, 1, padding=1, bias=True),
            # nn.BatchNorm2d(channels),
            # nn.ReLU(),
            # nn.Dropout(0.1),
            nn.Conv2d(inter_channels, channels, 1)
        ]

        super().__init__(*layers)


class DRN(nn.Module):
    """Fully connected network for segmentation using torchvision models.
    """

    _preprocess = MeanStdPreprocess()  # default pytorch

    def __init__(self, architecture="fcn",
                 feature_size=3, keep_classifier=False):
        super().__init__()

        self.backbone = fcn_resnet50(
            pretrained=False, num_classes=feature_size)

        # self.backbone.classifier = _MyFCNHead(
        #    2048, feature_size)

        self._feature_size = feature_size

    def forward(self, x):
        """Forward"""
        input_shape = x.shape[-2:]

        with torch.no_grad():
            features = self.backbone.backbone(x)['out']

        x = self.backbone.classifier(features)
        x = F.interpolate(x, size=input_shape,
                          mode='bilinear', align_corners=False)
        return x

    def extract_frame_features(self, frame):
        """Extract features from slamtb.frame.Frame
        """
        device = next(self.parameters()).device
        x = self.preprocess(torch.from_numpy(
            frame.rgb_image), None).unsqueeze(0).to(device)

        with torch.no_grad():
            return self.eval()(x).squeeze()

    def get_parameters(self, _):
        """Get learnable paramters.
        """
        return self.backbone.classifier.parameters()

    def get_named_parameters(self):
        """Get named parameters
        """
        return self.backbone.classifier.named_parameters()

    @property
    def feature_size(self):
        """Feature channel size.
        """
        return self._feature_size

    @staticmethod
    def preprocess(image, _):
        """Preprocess a RGB image
        """
        return DRN._preprocess.preprocess(image)

    @staticmethod
    def inv_preprocess(tensor):
        """Inverse preprocess from tensor to image.
        """
        return DRN._preprocess.inverse_preprocess(tensor)


class _Tests:
    def __init__(self):
        from ._testing import ModelSuite

        self.drn = ModelSuite(lambda: DRN(architecture="fcn", feature_size=32))


if __name__ == '__main__':
    from fire import Fire

    Fire(_Tests)
