"""Common preprocessing functions shared by Nets.
"""

import torch
from torchvision import transforms
import cv2
import numpy as np
from PIL import Image

from slamtb.processing import bilateral_depth_filter


def to_feature(img):
    """Convert HxWxRGB Uint8 [0.255] to RGBxHxW float [0..1]

    Args:
        img ():
    """

    if isinstance(img, Image.Image):
        img = transforms.functional.pil_to_tensor(img)
    else:
        if isinstance(img, np.ndarray):
            img = torch.from_numpy(img)
        img = img.permute(2, 0, 1)

    img = (img / 255.0).float()
    return img


def to_image(tensor):
    """Convert RGBxHxW float [0..1] to HxWxRGB Uint8 [0.255]
    """

    return (tensor.permute(1, 2, 0)*255.0).byte().cpu()


def preprocess_torchvision(rgb_image):
    """Apply the standard torchvision preprocessing.
    """
    transf = transforms.Compose([
        transforms.Lambda(to_feature),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])])

    return transf(rgb_image)


def inv_preprocess_torchvision(image):
    """Apply the inverse preprosseing applied by torchvision"""
    transf = transforms.Normalize(
        mean=[-0.485/0.229, -0.456/0.224, -0.406/0.225],
        std=[1/0.229, 1/0.224, 1/0.225])
    return to_image(transf(image))


class MeanStdPreprocess:
    """Simple mean std preprocess class container
    """

    def __init__(self, mean=[0.485, 0.456, 0.406],
                 std=[0.229, 0.224, 0.225]):
        self._transform = transforms.Compose([
            transforms.Normalize(
                mean=torch.tensor(mean),
                std=torch.tensor(std))])
        self._inv_transform = transforms.Compose([
            transforms.Normalize(
                mean=torch.tensor(
                    [-mean[0]/std[0], -mean[1]/std[1], -mean[2]/std[2]]),
                std=torch.tensor(
                    [1.0/std[0], 1.0/std[1], 1.0/std[2]]))])

    def preprocess(self, img):
        """Preprocess
        """
        return self._transform(to_feature(img))

    def inverse_preprocess(self, tensor):
        """Inv the preprocess"""
        return to_image(self._inv_transform(tensor))


class SceneNetPreprocess:
    def __init__(self, bilateral_filter=False):
        self.bilateral_filter = bilateral_filter

    def preprocess(self, rgb_image, depth_image):
        """Preprocess the rgb and depth image into one 4-channel tensor"""
        rgb_image = cv2.cvtColor(rgb_image.numpy(), cv2.COLOR_RGB2BGR)
        rgb_image = rgb_image / 255.0
        rgb_image = rgb_image.astype(np.float32)

        if self.bilateral_filter:
            depth_image = bilateral_depth_filter(
                torch.from_numpy(depth_image),
                torch.from_numpy(depth_image > 0),
                filter_width=13, depth_scale=1.0).numpy()

        tensor = torch.from_numpy(np.dstack([rgb_image, depth_image])).float()
        return tensor.permute(2, 0, 1)

    @staticmethod
    def inverse_preprocess(tensor, max_depth=5.0):
        """Return the inverse tensor as the RGB concated with the depth image.
        """
        rgb_tensor = tensor[:3, :, :]
        depth_tensor = tensor[3, :, :]

        rgb_tensor = rgb_tensor.transpose(1, 0).transpose(2, 1)
        rgb_tensor = rgb_tensor*255
        rgb_tensor = rgb_tensor.byte().cpu().numpy()
        rgb_tensor = cv2.cvtColor(rgb_tensor, cv2.COLOR_BGR2RGB)

        depth_tensor = depth_tensor.cpu().numpy()
        depth_tensor = depth_tensor / max_depth*255.0
        depth_tensor = depth_tensor.astype(np.uint8)
        depth_tensor = np.dstack([depth_tensor, depth_tensor, depth_tensor])

        return torch.from_numpy(np.hstack((rgb_tensor, depth_tensor)))
