"""RGB segmentation architectures.
"""

import torch
import torch.nn as nn
import segmentation_models_pytorch as smp
from torchvision import transforms
from segmentation_models_pytorch.encoders import get_preprocessing_fn

import slamfeat.model.thirdparty.segmentation_detection as psd
from slamfeat.model.preprocessing import MeanStdPreprocess


# pylint: disable=arguments-differ

class DilatedResnet(nn.Module):
    """Dilated Resnet
    """

    _preprocess = MeanStdPreprocess(
        mean=[0.5573105812072754, 0.37420374155044556, 0.37020164728164673],
        std=[0.24336038529872894, 0.2987397611141205, 0.31875079870224])

    def __init__(self, feature_size=8, resnet_kind="Resnet34_8s", pretrained=True):
        super().__init__()

        resnet_map = {
            "Resnet34_32s": psd.Resnet34_32s,
            "Resnet34_8s": psd.Resnet34_8s,
            "Resnet18_16s": psd.Resnet18_16s,
            "Resnet34_16s": psd.Resnet34_16s,
            "Resnet9_8s": psd.Resnet9_8s
        }

        self._fcn = resnet_map[resnet_kind](
            num_classes=feature_size, pretrained=pretrained)
        self._feature_size = feature_size

    def forward(self, x):
        """Forward.
        """
        x = self._fcn(x)
        return x

    def get_parameters(self, _):
        """Return the learnable parameters.
        """
        return self._fcn.parameters()

    @property
    def feature_size(self):
        """Return feature size
        """
        return self._feature_size

    @staticmethod
    def preprocess(image, _):
        """Preprocess a RGB image
        """
        return DilatedResnet._preprocess.preprocess(image)

    @staticmethod
    def inv_preprocess(tensor):
        """Inverse preprocess from tensor to image.
        """
        return DilatedResnet._preprocess.inverse_preprocess(tensor)

    def extract_features(self, rgb_image, _):
        """Extract features from rgb_image and depth tuple.
        """
        x = self.preprocess(rgb_image, None)
        device = next(self.parameters()).device
        with torch.no_grad():
            return self.eval()(x.unsqueeze(0).to(device)).squeeze()

    def extract_frame_features(self, frame):
        """Extract features from a slamtb.frame.Frame.
        """
        return self.extract_features(torch.from_numpy(frame.rgb_image), None)

    @staticmethod
    def distance(feat1, feat2):
        hheight = int(feat1.size(0) / 2)
        hwidth = int(feat1.size(1) / 2)

        # return (feat1[:, hheight, hwidth] - feat2[:, hheight, hwidth]).norm(2).item()
        return (feat1 - feat2).norm(2, dim=0).mean().item()


class SMP(nn.Module):
    """Base network for using the segmentation_models_pytorch module.
    """

    def __init__(self, net_type="FPN", feature_size=8, encoder_name="resnet34",
                 encoder_weights="imagenet"):
        super().__init__()

        self._preprocess_fn = get_preprocessing_fn(
            encoder_name, pretrained="imagenet")

        if net_type == "FPN":
            self.net = smp.FPN(encoder_name=encoder_name,
                               encoder_weights=encoder_weights,
                               classes=feature_size)
        elif net_type == "PSPNet":
            self.net = smp.PSPNet(encoder_name=encoder_name,
                                  encoder_weights=encoder_weights,
                                  classes=feature_size)
        elif net_type == "Unet":
            self.net = smp.Unet(encoder_name=encoder_name,
                                encoder_weights=encoder_weights,
                                classes=feature_size, encoder_depth=3, decoder_channels=[
                                    256, 128, 64])

        self._feature_size = feature_size

    def forward(self, x):
        x = self.net(x)
        return x

    def get_parameters(self, learning_rate):
        """Get learnable parameters.
        """
        return self.net.parameters()

    @property
    def feature_size(self):
        """Channel wise feature size.
        """
        return self._feature_size

    def preprocess(self, rgb_image, _):
        """Preprocessing
        """

        return self._preprocess_fn(rgb_image).float().permute(2, 0, 1)

    def extract_frame_features(self, frame):
        """Extract features from a slamtb Frame.
        """
        x = self.preprocess(torch.from_numpy(frame.rgb_image), None)

        device = next(self.parameters()).device
        with torch.no_grad():
            return self.eval()(x.unsqueeze(0).to(device)).squeeze()


class _Tests:
    def __init__(self):
        from slamfeat.model._testing import ModelSuite

        self.dilated_resnet = ModelSuite(lambda: DilatedResnet(
            feature_size=8, resnet_kind="Resnet34_8s"))
        self.unet = ModelSuite(lambda: SMP(
            net_type="Unet", encoder_name="vgg11", encoder_weights="imagenet",
            feature_size=32
        ))


if __name__ == '__main__':
    from fire import Fire

    Fire(_Tests)
