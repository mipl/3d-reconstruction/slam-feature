"""Pose graph generation by using submaps.
"""
import torch

from slamtb.posegraph import (SubmapPoseGraph as BaseSubmapPoseGraph)


class SubmapPoseGraph(BaseSubmapPoseGraph):
    """Util class to generate a pose graph by submapping.

    Attributes:

        pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
         Linear pose graph
    """

    def __init__(self, pose_graph=None):
        super().__init__(pose_graph)

    def save(self, file):
        """Save the pose graph.
        """
        torch.save(self.pose_graph, file)

    @classmethod
    def load(cls, file):
        """Load the pose graph.
        """
        return cls(torch.load(file))
