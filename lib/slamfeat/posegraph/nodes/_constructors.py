"""Constructing nodes for pose graphs
"""

from pathlib import Path

import torch
import rflow

from slamtb.camera import RTCamera
from slamtb.trajectory import Odometry
from slamtb._utils import NoneCallProxy

# pylint: disable=no-self-use


class _OdometryContext:
    def __init__(self, icp, verifier):
        self.icp = icp
        self.verifier = verifier
        self.rt_cam = RTCamera()
        self.odometry = Odometry()

        self.prev_fpcl = None
        self.prev_dense_features = None

    def estimate(self, live_fpcl, dense_features):
        if self.prev_fpcl is None:
            self.prev_fpcl = live_fpcl
            self.prev_dense_features = dense_features

            return (True, RTCamera(),
                    torch.eye(6, dtype=torch.double)*1000)

        icp_result = self.icp.estimate_frame(
            self.prev_fpcl, live_fpcl,
            source_feats=self.prev_dense_features,
            target_feats=dense_features)
        if icp_result.transform is not None:
            transform = icp_result.transform
            information = icp_result.information

        self.prev_fpcl = live_fpcl
        self.prev_dense_features = dense_features
        self.odometry.update(transform)

        if transform is None:
            transform = torch.eye(4, dtype=torch.double)

        return self.verifier(icp_result), self.odometry.to_camera(), information


class CreateSurfelSubmapGraph(rflow.Interface):
    """Creates submaps as surfel model from a dataset.
    """

    def evaluate(self, resource, dataset, submap_size,
                 stable_conf_thresh, stable_time_thresh,
                 odometry, odo_feature_model,
                 feature_model, sparse_feature_model=None,
                 max_frames=None):
        """Evaluate"""

        from tqdm import tqdm

        from tenviz import Context
        from slamtb.ui import SurfelReconstructionUI, RunMode, FrameUI
        from slamtb.fusion.surfel import SurfelFusion
        from slamtb.surfel import SurfelModel
        from slamtb.registration import RegistrationVerifier
        from slamtb.frame import FramePointCloud
        from slamtb.posegraph import SubmapPoseGraphBuilder
        from slamtb.keyframe import IntervalKeyFrameDetector

        pg_builder = SubmapPoseGraphBuilder()
        keyframe_detector = IntervalKeyFrameDetector(submap_size)

        device = "cuda:0"
        if odo_feature_model is not None:
            odo_feature_model = odo_feature_model.to(device)
        if feature_model is not None:
            feature_model = feature_model.to(device)

        odometry_context = _OdometryContext(odometry, RegistrationVerifier())

        gl_context = Context()
        surfel_model = SurfelModel(
            gl_context, 640*480*submap_size//12,
            feature_size=(feature_model.feature_size
                          if feature_model is not None else 0))

        rec_ui = SurfelReconstructionUI(
            surfel_model, RunMode.PLAY, inverse=True, title="Submap reconstruction")
        frame_ui = FrameUI(depth_max=4000)

        fusion = SurfelFusion(
            surfel_model, stable_conf_thresh=stable_conf_thresh,
            stable_time_thresh=stable_time_thresh)

        max_frames = (len(dataset) if max_frames is None
                      else min(len(dataset), max_frames))

        frame_count = 0
        for _ in tqdm(enumerate(rec_ui), total=max_frames):
            if frame_count >= max_frames:
                break

            frame = dataset[frame_count]
            frame.bilateral_depth_filter()
            frame.clamp_max_depth(3.0)
            frame_ui.update(frame)

            proc_fpcl = FramePointCloud.from_frame(frame).to(device)

            odo_success, rt_cam, information = odometry_context.estimate(
                proc_fpcl, NoneCallProxy(odo_feature_model).extract_frame_features(
                    frame))
            if not odo_success:
                print("Tracking fail")

            features = NoneCallProxy(feature_model).extract_frame_features(
                frame)

            sparse_features = None

            if sparse_feature_model is not None:
                sparse_features = sparse_feature_model.extract_features(
                    frame.rgb_image)
                sparse_features = sparse_features.masked_select(
                    sparse_features.responses > 0.025)

            fuse_stats = fusion.fuse(
                proc_fpcl, pg_builder.get_local_camera(rt_cam),
                features=features,
                sparse_features=sparse_features)

            print("{} sparse feature count {}".format(
                fuse_stats, len(surfel_model.sparse_features)))
            frame_count += 1

            if keyframe_detector.is_keyframe(frame_count):
                submap = surfel_model.to_surfel_cloud()[0].to("cpu")
                submap = submap[submap.confidences > stable_conf_thresh]

                pg_builder.append(keyframe_detector.get_frame_keyframe(frame_count),
                                  submap, rt_cam, information)
                fusion.reset()

        submap = surfel_model.to_surfel_cloud()[0].to("cpu")
        submap = submap[submap.confidences > stable_conf_thresh]
        if submap.size > 10000:
            pose_graph = pg_builder.finish(
                keyframe_detector.get_frame_keyframe(frame_count) + 1,
                submap)
        else:
            pose_graph = pg_builder.finish(None, None)

        rec_ui.close()
        frame_ui.close()

        Path(resource.filepath).parent.mkdir(parents=True, exist_ok=True)
        torch.save(pg_builder.pose_graph, resource.filepath)
        return pg_builder.pose_graph

    def load(self, resource):
        """Load"""
        return torch.load(resource.filepath)


class GetGroundTruthOdometry(rflow.Interface):
    """Creates a posegraph using the ground truth odometry from the
    dataset. This ground truth doesn't have loop closures.
    """

    def evaluate(self, resource, dataset, submap_size, max_frames=None):
        """Evaluate"""
        from slamtb.posegraph import SubmapPoseGraphBuilder
        from slamtb.keyframe import IntervalKeyFrameDetector

        base_camera = dataset[0].info.rt_cam

        pg_builder = SubmapPoseGraphBuilder()
        keyframe_detector = IntervalKeyFrameDetector(submap_size)

        max_frames = (len(dataset) if max_frames is None
                      else min(len(dataset), max_frames))

        info = torch.eye(6, dtype=torch.double) * 5000
        for frame_idx in range(max_frames):
            if not keyframe_detector.is_keyframe(frame_idx):
                continue

            rt_cam = dataset[frame_idx].info.rt_cam
            pg_builder.append(keyframe_detector.get_frame_keyframe(frame_idx),
                              None, rt_cam.relative_to(base_camera), info)

        pose_graph = pg_builder.finish(keyframe_detector.get_frame_keyframe(frame_idx) + 1,
                                       None)

        resource.pickle_dump(pose_graph)

        return pose_graph

    def load(self, resource):
        """Load"""
        return resource.pickle_load()


class GetGroundTruthPoseGraph(rflow.Interface):
    """Create a ground truth pose graph.
    """

    def evaluate(self, resource, submap_pg, gt_odometry_pg, overlap_rate):
        """Evaluate."""

        from slamtb.posegraph import PoseGraphBuilder
        from tqdm import tqdm
        from slamtb.camera import RigidTransform
        from slamtb.geometry import AABB
        from scipy.spatial.ckdtree import cKDTree

        gt_pose_graph = {
            node_id: node.clone(no_edges=False)
            for node_id, node in gt_odometry_pg.items()
        }

        if False:
            for node in gt_pose_graph.values():
                if node.i in submap_pg:
                    node.geometry = submap_pg[node.i].geometry

        pg_builder = PoseGraphBuilder(gt_pose_graph)

        nodes = list(submap_pg.keys())
        for i in tqdm(range(len(nodes) - 1), total=len(nodes)-1):
            node_i = nodes[i]

            pose_i = gt_odometry_pg[node_i].pose
            points_i = RigidTransform(
                pose_i.float()) @ submap_pg[node_i].geometry.points.cpu()
            box_i = AABB.from_points(points_i)

            points_i = cKDTree(points_i.numpy(), leafsize=128)

            for j in range(i + 2, len(nodes)):
                node_j = nodes[j]
                pose_j = gt_odometry_pg[node_j].pose
                points_j = (RigidTransform(pose_j.float())
                            @ submap_pg[node_j].geometry.points.cpu())
                box_j = AABB.from_points(points_j)

                points_j = points_j.numpy()

                intersection_box = box_i.intersection(box_j)
                intersection_rate = intersection_box.area() / max(box_i.area(), box_j.area())

                if intersection_rate < 0.05:
                    continue

                _, indices = points_i.query(
                    points_j, distance_upper_bound=0.05)
                # match_count = len(set(indices[indices < points_i.n]))
                match_count = (indices < points_i.n).sum()
                match_rate = match_count / points_i.n
                if match_rate >= overlap_rate:

                    information = torch.eye(4, dtype=torch.float64)*1000
                    pg_builder.add(
                        j, i,
                        (pose_i.inverse() @ pose_j),
                        information, uncertain=False)

        torch.save(pg_builder.pose_graph, resource.filepath)

        return pg_builder.pose_graph

    def load(self, resource):
        """Load"""
        return torch.load(resource.filepath)
