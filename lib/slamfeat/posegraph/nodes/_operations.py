"""Operators on posegraphs
"""

import torch
import rflow
import open3d

# pylint: disable=no-self-use


class PoseGraphTransformationMetrics(rflow.Interface):
    """Compute the metrics of a pose graph's transformations.
    """

    def evaluate(self, pred_pose_graph, gt_pose_graph, loop_closure_eval=False):
        """Eval"""
        from slamtb.metrics.posegraph import (
            edges_rotational_rmse,
            edges_translational_rmse,
            align_pose_graph_trajectories
        )

        from slamfeat.posegraph.metrics import loop_closure_precision_recall
        from slamfeat.table import make_table

        lc_precision, lc_recall = loop_closure_precision_recall(
            pred_pose_graph, gt_pose_graph)

        metrics = {
            "MTE": round(edges_translational_rmse(gt_pose_graph, pred_pose_graph,
                                                  only_uncertain_edges=loop_closure_eval,
                                                  ), 4),
            "MRE": round(edges_rotational_rmse(gt_pose_graph, pred_pose_graph,
                                               only_uncertain_edges=loop_closure_eval,
                                               ), 4),
            "L.C. Precision": round(lc_precision, 2),
            "L.C. Recall": round(lc_recall, 2)
        }

        print(make_table(metrics))
        self.save_measurement(metrics)
        return metrics


class PoseGraphTrajectoryMetrics(rflow.Interface):
    def evaluate(self, gt_pose_graph, pred_pose_graph):
        from slamtb.trajectory import Trajectory
        from slamtb.metrics.trajectory import (
            absolute_translational_error,
            relative_rotational_error)
        from slamfeat.table import make_table

        gt_trajectory = Trajectory.from_posegraph(gt_pose_graph)
        pred_trajectory = Trajectory.from_posegraph(pred_pose_graph)
        pred_trajectory = pred_trajectory.aligned_to(gt_trajectory)

        metrics = {
            "ATE": round(
                absolute_translational_error(
                    gt_trajectory,
                    pred_trajectory).mean().sqrt().item(), 4),
            "RRE": round(
                relative_rotational_error(
                    gt_trajectory,
                    pred_trajectory).mean().sqrt().item(), 4)
        }
        print(make_table(metrics))
        self.save_measurement(metrics)
        return metrics


class PertubTransformationEdges(rflow.Interface):
    """Pertub the pose graph edges with random transformations.
    """

    def evaluate(self, resource, pose_graph, rand_seed=None,
                 max_rot_degrees=3, max_translation=0.005):
        """
        Args:

            resource (:obj:`rflow.FSResource`): Save pose graph.

            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             input pose graph.

            rand_seed (int, optional): Optional random seed.

            max_rot_degrees (float): Maximum random rotation degrees to pertub.

            max_translation (float): Maximum random translation to pertub.

        Returns: (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
         Pose graph with its edges pertubed.

        """
        import random
        import math

        from slamtb.camera import RigidTransform
        from slamtb.posegraph import recalculate_poses_from_edges
        new_pose_graph = {}

        for i, node in pose_graph.items():
            new_pose_graph[i] = node.clone(
                no_edges=True, shallow_geometry=True)

        if rand_seed is not None:
            random.seed(rand_seed)
        for i, node in pose_graph.items():
            for j, edge in node.edges.items():
                if j in new_pose_graph[i].edges:
                    continue

                new_pose_graph[i][j] = edge.clone()

                new_pose_graph[i][j].transform @= RigidTransform.from_random(
                    math.radians(max_rot_degrees), max_translation,
                    dtype=torch.float64).matrix

                if i in pose_graph[j].edges:
                    new_pose_graph[j][i] = pose_graph[j][i].clone()
                    new_pose_graph[j][i].transform = (
                        new_pose_graph[i][j].transform)

        new_pose_graph = recalculate_poses_from_edges(new_pose_graph)
        torch.save(new_pose_graph, resource.filepath)
        return new_pose_graph

    def load(self, resource):
        """Load"""

        return torch.load(resource.filepath)


class PoseGraphFeaturesMetrics(rflow.Interface):
    """Compute the metrics of a pose graph's features.
    """

    def evaluate(self, submaps, pose_graph):
        from slamfeat.posegraph import norm_features_differences
        from slamfeat.table import make_table

        if False:
            norm_diff_graph = norm_features_differences(submaps, pose_graph)

            differences = []

            for i in norm_diff_graph.keys():
                diff = torch.stack(list(norm_diff_graph[i].values()))
                mask = ~torch.isinf(diff)
                diff = (diff*mask).sum(0) / mask.sum(dim=0)

                differences.extend(diff[~torch.isnan(diff)].tolist())

            differences = torch.tensor(differences)
            metrics = {
                "L2 Mean": differences.mean().item(),
                "L2 Std": differences.std().item()
            }

            print(make_table(metrics))
            self.save_measurement(metrics)
            return metrics
        else:
            from slamfeat.posegraph.metrics import nn_feature_accuracy

            return {'NN-ACC': nn_feature_accuracy(submaps, pose_graph)}


def _loop_closure_proc(args):
    registration, curr_node_id, other_node_id, curr_geomtry, other_geometry = args
    result = registration.estimate_sparse_frag(
        other_geometry,
        curr_geomtry)
    return result, curr_node_id, other_node_id


class LoopClosure(rflow.Interface):
    """Find loop closures on a pose_graph's using submap registration.
    """

    def evaluate(self, resource, pose_graph, registration, verifier=None,
                 num_workers=11):
        """
        Args:

            resource (:obj:`rflow.FSResource`): Save pose graph.

            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             input pose graph.

            registration (:obj:`object`): Registration method.

            verifier (:obj:`object`): Registration verifier.

        Returns: (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
         Pose graph with found loop closure edges.

        """
        #from torch.multiprocessing import Pool
        from multiprocessing import Pool

        from tqdm import tqdm
        from slamtb.registration import RegistrationVerifier
        from slamtb.posegraph import PoseGraphEdge
        from slamtb.posegraph import dump_pose_graph

        print(dump_pose_graph(pose_graph))
        new_pose_graph = {
            i: node.clone(no_edges=True, no_geometry=True)
            for i, node in pose_graph.items()
        }

        if verifier is None:
            verifier = RegistrationVerifier(covariance_max_threshold=1e-02)

        node_ids = list(pose_graph.keys())

        sparse_frags = {}
        for node in pose_graph.values():
            sparse_frags[node.i] = registration.preprocess(
                node.geometry.to("cpu"))

        o3d_pcls = {}
        for node in pose_graph.values():
            o3d_pcls[node.i] = node.geometry.to(
                "cpu").as_point_cloud().to_open3d()

        parameters = []
        for iidx in range(len(node_ids) - 1):
            curr_node_id = node_ids[iidx]
            next_node_id = node_ids[iidx + 1]

            if next_node_id in pose_graph[curr_node_id].edges:
                new_pose_graph[curr_node_id].edges[next_node_id] = (
                    pose_graph[curr_node_id].edges[next_node_id].clone())

            for jidx in range(iidx + 2, len(node_ids)-1):
                other_node_id = node_ids[jidx]

                parameters.append((registration, curr_node_id, other_node_id,
                                   sparse_frags[curr_node_id], sparse_frags[other_node_id]))

        if True:
            with Pool(num_workers) as pool:
                for result, curr_node_id, other_node_id in tqdm(pool.imap_unordered(
                        _loop_closure_proc, parameters), total=len(parameters)):
                    if not verifier(result):
                        continue
                    information = (open3d.pipelines.registration
                                   .get_information_matrix_from_point_clouds(
                                       o3d_pcls[other_node_id], o3d_pcls[curr_node_id],
                                       0.25, result.transform))
                    print(f"Edge from {curr_node_id} to {other_node_id}")
                    new_pose_graph[other_node_id].edges[curr_node_id] = PoseGraphEdge(
                        curr_node_id, result.transform, torch.from_numpy(information))
        else:
            for param in tqdm(parameters, total=len(parameters)):
                result, curr_node_id, other_node_id = _loop_closure_proc(param)
                if not verifier(result):
                    continue
                information = (
                    open3d.pipelines.registration.get_information_matrix_from_point_clouds(
                        o3d_pcls[other_node_id], o3d_pcls[curr_node_id],
                        0.25, result.transform))
                print(f"Edge from {curr_node_id} to {other_node_id}")
                new_pose_graph[other_node_id].edges[curr_node_id] = PoseGraphEdge(
                    curr_node_id, result.transform, torch.from_numpy(information))

        torch.save(new_pose_graph, resource.filepath)
        print(dump_pose_graph(new_pose_graph))
        return new_pose_graph

    def load(self, resource):
        """Load the pose graph.
        """
        return torch.load(resource.filepath)


class OptimizePoseGraph(rflow.Interface):
    """Run pose graph optimization using Open3D.
    """

    def evaluate(self, resource, pose_graph):
        """
        Args:

            resource (:obj:`rflow.FSResource`): Save pose graph.

            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             input pose graph.

        Returns: (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
         Pose graph after pose graph optimization.

        """
        from slamtb.posegraph import (
            to_open3d_pose_graph, from_open3d_pose_graph,
            recalculate_edges_from_poses)

        o3d_pg, idx_to_id = to_open3d_pose_graph(pose_graph)

        method = open3d.pipelines.registration.GlobalOptimizationLevenbergMarquardt()
        criteria = open3d.pipelines.registration.GlobalOptimizationConvergenceCriteria()
        option = open3d.pipelines.registration.GlobalOptimizationOption(
            max_correspondence_distance=2.0,
            edge_prune_threshold=0.25,
            preference_loop_closure=5.0,
            reference_node=0)
        open3d.pipelines.registration.global_optimization(
            o3d_pg, method, criteria, option)

        new_pose_graph = from_open3d_pose_graph(o3d_pg, idx_to_id)
        new_pose_graph = recalculate_edges_from_poses(new_pose_graph)

        torch.save(new_pose_graph, resource.filepath)
        return new_pose_graph

    def load(self, resource):
        """Load the pose graph.
        """
        return torch.load(resource.filepath)


class MergeSubmaps(rflow.Interface):
    def evaluate(self, resource, submaps, pose_graph, voxel_size):
        import tenviz.io
        from slamtb.surfel import SurfelVolume

        geometries = []
        for node_id, node in submaps.items():
            geometries.append(
                node.geometry.transform(pose_graph[node_id].pose))

        bb_min = []
        bb_max = []
        for geom in geometries:
            bb_min.append(geom.points.min(0)[0].cpu())
            bb_max.append(geom.points.max(0)[0].cpu())

        bb_min = torch.stack(bb_min).min(0)[0]
        bb_max = torch.stack(bb_max).max(0)[0]

        bb_box = torch.tensor([
            [bb_min[0], bb_min[1], bb_min[2]],
            [bb_max[0], bb_max[1], bb_max[2]]])
        volume = SurfelVolume(bb_box, voxel_size, None)

        for geom in geometries:
            volume.accumulate(geom.to("cpu"))

        merged = volume.to_surfel_cloud()
        tenviz.io.write_3dobject(resource.filepath,
                                 verts=merged.points.numpy(),
                                 colors=merged.colors.numpy(),
                                 normals=merged.normals.numpy())
        return merged.as_point_cloud()

    def load(self, resource):
        import tenviz.io
        from slamtb.pointcloud import PointCloud

        geo = tenviz.io.read_3dobject(resource.filepath)

        return PointCloud(geo.verts, geo.colors, geo.normals)
