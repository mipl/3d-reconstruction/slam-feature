"""Pose graph evaluation metrics
"""

from statistics import mean
from collections import defaultdict

import torch
from scipy.spatial import cKDTree

from slamtb.camera import RigidTransform

from slamfeat._cslamfeat import SlamFeatOp


def loop_closure_precision_recall(pred_pose_graph, gt_pose_graph):
    gt_total_edges = 0
    pred_total_edges = 0
    true_positives = 0
    false_positives = 0

    for gt_id, gt_node in gt_pose_graph.items():
        gt_total_edges += len(gt_node.edges)

        if gt_id not in pred_pose_graph:
            continue

        pred_node = pred_pose_graph[gt_id]
        pred_total_edges += len(pred_node.edges)

        gt_edges = set(gt_node.edges.keys())
        pred_edges = set(pred_node.edges.keys())

        true_positives += len(gt_edges.intersection(pred_edges))
        false_positives += len(pred_edges) - \
            len(gt_edges.intersection(pred_edges))

    print(true_positives, gt_total_edges, pred_total_edges)
    precision = true_positives / pred_total_edges
    recall = true_positives / gt_total_edges

    return precision, recall


def norm_features_differences(fragments, pose_graph, max_point_dist=0.0005):
    """Computes the L2 norm between the difference of the features from
    the spatial closest neighbor on all siblings fragments of a pose
    graph.


    Args:

        fragments (List[str]): List of fragment files. Files should
         contain a :obj:`slamtb.surfel.SurfelCloud`.

        pose_graph (Dict[:obj:`slamfeat.posegraph.PoseGraphNode`]):
         Pose graph.

        max_point_dist (float): Maximum distannce to find spatial
         point neighborhood.

    Returns: (Dict[int: Dict[int: :obj:`torch.Tensor`]): A graph with
        the same structure as the pose graph, but with values being
        the feature distances from the points.

    """
    from torch.nn import CosineSimilarity

    norm_diff_graph = defaultdict(dict)

    max_value = 0
    min_value = 100000000
    norms = []

    sim = CosineSimilarity(dim=0)
    for i, node in pose_graph.items():
        i_frag = torch.load(fragments[i])
        i_tree = cKDTree(i_frag.points.cpu().numpy())

        for j, edge in node.edges.items():
            j_frag = torch.load(fragments[j])
            j_points = RigidTransform(
                edge.transform.inverse().float()) @ j_frag.points

            _, index = i_tree.query(j_points.numpy(), k=1,
                                    distance_upper_bound=max_point_dist)
            valid = index < i_tree.n

            feat_diff = torch.full((i_frag.size,), float('inf'),
                                   dtype=torch.float32)

            feat_a = i_frag.features[:, index[valid]]
            # feat_a = (feat_a + 0.4556) / (0.4556 + 0.6252)
            # feat_a = feat_a / feat_a.norm(2, dim=0)

            feat_b = j_frag.features[:, valid]
            # feat_b = (feat_b + 0.4556) / (0.4556 + 0.6252)
            # feat_b = feat_b / feat_b.norm(2, dim=0)

            norms.append(feat_a.norm(2, dim=0).mean().item())
            norms.append(feat_b.norm(2, dim=0).mean().item())

            feat_diff[index[valid]] = (feat_a - feat_b).norm(2, dim=0)

            # feat_diff[index[valid]] = sim(feat_a, feat_b)

            max_value = max(
                [max_value, feat_a.max().item(), feat_b.max().item()])
            norm_diff_graph[i][j] = feat_diff

            min_value = min(
                [min_value, feat_a.min().item(), feat_b.min().item()])

    print(max_value)
    print(min_value)

    print(mean(norms))
    return norm_diff_graph


def nn_feature_accuracy(fragments, pose_graph, neighbors=5, max_point_dist=0.05):
    """Computes the L2 norm between the difference of the features from
    the spatial closest neighbor on all siblings fragments of a pose
    graph.


    Args:

        fragments (List[str]): List of fragment files. Files should
         contain a :obj:`slamtb.surfel.SurfelCloud`.

        pose_graph (Dict[:obj:`slamfeat.posegraph.PoseGraphNode`]):
         Pose graph.

        max_point_dist (float): Maximum distannce to find spatial
         point neighborhood.

    Returns: (Dict[int: Dict[int: :obj:`torch.Tensor`]): A graph with
        the same structure as the pose graph, but with values being
        the feature distances from the points.

    """

    means = []

    for i, node in pose_graph.items():
        i_frag = torch.load(fragments[i])
        i_tree = cKDTree(i_frag.points.cpu().numpy())

        for j, edge in node.edges.items():
            j_frag = torch.load(fragments[j])
            j_points = RigidTransform(
                edge.transform.inverse().float()) @ j_frag.points

            _, index = i_tree.query(j_points.numpy(), k=neighbors,
                                    distance_upper_bound=max_point_dist)
            accuracy = torch.zeros(j_points.size(0), dtype=torch.int32)
            SlamFeatOp.nn_feature_accuracy(
                i_frag.features,
                j_frag.features,
                torch.from_numpy(index),
                accuracy)
            accuracy = accuracy[accuracy >= 0]
            means.append(accuracy.sum().item() / accuracy.size(0))

    return mean(means)
