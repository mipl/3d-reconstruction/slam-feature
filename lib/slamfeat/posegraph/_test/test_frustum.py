"""Frustum intersection testing.
"""
from pathlib import Path

from fire import Fire
import numpy as np

import tenviz

from slamfeat.posegraph.frustum import (
    Frustum,
    calculate_frustum_intersection_volume,
    is_frustums_angle_good, create_posegraph_from_dataset)


class _Testing:
    @staticmethod
    def view():
        """Interactive test for the frustum intersection volume.
        """
        contexts = [tenviz.Context(), tenviz.Context()]
        scenes = [[], []]

        frus1_proj = tenviz.Projection.perspective(45.0, 0.5, 3.0, aspect=0.9)
        frus1_cam = np.eye(4, dtype=np.float32)

        frus2_proj = tenviz.Projection.perspective(45.0, 0.5, 3.0, aspect=0.9)
        frus2_cam = np.eye(4, dtype=np.float32)

        for ctx, scene in zip(contexts, scenes):
            with ctx.current():
                grid = tenviz.nodes.create_axis_grid(-5, 5, 10)
                cam1 = tenviz.nodes.create_virtual_camera(
                    frus1_proj, frus1_cam)
                scene.extend([grid, cam1])

        with contexts[0].current():
            cam2 = tenviz.nodes.create_virtual_camera(frus2_proj, frus2_cam)
            scenes[0].append(cam2)

        world_viewer = contexts[0].viewer(
            scenes[0], tenviz.CameraManipulator.WASD)
        world_viewer.title = "World"
        world_viewer.view_matrix = np.array(
            [[0.610112, -3.72529e-09, 0.792315, 2.98175],
             [0.0649421, 0.996635, -0.0500079, -1.08106],
             [-0.789649, 0.0819651, 0.608059, -3.02863],
             [0, 0, 0, 1]])

        camera_viewer = contexts[1].viewer(
            scenes[1], tenviz.CameraManipulator.WASD)
        camera_viewer.title = "Move the frustum"
        camera_viewer.view_matrix = np.array(
            [[0.878238, 1.49012e-08, -0.478225, -1.2739],
             [-0.227789, 0.879271, -0.418324, -1.35287],
             [0.420489, 0.476323, 0.772209, -0.76729],
             [0, 0, 0, 1]])

        quit_test = False
        while not quit_test:
            view_mtx = camera_viewer.view_matrix
            cam2.transform = np.linalg.inv(view_mtx)
            mtx = np.linalg.inv(view_mtx)

            keys = [
                world_viewer.wait_key(0),
                camera_viewer.wait_key(0)
            ]
            for key in keys:
                if key < 0:
                    quit_test = True
                    break
            frus1 = Frustum.create_from_opengl_cam(frus1_proj, frus1_cam)
            frus2 = Frustum.create_from_opengl_cam(frus2_proj, mtx)
            if is_frustums_angle_good(frus1, frus2):
                print(calculate_frustum_intersection_volume(
                    frus1, frus2, engine='cgal'))
            else:
                print("Not in angle")

    @staticmethod
    def pose_graph():
        """Tests the building of pose graph by comparing the frustum intersections.
        """
        import rflow
        from torch.utils.data import Subset

        from slamtb.frame import PointCloud
        from slamtb.viz.posegraph import view_pose_graph

        dataset_g = rflow.open_graph(
            Path(__file__).parent / "../../../../data/scenenn/", "scenenn")

        dataset = Subset(dataset_g["032.dataset"].call(),
                         #list(range(0, 5000, 50))
                         list(range(0, 500, 80)) + [3000]
                         #[480, 3000]
                         )

        print(list(range(0, 500, 80)))
        pose_graph = create_posegraph_from_dataset(
            dataset, frustum_intersection_ratio=0.25,
            num_processes=0)

        # pylint: disable=consider-using-enumerate
        for i in range(len(dataset)):
            pcl = PointCloud.from_frame(
                dataset[i], world_space=True, compute_normals=False)[0]
            pose_graph[i].geometry = pcl

        view_pose_graph(
            pose_graph, dataset[0].info.kcam, far=0.5)


if __name__ == '__main__':
    Fire(_Testing)
