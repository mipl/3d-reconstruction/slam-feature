"""Record camera pose to a json file.
"""
import argparse
import time
import numpy as np

import torch

import tenviz
import tenviz.io
from tenviz.pose import PoseDict, Pose

import slamfeat.data.pose
import slamfeat.viz.replica

_EPSILON = 1.0e-5


def _are_same_poses(pose0, pose1):
    return abs(pose0.to_matrix() - pose1.to_matrix()).norm() < _EPSILON


def run_record_pose(input_mesh, output_json):
    mesh = tenviz.io.read_3dobject(input_mesh).torch()

    context = tenviz.Context(640, 480)

    with context.current():
        mesh_node = slamfeat.viz.replica.create_draw_prg(mesh)

    viewer = context.viewer([mesh_node], tenviz.CameraManipulator.WASD)

    pose_dict = PoseDict()
    start_time = None

    prev_pose = None
    prev_time = None
    viewer.view_matrix = np.eye(4)

    print("Press 'r' to start recording")
    while True:
        key = viewer.wait_key(1)
        if key < 0:
            break

        if chr(key & 0xff).lower() == 'r':
            start_time = time.time()
            print("Recording...")

        if start_time is None:
            continue

        curr_time = time.time() - start_time
        view_mtx = torch.from_numpy(viewer.view_matrix)

        curr_pose = Pose.from_matrix(view_mtx)
        if prev_pose is None:
            pose_dict[curr_time] = curr_pose
            prev_pose = curr_pose
            prev_time = curr_time
            continue

        if _are_same_poses(prev_pose, curr_pose):
            prev_time = curr_time
        else:
            pose_dict[prev_time] = prev_pose
            pose_dict[curr_time] = curr_pose

            prev_pose = curr_pose
            prev_time = curr_time

    with open(output_json, 'w') as file:
        slamfeat.data.pose.write_pose_json(pose_dict, file)


def _main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_mesh", metavar='input-mesh', help="Input mesh")
    parser.add_argument("output", metavar="output-json",
                        help="Output json file containing the poses")
    args = parser.parse_args()
    run_record_pose(args.input_mesh, args.output_json)


if __name__ == '__main__':
    _main()
