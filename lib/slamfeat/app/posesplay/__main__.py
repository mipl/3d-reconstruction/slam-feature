"""Play pose json files.
"""

import argparse
import time

import tenviz
import tenviz.io

import slamfeat.viz.replica
import slamfeat.data.pose


def run_pose_play(input_mesh, poses, segmentation=False):
    mesh = tenviz.io.read_3dobject(input_mesh).torch()

    context = tenviz.Context(640, 480)
    with context.current():
        mesh_node = slamfeat.viz.replica.create_draw_prg(
            mesh, segmentation=segmentation)

    viewer = context.viewer([mesh_node], tenviz.CameraManipulator.WASD)

    with open(poses, 'r') as file:
        pose_dict = slamfeat.data.pose.read_pose_json(file)
    start_time = time.time()

    while True:
        key = viewer.wait_key(1)
        if key < 0:
            break

        curr_time = time.time() - start_time
        print("{}s".format(curr_time))
        curr_pose = pose_dict[curr_time]
        viewer.view_matrix = curr_pose.to_matrix().float()


def _main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "input_mesh", metavar='input-mesh', help="Input mesh")
    parser.add_argument(
        "poses", help="Poses json")
    parser.add_argument('--segmentation', action="store_true",
                        help="whatever to show segmentation")

    args = parser.parse_args()
    run_pose_play(args.input_mesh, args.poses)


if __name__ == '__main__':
    _main()
