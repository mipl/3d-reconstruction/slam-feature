#!/bin/env python

"""Quick test for the train rountine.
"""
from pathlib import Path

import torch
from torch.utils.tensorboard import SummaryWriter
import pytorch_lightning as pl
import rflow


from slamtb._utils import profile

from slamfeat.data import IndexedCorrespondenceDataset
from slamfeat.model.rgbsegmentation import DilatedResnet

#from slamfeat.train.contrastive_train import ContrastiveTrain, CorrespondencePairTrain
from slamfeat.train.base import TrainConfig

from slamfeat.train.contrastive_train import ContrastiveData, ContrastiveModel


def _test():
    summ_writer = SummaryWriter()

    model = DilatedResnet(feature_size=3, resnet_kind="Resnet9_8s")
    dataset_g = rflow.open_graph(
        Path(__file__).parent / "../../../../data/pdc", "catepillar")

    dataset = dataset_g['s1.dataset'].call()

    train_dataset = IndexedCorrespondenceDataset(
        dataset, 4)
    val_dataset = IndexedCorrespondenceDataset(
        dataset, 4)

    torch.backends.cudnn.benchmark = True
    train = CorrespondencePairTrain(
        "cuda:0", model,
        TrainConfig(5, 1, 0.1, [1, 2, 3, 4], 0.9,
                    loss_func='contrastive'),
        train_dataset,
        val_dataset, summ_writer, "test")

    with profile(Path(__file__).parent / "tupletrain.cprof"):
        train.run(0, num_train_samples=50, num_val_samples=25)

    train.log_hparams()


def _test2():
    model = DilatedResnet(feature_size=3, resnet_kind="Resnet9_8s")
    dataset_g = rflow.open_graph(
        Path(__file__).parent / "../../../../data/pdc", "catepillar")

    dataset = dataset_g['s1.dataset'].call()

    train_dataset = IndexedCorrespondenceDataset(
        dataset, 4)
    val_dataset = IndexedCorrespondenceDataset(
        dataset, 4)
    data_module = ContrastiveData(train_dataset, val_dataset, batch_size=1,
                                  num_train_samples=50, num_val_samples=25,
                                  preprocess_fn=model.preprocess)
    model_module = ContrastiveModel(model, loss_margin=0.5, learning_rate=0.1,
                                    weight_decay=0.9)

    trainer = pl.Trainer(gpus=1, max_epochs=5, fast_dev_run=True)

    trainer.fit(model_module, data_module)


if __name__ == '__main__':
    _test2()
