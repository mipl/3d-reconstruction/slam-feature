"""Testing of loss function.
"""
from pathlib import Path

import numpy as np
import torch
from fire import Fire
import rflow


class _Testing:
    @staticmethod
    def contrastive():
        from slamfeat.data import RGBDCorrespondenceDataset
        from slamfeat.train.contrastive_loss import ContrastiveLoss
        from slamfeat.train.contrastive_train import _Collate as Collate

        loss_fn = ContrastiveLoss()

        data_g = rflow.open_graph(
            Path(__file__).parent / "../../../../data/ilrgbd/", "loft")
        dataset = data_g["dataset"].call()
        visibility_graph = data_g["visibility_graph"].call()

        np.random.seed(10)
        torch.manual_seed(10)
        dataset = RGBDCorrespondenceDataset(dataset, visibility_graph)

        rgb_item = dataset[44]

        def preprocess(rgb, _):
            return rgb.float().permute(2, 0, 1) / 255.0

        item = Collate.collate(preprocess, [rgb_item])
        result = loss_fn(item.anc_tensors,
                         item.dock_tensors, item.anc_pos_indices,
                         item.dock_pos_indices,
                         item.anc_neg_indices, item.dock_neg_indices)
        print(result)


if __name__ == "__main__":
    Fire(_Testing)
