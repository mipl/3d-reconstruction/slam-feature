"""Nodes for traning workflows.
"""
import rflow


class ContrastiveTrain(rflow.Interface):
    """An interface rflow for contrastive training.
    """

    def non_collateral(self):
        return ["device", "num_workers", "profile", "num_val_samples"]

    def evaluate(self, resource, model, train_config, train_dataset, val_dataset,
                 num_workers=5, profiler=None,
                 num_train_samples=5000, num_val_samples=200,
                 fast_dev_run=False):
        from pathlib import Path

        import torch
        import pytorch_lightning as pl
        # from torch.utils.tensorboard import SummaryWriter

        from slamfeat.config import get_expid
        from slamfeat.train.contrastive_train import ContrastiveData, ContrastiveModel

        torch.backends.cudnn.benchmark = True

        exp_id = get_expid("runs")
        print(f"Experiment ID: {exp_id}")
        data = ContrastiveData(train_dataset, val_dataset, batch_size=train_config.batch_size,
                               preprocess_fn=model.preprocess,
                               num_train_samples=num_train_samples,
                               num_val_samples=num_val_samples,
                               num_workers=num_workers)
        contrastive_model = ContrastiveModel(model, loss_margin=train_config.loss_margin,
                                             learning_rate=train_config.learning_rate,
                                             weight_decay=train_config.weight_decay)

        checkpoint_callback = pl.callbacks.model_checkpoint.ModelCheckpoint(
            monitor="val_loss",
            dirpath=resource.filepath,
            filename='model-epoch{epoch:02d}',
            auto_insert_metric_name=False)
        trainer = pl.Trainer(gpus=1,
                             max_epochs=train_config.max_epochs,
                             fast_dev_run=fast_dev_run,
                             callbacks=[checkpoint_callback],
                             profiler=profiler
                             )
        try:
            trainer.fit(contrastive_model, data)
        except KeyboardInterrupt:
            print("Early stopping the training")

        checkpoint_callback.to_yaml(
            str(Path(resource.filepath) / "best_models.yaml"))
        return self.load(resource, model)

    def load(self, resource, model):
        """
        Load model.
        """
        from pathlib import Path
        import yaml

        import torch
        import numpy as np

        from slamfeat.train.contrastive_train import ContrastiveModel

        # Maintain compability with older versions of our project
        if Path(resource.filepath).is_file():
            model = torch.load(resource.filepath)
            model = model.eval()
            return model

        with open(Path(resource.filepath) / "best_models.yaml", 'r') as file:
            best_models = yaml.load(file)

        filepaths, losses = zip(*best_models.items())
        best_loss_idx = np.argmin(losses)

        model_checkpoint = ContrastiveModel.load_from_checkpoint(
            filepaths[best_loss_idx], model=model, learning_rate=0.0)

        return model_checkpoint.model.eval()
