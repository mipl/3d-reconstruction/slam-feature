"""Training of triplet loss using Adam Optimizer.
"""

import torch
from torch.utils.data import RandomSampler
from torchvision.utils import make_grid
import pytorch_lightning as pl

from .contrastive_loss import ContrastiveLoss


class _Collate:
    def __init__(self, anc_tensors, dock_tensors,
                 anc_pos_indices, dock_pos_indices,
                 anc_neg_indices, dock_neg_indices):
        self.anc_tensors = anc_tensors
        self.dock_tensors = dock_tensors

        self.anc_pos_indices = anc_pos_indices
        self.dock_pos_indices = dock_pos_indices

        self.anc_neg_indices = anc_neg_indices
        self.dock_neg_indices = dock_neg_indices

    @classmethod
    def collate(cls, proc_fn, item_list):
        """
        """
        anc_tensors = []
        dock_tensors = []

        anc_pos_indices = []
        dock_pos_indices = []
        anc_neg_indices = []
        dock_neg_indices = []

        for item in item_list:
            item = item.sample_correspondences(2500, 2500)
            anc_tensors.append(proc_fn(item.anc_rgb, item.anc_depth))
            dock_tensors.append(proc_fn(item.dock_rgb, item.dock_depth))

            anc_pos_indices.append(item.anc_posv_index)
            dock_pos_indices.append(item.dock_posv_index)

            anc_neg_indices.append(item.anc_negv_index)
            dock_neg_indices.append(item.dock_negv_index)

        return _Collate(torch.stack(anc_tensors), torch.stack(dock_tensors),
                        torch.stack(anc_pos_indices),
                        torch.stack(dock_pos_indices),
                        torch.stack(anc_neg_indices),
                        torch.stack(dock_neg_indices))

    def pin_memory(self):
        """Pin cpu tensors to pinned memory. This function is called by
        PyTorch.
        """
        self.anc_tensors = self.anc_tensors.pin_memory()
        self.dock_tensors = self.dock_tensors.pin_memory()

        self.anc_pos_indices = self.anc_pos_indices.pin_memory()
        self.dock_pos_indices = self.dock_pos_indices.pin_memory()

        self.anc_neg_indices = self.anc_neg_indices.pin_memory()
        self.dock_neg_indices = self.dock_neg_indices.pin_memory()

        return self

    def to(self, destine):
        return _Collate(self.anc_tensors.to(destine),
                        self.dock_tensors.to(destine),
                        self.anc_pos_indices.to(destine),
                        self.dock_pos_indices.to(destine),
                        self.anc_neg_indices.to(destine),
                        self.dock_neg_indices.to(destine))

    def __len__(self):
        return self.anc_neg_indices.size(0)


class _CollateWrapper:
    def __init__(self, proc_fn):
        self.proc_fn = proc_fn

    def __call__(self, batch):
        return _Collate.collate(self.proc_fn, batch)


class ContrastiveData(pl.LightningDataModule):
    def __init__(self, train_dataset, val_dataset, *, batch_size: int,
                 num_train_samples: int, num_val_samples: int,
                 preprocess_fn,
                 num_workers: int = 0):
        super().__init__()
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.batch_size = batch_size
        self.num_train_samples = num_train_samples
        self.num_val_samples = num_val_samples
        self.preprocess_fn = preprocess_fn
        self.num_workers = num_workers

    def _create_dataloader(self, dataset):
        collate_fn = _CollateWrapper(self.preprocess_fn)

        return torch.utils.data.DataLoader(
            dataset, batch_size=self.batch_size,
            shuffle=False, num_workers=self.num_workers,
            sampler=RandomSampler(
                dataset, replacement=True,
                num_samples=self.num_train_samples),
            collate_fn=collate_fn, pin_memory=True)

    def train_dataloader(self):
        return self._create_dataloader(self.train_dataset)

    def val_dataloader(self):
        return self._create_dataloader(self.val_dataset)

    def prepare_data(self):
        pass

    def setup(self, stage=None):
        pass


class ContrastiveModel(pl.LightningModule):
    def __init__(self, model, loss_margin=0.5, learning_rate=0.005, weight_decay=0.005, lr_plateu_patience=3):
        super().__init__()
        self.model = model
        self.loss_fn = ContrastiveLoss(margin=loss_margin)
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.lr_plateu_patience = lr_plateu_patience

    def forward(self, item):
        anchor_feats = self.model(item.anc_tensors)
        dock_feats = self.model(item.dock_tensors)

        return anchor_feats, dock_feats

    def training_step(self, item, batch_idx):
        anchor_feats = self.model(item.anc_tensors)
        dock_feats = self.model(item.dock_tensors)

        loss = self.loss_fn(anchor_feats, dock_feats,
                            item.anc_pos_indices, item.dock_pos_indices,
                            item.anc_neg_indices, item.dock_neg_indices)

        if not isinstance(loss, torch.Tensor):
            return None

        self.log("train_loss", loss, on_epoch=True)
        return loss

    def validation_step(self, item, batch_idx):
        anchor_feats = self.model(item.anc_tensors)
        dock_feats = self.model(item.dock_tensors)

        loss = self.loss_fn(anchor_feats, dock_feats,
                            item.anc_pos_indices, item.dock_pos_indices,
                            item.anc_neg_indices, item.dock_neg_indices)

        if not isinstance(loss, torch.Tensor):
            return None
        self.log("val_loss", loss, on_epoch=True)
        return loss

    def configure_optimizers(self):
        params = self.model.get_parameters(self.learning_rate)
        optimizer = torch.optim.Adam(params, lr=self.learning_rate,
                                     weight_decay=self.weight_decay)

        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, patience=self.lr_plateu_patience)
        return {
            'optimizer': optimizer,
            'lr_scheduler': scheduler,
            'monitor': 'train_loss'
        }

    def update_viz(self, phase, item):
        if not hasattr(self.model, "inv_preprocess"):
            return

        anchor_imgs = [self.model.inv_preprocess(
            tensor) for tensor in item.anc_tensors]
        dock_imgs = [self.model.inv_preprocess(tensor)
                     for tensor in item.dock_tensors]

        images = []
        for aimg, dimg in zip(anchor_imgs, dock_imgs):
            images.append(
                torch.cat([aimg, dimg], dim=1).permute(2, 0, 1))
        images = make_grid(images, nrow=1)

        summ_writer = self.logger.experiment
        summ_writer.add_image(
            phase + "/input", images.cpu(), dataformats='CHW')
