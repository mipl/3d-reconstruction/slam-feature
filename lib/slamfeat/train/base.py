"""Training of triplet loss using Adam Optimizer.
"""
import math

import ipdb
from tqdm import tqdm
import torch
import torch.optim as optim
from torch.optim.lr_scheduler import MultiStepLR
from torch.utils.data import DataLoader, RandomSampler
from torchnet.meter import AverageValueMeter


class TrainConfig:
    """Training hyper-parameters.
    """

    def __init__(self, max_epochs, batch_size, learning_rate,
                 lr_step_epochs, weight_decay,
                 loss_func='triplet', loss_margin=0.5, gamma=0.5):
        self.max_epochs = max_epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.lr_step_epochs = lr_step_epochs
        self.weight_decay = weight_decay
        self.loss_func = loss_func
        self.loss_margin = loss_margin

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return vars(self) == vars(other)

    def __str__(self):
        return str(self.__dict__)

    def to_hdict(self):
        """Get it as dict of hyper-parameters suitable to add_hparams.
        """
        hdict = self.__dict__.copy()
        hdict['lr_step_epochs'] = str(self.lr_step_epochs)
        return hdict
