"""Modules for loss functions
"""

import torch
import torch.nn


class BaseIndexedTupleLoss(torch.nn.Module):
    """
    Contrastive loss function.
    Based on:
    """

    def __init__(self, positive_fn, negative_fn):
        super().__init__()
        self.positive_fn = positive_fn
        self.negative_fn = negative_fn

    def forward(self, anchor_featmap, dock_featmap,
                anchor_pos_indices, dock_pos_indices,
                anchor_neg_indices, dock_neg_indices):
        # pylint: disable=invalid-name

        B, F, H, W = anchor_featmap.shape

        x1 = anchor_featmap.view(B, F, H*W)
        x2 = dock_featmap.view(B, F, H*W)

        total_pos_loss = None
        for b in range(B):
            if anchor_pos_indices[b].numel() == 0:
                continue

            pos_loss = self.positive_fn(
                torch.index_select(x1[b], 1, anchor_pos_indices[b]),
                torch.index_select(x2[b], 1, dock_pos_indices[b]))

            if total_pos_loss is None:
                total_pos_loss = pos_loss
            else:
                total_pos_loss += pos_loss

        total_neg_loss = None
        for b in range(B):
            if anchor_neg_indices[b].numel() == 0:
                continue
            neg_loss = self.negative_fn(
                torch.index_select(x1[b], 1, anchor_neg_indices[b]),
                torch.index_select(x2[b], 1, dock_neg_indices[b]))

            if total_neg_loss is None:
                total_neg_loss = neg_loss
            else:
                total_neg_loss += neg_loss

        if total_pos_loss is None:
            total_pos_loss = 0

        if total_neg_loss is None:
            total_neg_loss = 0

        ret = total_pos_loss + total_neg_loss

        return ret


class ContrastiveLoss1(BaseIndexedTupleLoss):
    def __init__(self, margin=0.5, non_hard_neg_weight=1, use_hard_neg=True, max_dist=50.0):
        super().__init__(
            self._positive_fn,
            self._negative_fn)
        self.margin = margin
        self.non_hard_neg_weight = non_hard_neg_weight
        self.use_hard_neg = use_hard_neg
        self.max_dist = max_dist

    def _positive_fn(self, anchor_feat, pos_feat):
        diff = anchor_feat - pos_feat
        return (1.0 / anchor_feat.size(1)) * diff.pow(2).sum()

    def _negative_fn(self, anchor_feat, neg_feat):
        #loss = self.margin - (anchor_feat - neg_feat).norm(2, dim=0)
        loss = (self.margin - (anchor_feat - neg_feat).norm(2, dim=0)).pow(2)

        if self.use_hard_neg:
            with torch.no_grad():
                neg_count = max((loss > 0.0).sum().item(), 1)
        else:
            neg_count = anchor_feat.size(1)

        # loss = torch.clamp(loss, min=0).pow(2)
        #return (1.0 / neg_count) * loss.pow(2).sum()
        return (1.0 / neg_count) * loss.sum()

class ContrastiveLoss(BaseIndexedTupleLoss):
    def __init__(self, margin=0.5, non_hard_neg_weight=1, use_hard_neg=True, max_dist=50.0):
        super().__init__(
            self._positive_fn,
            self._negative_fn)
        self.margin = margin
        self.non_hard_neg_weight = non_hard_neg_weight
        self.use_hard_neg = use_hard_neg
        self.max_dist = max_dist

    def _positive_fn(self, anchor_feat, pos_feat):
        diff = anchor_feat - pos_feat
        return (1.0 / anchor_feat.size(1)) * diff.pow(2).sum()

    def _negative_fn(self, anchor_feat, neg_feat):
        # loss = (self.margin - (anchor_feat - neg_feat).norm(2, dim=0)).pow(2)
        
        diff_sizes = (anchor_feat - neg_feat).norm(2, dim=0)
        if self.use_hard_neg:
            with torch.no_grad():
                neg_count = max((diff_sizes > self.margin).sum().item(), 1)
        else:
            neg_count = anchor_feat.size(1)

        loss = torch.clamp(self.margin - diff_sizes, min=0).pow(2).sum()

        return (1.0 / neg_count) * loss.sum()
    
