"""Simple things related to table making.
"""
from pytablewriter import MarkdownTableWriter


def make_table(meas_dict, item=None):
    """Creates a table from a dictonary.
    
    Args:

        meas_dict (Dict): Keys are the headers and values are
         the... values.

    """
    writer = MarkdownTableWriter()

    values = ["! " + str(value) for value in meas_dict.values()]
    if item is None:
        writer.headers = list(meas_dict.keys())
        writer.value_matrix = [values]
    else:
        writer.headers = [""] + list(meas_dict.keys())
        writer.value_matrix = [[item] + values]

    return writer.dumps()
