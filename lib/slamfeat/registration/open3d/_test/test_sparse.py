"""Tests ransac registration
"""

from pathlib import Path

import torch
import fire

from slamtb.viz import geoshow
from slamtb.registration import RegistrationVerifier

from slamfeat.registration.open3d.ransac import (RANSACSparseRegistration,
                                                 FPFHFragProcessing)


class _Testing:
    @staticmethod
    def _run_test(frag0, frag1):
        voxel_size = 0.075

        frag_proc = FPFHFragProcessing(voxel_size)
        reg = RANSACSparseRegistration(frag_proc, voxel_size*1.5)

        result = reg.estimate_pcl(frag0, frag1)
        reg_verification = RegistrationVerifier()

        if not reg_verification(result):
            print("Missaligned")

        print("1 - source")
        print("2 - target")
        print("3 - aligned source")
        geoshow([frag0, frag1, frag0.transform(result.transform.float())])

    @staticmethod
    def synthetic():
        """Tests the Open3D ransac sparse registration wrapper with our data.
        """
        data_path = Path(__file__).parent / \
            "../../../../../exps/registration/colored_icp/apartment_0"

        frag0 = torch.load(data_path / "fragment-100.torch")
        frag1 = torch.load(data_path / "fragment-200.torch")

        _Testing._run_test(frag0, frag1)

    @staticmethod
    def real():
        """Tests the Open3D ransac sparse registration wrapper with our data.
        """
        data_path = Path(__file__).parent / \
            "../../../../../exps/registration/colored_icp/045"

        frag0 = torch.load(data_path / "fragment-100.torch")
        frag1 = torch.load(data_path / "fragment-200.torch")

        _Testing._run_test(frag0, frag1)


if __name__ == '__main__':
    fire.Fire(_Testing)
