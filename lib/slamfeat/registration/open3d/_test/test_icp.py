"""Verifies Open3D's registration algorithms.

"""
from pathlib import Path

import open3d
import torch

import slamtb
from slamtb.data.ftb import load_ftb
from slamtb.registration._test.testing import run_pair_test, run_trajectory_test

from ..open3d_icp import RGBDOdometry, ColorICP


TEST_DATA = Path(slamtb.__file__).parent.parent / "test-data/rgbd"

SYNTHETIC_FRAME_ARGS = dict(frame1_idx=10, blur=False, filter_depth=False)

REAL_FRAME_ARGS = dict(frame1_idx=28, blur=False, filter_depth=False)


class _Tests:

    # pylint: disable=missing-function-docstring

    @staticmethod
    def rgbd_real():
        run_pair_test(RGBDOdometry(), load_ftb(TEST_DATA / "sample1"),
                      **REAL_FRAME_ARGS)

    @staticmethod
    def rgbd_synthetic():
        run_pair_test(RGBDOdometry(), load_ftb(TEST_DATA / "sample2"),
                      **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def rgb_real():
        run_pair_test(
            RGBDOdometry(color_only=True),
            load_ftb(TEST_DATA / "sample1"),
            **REAL_FRAME_ARGS)

    @staticmethod
    def rgb_synthetic():
        run_pair_test(
            RGBDOdometry(color_only=True),
            load_ftb(TEST_DATA / "sample2"),
            **SYNTHETIC_FRAME_ARGS)

    @staticmethod
    def rgbd_trajectory():
        dataset = load_ftb(TEST_DATA / "sample1")
        icp = RGBDOdometry(False)

        frame_args = {
            'filter_depth': True,
            'blur': False,
        }
        run_trajectory_test(icp, dataset,
                            **frame_args)

    @staticmethod
    def coloricp_real():
        run_pair_test(
            ColorICP([(1.0, 50), (.5, 25)]),
            load_ftb(TEST_DATA / "sample1"),
            **REAL_FRAME_ARGS)

    @staticmethod
    def coloricp_synthetic():
        run_pair_test(
            ColorICP([(1.0, 50), (.5, 25)]),
            load_ftb(TEST_DATA / "sample2"),
            **SYNTHETIC_FRAME_ARGS)


if __name__ == '__main__':
    import fire

    fire.Fire(_Tests)
