"""Open3D ICP registration stuff!
"""

import numpy as np
import cv2
import torch
import open3d

from slamtb.registration import RegistrationResult


class ColoredICP:
    """Wrapper around Open3D's ColoredICP algorithm.
    """

    def __init__(self, scale_iters):
        self.scales = [scale for scale, _ in scale_iters]
        self.iters = [iters
                      for _, iters in scale_iters]

    def estimate_pcl(self, source_pcl, target_pcl, transform=None, **kwargs):
        # pylint: disable=unused-argument
        """Register two point clouds or surfels using the ColoredICP algorithm.
        """
        source_pcl = source_pcl.to_open3d()
        target_pcl = target_pcl.to_open3d()

        if transform is None:
            transform = np.eye(4)
        else:
            transform = transform.numpy()

        transform = transform.astype(np.float64)
        for radius, iters in list(zip(self.scales, self.iters))[::-1]:
            if radius > 0.0:
                source_down = source_pcl.voxel_down_sample(radius)
                target_down = target_pcl.voxel_down_sample(radius)
                max_cooresp_dist = 2.0*radius
            else:
                source_down = source_pcl
                target_down = target_pcl
                max_cooresp_dist = 1.0

            conv_criteria = open3d.pipelines.registration.ICPConvergenceCriteria(
                relative_fitness=1e-6,
                relative_rmse=1e-6,
                max_iteration=iters)
            try:
                result = open3d.pipelines.registration.registration_colored_icp(
                    source_down, target_down, max_cooresp_dist, transform,
                    criteria=conv_criteria)
            except RuntimeError:
                return RegistrationResult()

            transform = result.transformation

        transform = torch.from_numpy(result.transformation.copy())

        information = open3d.pipelines.registration.get_information_matrix_from_point_clouds(
            source_pcl, target_pcl, self.scales[-1], result.transformation)
        information = torch.from_numpy(information)
        return RegistrationResult(transform, information, result.inlier_rmse,
                                  1.0)
