
"""Open3D Ransac regisration stuff!
"""

import cv2
import torch
import open3d
import numpy as np

from slamtb.registration import RegistrationResult
from slamtb.registration.sparse import (
    SparseFeatureRegistration as _SparseFeatureRegistration)


class SparseSubmap:
    def __init__(self, points, normals, features):
        self.points = points
        self.normals = normals
        self.features = features

    def to_open3d(self):
        pcl = open3d.geometry.PointCloud()
        pcl.points = open3d.utility.Vector3dVector(self.points.numpy())
        pcl.normals = open3d.utility.Vector3dVector(self.normals.numpy())

        feats = open3d.pipelines.registration.Feature()
        feats.data = self.features.numpy()
        return pcl, feats


class FPFHSubmapFeatures:
    """Converts submap into an Open3D pointcloud and features.
    """

    def __init__(self, voxel_size):
        self.voxel_size = voxel_size

    def __call__(self, frag):
        pcd = frag.to("cpu").as_point_cloud().to_open3d()
        if self.voxel_size > 0:
            pcd_down = pcd.voxel_down_sample(self.voxel_size)
        else:
            pcd_down = pcd
            pcd_down.estimate_normals(
                open3d.geometry.KDTreeSearchParamHybrid(radius=self.voxel_size * 2.0,
                                                        max_nn=30))

        pcd_features = open3d.pipelines.registration.compute_fpfh_feature(
            pcd_down,
            open3d.geometry.KDTreeSearchParamHybrid(radius=self.voxel_size * 5,
                                                    max_nn=100))

        return SparseSubmap(torch.from_numpy(np.array(pcd_down.points)),
                            torch.from_numpy(np.array(pcd_down.normals)),
                            torch.from_numpy(np.array(pcd_features.data)))

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.voxel_size == other.voxel_size


class SparseSubmapFeatures:
    """Use the sparse features from the submap.
    """

    def __init__(self, min_feature_weight=2, min_surfel_confidence=40, use_dense_features=False):
        self.min_feature_weight = min_feature_weight
        self.min_surfel_confidence = min_surfel_confidence
        self.use_dense_features = use_dense_features

    def __call__(self, frag):
        """Returns a slamtb SurfelCloud into Open3D point cloud and features
        coming from the submap's sparse features.

        Args:
            frag (:obj:`slamtb.surfel.SurfelCloud`): Target submap.

        Returns:
            ((:obj:`open3d.geometry.PointCloud`, :obj:`open3d.registration.Feature`)):
             Open3D point cloud and feature data structures.

        """
        point_index = frag.sparse_features.keys()
        weights = frag.sparse_features.weights()

        confs = frag.confidences[point_index].cpu()
        # selected = (weights >= self.min_feature_weight) & (confs >= 40)
        selected = (weights >= 2.0)
        point_index = point_index[selected]

        points = frag.points[point_index].cpu()
        normals = frag.normals[point_index].cpu()

        if False:
            dense = frag.features[:, point_index].cpu()
            features = torch.cat((features, dense))
            # features /= features.norm(2, dim=0)

        if not self.use_dense_features:
            features = frag.sparse_features.descriptors()[
                selected, :].transpose(1, 0)
            features /= features.norm(2, dim=0)
        else:
            dense = frag.features[:, point_index].cpu()
            features = dense / dense.norm(2, dim=0)

        return SparseSubmap(points, normals, features)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False

        return (self.min_feature_weight == other.min_feature_weight
                and self.min_surfel_confidence == other.min_surfel_confidence)


class RANSACSparseRegistration:
    """Wrapper around Open3D ransac regisration of point clouds.
    """

    def __init__(self, frag_proc, distance_threshold):
        self.frag_proc = frag_proc
        self.distance_threshold = distance_threshold

    def preprocess(self, frag):
        return self.frag_proc(frag)

    def estimate_sparse_frag(self, source_frag, target_frag):
        self.distance_threshold = 0.25

        source_pcl, source_feats = source_frag.to_open3d()
        target_pcl, target_feats = target_frag.to_open3d()
        
        if False:
            o3d_result = (
                open3d.pipelines.registration.registration_ransac_based_on_feature_matching(
                    source_pcl, target_pcl, source_feats, target_feats,
                    True, self.distance_threshold,
                    open3d.pipelines.registration.TransformationEstimationPointToPoint(
                        False),
                    5,
                    [
                        open3d.pipelines.registration.CorrespondenceCheckerBasedOnDistance(
                            self.distance_threshold)
                    ],
                    open3d.pipelines.registration.RANSACConvergenceCriteria(1_000_000, 0.999))
            )
        else:
            o3d_result = open3d.pipelines.registration.registration_fast_based_on_feature_matching(
                source_pcl, target_pcl, source_feats, target_feats,
                open3d.pipelines.registration.FastGlobalRegistrationOption(
                    self.distance_threshold
                ))

        result = RegistrationResult(
            torch.from_numpy(o3d_result.transformation.copy()),
            None,
            residual=0.0, match_ratio=o3d_result.fitness,
            inlier_rmse=o3d_result.inlier_rmse)

        return result

    def estimate_pcl(self, source_frag, target_frag, **kwargs):
        # pylint: disable=unused-argument
        """Estimate the registration between surfels or point cloud.
        """

        return self.estimate_sparse_frag(self.preprocess(source_frag),
                                         self.preprocess(target_frag))


class SparseFeatureRegistration(_SparseFeatureRegistration):

    def estimate_pcl(self, source_frag, target_frag, **kwargs):

        def _get_points_feats(frag):
            selected = frag.sparse_features.weights() > 0.0
            features = frag.sparse_features.descriptors()[selected]
            index = frag.sparse_features.keys()[selected]

            points = frag.points[index, :].cpu()
            dense_features = frag.features[:, index].transpose(1, 0).cpu()

            features /= features.norm(2, dim=0)
            # dense_features /= dense_features.norm(2, dim=0)
            # features = torch.cat((features, dense_features), dim=1)

            return points, features

        source_points, source_feats = _get_points_feats(source_frag)
        target_points, target_feats = _get_points_feats(target_frag)

        matcher = cv2.BFMatcher(self.distance_mode)
        matchings = matcher.match(source_feats.numpy(), target_feats.numpy())
        matchings = torch.tensor(
            # source idx, target idx
            [
                # (match.queryIdx, match.trainIdx) for match in matchings
                (match.trainIdx, match.queryIdx) for match in matchings
                # if match.distance < self.max_feat_distance
            ])

        # return self.estimate(source_points, target_points, matchings)[0]
        return self.estimate(target_points, source_points, matchings)[0]


def ransac_registration_factory(voxel_size=0.05, feature_proc=None):
    """Creates a :obj:`RANSACSparseRegistration` instance.
    """

    if feature_proc is None:
        feature_proc = FPFHSubmapFeatures(voxel_size)

    return RANSACSparseRegistration(feature_proc, voxel_size*5)
    # return SparseFeatureRegistration(max_feat_distance=0.12, residual_threshold=0.05,
    #                                 max_spatial_distance=None)
