"""Registration verifiers
"""


class Open3DRegistrationVerifier:
    """From the Open3D's registration tutorial.
    """

    def __init__(self, information_ratio=0.3):
        self.information_ratio = information_ratio

    def __call__(self, result):
        if result.transform.trace() == 4.0:
            return False
        ratio = result.information[5, 5].item(
        ) / min(result.source_size, result.target_size)
        print(ratio)
        return ratio > self.information_ratio


class SparseRegistrationVerifier:
    """Verifier based on the inlier_rmse
    """

    def __init__(self, inlier_rmse_threshold=1.0e-1, match_ratio_threshold=1e-1):
        self.inlier_rmse_threshold = inlier_rmse_threshold
        self.match_ratio_threshold = match_ratio_threshold

    def __call__(self, result):
        if result.transform.trace() == 4.0:
            return False

        # if result.inlier_rmse > self.inlier_rmse_threshold:
        #    return False

        if result.match_ratio < self.match_ratio_threshold:
            return False

        return True

    def __str__(self):
        return ("SparseFeatureRegistration(inlier_rmse_threshold={}".format(self.inlier_rmse_threshold)
                + ", match_ratio_threshold={})".format(self.match_ratio_threshold))

    def __repr__(self):
        return str(self)
