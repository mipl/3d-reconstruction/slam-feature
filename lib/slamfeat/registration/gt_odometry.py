import torch
from slamtb.registration import RegistrationResult


class GTOdometry:
    """Mock odometry to return the ground truth trajectory.
    """

    def __init__(self):
        self._base_transform = None

    def estimate_frame(self, source_frame, target_frame, **kwargs):
        """Mock estimation.
        """

        if self._base_transform is None:
            self._base_transform = target_frame.rt_cam.matrix.inverse()

        transform = (target_frame.rt_cam.matrix.inverse()
                     @ source_frame.rt_cam.matrix)
        return RegistrationResult(transform, torch.eye(6)*10000, 0, 1.0)
