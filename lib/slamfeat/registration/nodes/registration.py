"""Registration rflow nodes.
"""

import torch
import rflow

from slamtb.posegraph import PoseGraphEdge

# pylint: disable=no-self-use


class TestRegistration(rflow.Interface):
    """Tests a registration algorithm.
    """

    def evaluate(self, submaps, pose_graph, gt_pose_graph,
                 nodes, registration, verifier=None,
                 device="cpu:0", ignore_edge_transform=False):
        """
        Args:
            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             Input pose graph.

            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             Pose graph with ground truth transformations.

            device (str): Torch's device.

            ignore_edge_transform (bool): Don't use the edge's transforms as
             initialization.
        """
        from slamtb.registration import RegistrationVerifier
        from slamtb.metrics.posegraph import edges_rotational_rmse
        from slamtb.metrics.posegraph import edges_translational_rmse
        from slamtb.posegraph import PoseGraphBuilder
        from slamtb.viz.posegraphviewer import PoseGraphViewer

        if verifier is None:
            verifier = RegistrationVerifier(covariance_max_threshold=1e-02)

        i = nodes[0]
        start_node = pose_graph[i].clone(
            no_edges=True, no_geometry=True)
        start_node.geometry = submaps[i].geometry
        pg_builder = PoseGraphBuilder(start_node)

        for j in nodes[1:]:
            transform = (None if ignore_edge_transform else
                         pose_graph[i][j].transform.float())

            result = registration.estimate_pcl(
                submaps[i].geometry.to(device),
                submaps[j].geometry.to(device),
                transform=transform)

            pg_builder.add(i, j, result.transform, result.information)
            pg_builder.pose_graph[j].geometry = submaps[j].geometry

            print("Match ratio =", result.match_ratio,
                  " inlier RMSE =", result.inlier_rmse)
            print(verifier)
            if not verifier(result):
                print(f"No link from {i} {j}")
            i = j

        new_pose_graph = pg_builder.pose_graph

        print("Trans. RMSE: ", edges_translational_rmse(
            gt_pose_graph, new_pose_graph))
        print("Rotat. RMSE: ", edges_rotational_rmse(
            gt_pose_graph, new_pose_graph))

        PoseGraphViewer(
            new_pose_graph, title=registration.__class__.__name__,
        ).run()


class RegisterSubmaps(rflow.Interface):
    """Registers all pose graph edges.
    """

    def non_collateral(self):
        """Non collateral parameters
        """
        return ["device", "trajectory_only"]

    def evaluate(self, resource, submaps, pose_graph, registration,
                 device="cpu:0", ignore_edge_transform=False, trajectory_only=False):
        """
        Args:
            resource (:obj:`rflow.FSResource`): Filepath, save using torch.

            pose_graph (Dict[int: :obj:`slamtb.pose_graph.PoseGraphNode`]):
             input pose graph.

            registration (:obj:`object`): Registration method.

            device (str): Torch's device.

            ignore_edge_transform (bool): Don't use the edge's transforms as
             initialization.
        """
        from tqdm import tqdm
        from slamtb.posegraph import recalculate_poses_from_edges

        new_pose_graph = {}

        for i, node in pose_graph.items():
            new_pose_graph[i] = node.clone(
                no_edges=True, no_geometry=True)

        for i in tqdm(pose_graph.keys(), total=len(pose_graph)):
            node = pose_graph[i]
            curr_frag = submaps[i].geometry.to(device)

            for j, edge in node.edges.items():
                transform = None
                if not ignore_edge_transform:
                    transform = edge.transform.float()

                result = registration.estimate_pcl(
                    curr_frag, submaps[j].geometry.to(device),
                    transform=transform,
                    device=device)

                if result.transform is None:  # colored_icp sometimes fail:
                    print(f"Registration between {i} {j} failed")
                    new_pose_graph[i].edges[j] = edge.clone()
                    continue

                new_pose_graph[i].edges[j] = PoseGraphEdge(
                    j, result.transform, result.information,
                    uncertain=(i + 1) != j)

                if trajectory_only:
                    # stops visiting other edges
                    break

        new_pose_graph = recalculate_poses_from_edges(new_pose_graph)
        torch.save(new_pose_graph, resource.filepath)

        return new_pose_graph

    def load(self, resource):
        """Load"""
        return torch.load(resource.filepath)
