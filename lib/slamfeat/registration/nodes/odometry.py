"""Odometry nodes
"""

from enum import Enum
import math

import torch
import rflow

from slamfeat.registration.gt_odometry import GTOdometry

# pylint: disable=no-self-use


class OdometryMethod(Enum):
    """Odometry methods
    """
    MULTISCALE_ICP_FAST = 0
    MULTISCALE_ICP = 1
    GROUND_TRUTH = 5


class OdometryFactory(rflow.Interface):
    """Odometry Factory.
    """

    def evaluate(self, method, geom_weight=10, feat_weight=1):
        """Evaluate.
        """
        from slamtb.registration import MultiscaleRegistration
        from slamtb.registration.icp import ICPOdometry
        from slamtb.registration.autogradicp import AutogradICP

        if method == OdometryMethod.MULTISCALE_ICP_FAST:
            odometry = MultiscaleRegistration([
                (1.0, ICPOdometry(20, geom_weight=geom_weight,
                                  feat_weight=feat_weight)),
                (0.5, ICPOdometry(20, geom_weight=geom_weight,
                                  feat_weight=feat_weight)),
                (0.5, ICPOdometry(20, geom_weight=geom_weight,
                                  feat_weight=feat_weight))
            ])

        elif method == OdometryMethod.MULTISCALE_ICP:
            params1 = {
                'geom_weight': geom_weight,
                'feat_weight': feat_weight,
                'distance_threshold': 1.0,
                'normal_angle_thresh': math.pi/2,
                'feat_residual_thresh': 0.1
            }
            params2 = {
                'geom_weight': geom_weight,
                'feat_weight': feat_weight,
                'distance_threshold': 1.0,
                'normal_angle_thresh': math.pi/2,
                'feat_residual_thresh': 0.1
            }

            params3 = {
                'geom_weight': geom_weight,
                'feat_weight': feat_weight,
                'distance_threshold': 1.0,
                'normal_angle_thresh': math.pi/2,
                'feat_residual_thresh': 0.1
            }
            odometry = MultiscaleRegistration([
                (1.0, ICPOdometry(25, **params1)),
                (0.5, ICPOdometry(25, **params2)),
                (0.5, ICPOdometry(30, **params3))
            ])
        elif method == OdometryMethod.GROUND_TRUTH:
            odometry = GTOdometry()

        return odometry


class TestOdometry(rflow.Interface):
    """Warm-up testing for odometry methods.
    """

    def evaluate(self, dataset, preproc, odometry, frame_idxs, model=None,
                 device="cuda:0"):
        """Evaluate
        """
        from slamtb.camera import RTCamera
        from slamtb.viz import geoshow
        from slamtb.pointcloud import PointCloud
        from slamtb.metrics import (relative_translational_error,
                                    relative_rotational_error,
                                    absolute_translational_error)

        from slamfeat.table import make_table

        frames = []
        features = []

        for frame_idx in frame_idxs:
            frame = preproc(dataset[frame_idx])
            if model is not None:
                feats = model.extract_frame_features(frame).to(device).clone()
            else:
                feats = None

            frames.append(frame)
            features.append(feats)

        prev_frame = frames[0]
        prev_feat = features[0]

        camera = RTCamera()
        traj = {prev_frame.info.timestamp: camera}
        gt_traj = {prev_frame.info.timestamp: prev_frame.info.rt_cam}

        for frame, feats in zip(frames[1:], features[1:]):
            result = odometry.estimate_frame(
                frame, prev_frame,
                source_feats=feats,
                target_feats=prev_feat, device=device,
            )

            prev_frame = frame
            prev_feat = feats

            camera = camera.transform(result.transform)
            traj[frame.info.timestamp] = camera

            gt_traj[frame.info.timestamp] = frame.info.rt_cam

        geos = []
        for frame, camera in zip(frames, traj.values()):
            pcl = PointCloud.from_frame(frame, world_space=False,
                                        compute_normals=False)[0]
            pcl.itransform(camera.cam_to_world.float())
            geos.append(pcl)

        ate = absolute_translational_error(gt_traj, traj)
        rte = relative_translational_error(gt_traj, traj)
        rre = relative_rotational_error(gt_traj, traj)

        meas = {
            "ATE-RMSE:": ate.dot(ate).mean().item(),
            "RTE-RMSE:": rte.dot(rte).mean().item(),
            "RRE-RMSE:": rre.dot(rre).mean().item()
        }

        print(make_table(meas, odometry.__class__.__name__))

        geoshow(geos,
                title=f"Testing {odometry.__class__.__name__}",
                invert_y=True)


class ComputeOdometry(rflow.Interface):
    """Run a registration method through a dataset.
    """

    def evaluate(self, resource, dataset, preproc, odometry, model=None,
                 num_frames=500):
        """Evaluate
        """
        from tqdm import tqdm

        from slamtb.data.tumrgbd import write_trajectory
        from slamtb.camera import RTCamera
        from slamtb.registration import RegistrationVerifier

        device = "cuda:0"

        if model is not None:
            model = model.to(device)
        traj = {}

        icp_verifier = RegistrationVerifier

        prev_frame = None
        prev_feats = None

        accum_pose = RTCamera()

        for frame_idx in tqdm(range(1, num_frames)):
            frame = preproc(dataset[frame_idx])

            if prev_frame is None:
                prev_frame = preproc(dataset[frame_idx - 1])
                traj[prev_frame.info.timestamp] = accum_pose.clone()

            if model is None:
                result = odometry.estimate_frame(
                    frame, prev_frame, device=device)
            else:
                if prev_feats is None:
                    tgt_image_feat = model.extract_frame_features(
                        prev_frame).to(device)
                src_image_feat = model.extract_frame_features(
                    frame).to(device)
                result = odometry.estimate_frame(
                    frame, prev_frame,
                    source_feats=src_image_feat,
                    target_feats=tgt_image_feat,
                    device=device)

            if not icp_verifier(result):
                print("Tracking fail")

            accum_pose = accum_pose.transform(
                result.transform)
            traj[frame.info.timestamp] = accum_pose.clone()

            prev_frame = frame

        write_trajectory(resource.filepath, traj)

        return traj

    def load(self, resource):
        """Load"""
        from slamtb.data.tumrgbd import read_trajectory
        return read_trajectory(resource.filepath)


class ViewOdometry(rflow.Interface):
    """View the dataset using the generated odometry
    """

    def evaluate(self, dataset, trajectory, title="Odometry"):
        """Evaluate.
        """
        from slamtb.viz.datasetviewer import DatasetViewer

        trajectory = list(trajectory.values())

        class _OverwriteTrajectory:
            def __getitem__(self, idx):
                frame = dataset[idx]
                return frame

            def __len__(self):
                return len(trajectory)

        viewer = DatasetViewer(_OverwriteTrajectory(),
                               title=title, camera_view=False)
        viewer.run()


class Metrics(rflow.Interface):
    """Compute the odometry metrics
    """

    def evaluate(self, dataset, trajectory):
        """Evaluate.
        """
        from slamtb.metrics.trajectory import (absolute_translational_error,
                                               relative_translational_error,
                                               relative_rotational_error)

        total = dset_len = len(dataset)
        traj_len = len(trajectory)

        if dset_len != traj_len:
            total = min(dset_len, traj_len)

        gt_trajectory = {dataset.get_info(i).timestamp: dataset.get_info(
            i).rt_cam for i in range(0, total)}

        ate = absolute_translational_error(gt_trajectory, trajectory)
        rte = relative_translational_error(gt_trajectory, trajectory)
        rre = relative_rotational_error(gt_trajectory, trajectory)

        meas = {
            "ATE-RMSE": ate.dot(ate).mean().sqrt().item(),
            "RTE-RMSE": rte.dot(rte).mean().sqrt().item(),
            "ROT-RMSE": rre.dot(rre).mean().sqrt().item()
        }
        print(meas)

        self.save_measurement(meas)

        return meas
