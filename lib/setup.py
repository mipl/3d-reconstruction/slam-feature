"""A setuptools based setup module for slam-feature
"""

import sys
import os

from setuptools import find_packages

try:
    from skbuild import setup
except ImportError:
    print('scikit-build is required to build from source.', file=sys.stderr)
    print('Please run:', file=sys.stderr)
    print('', file=sys.stderr)
    print('  python -m pip install scikit-build')
    sys.exit(1)

import torch
TORCH_ROOT = os.path.dirname(torch.__file__)


def _forbid_publish():
    argv = sys.argv
    blacklist = ['register', 'upload']

    for command in blacklist:
        if command in argv:
            values = {'command': command}
            print('Command "%(command)s" has been blacklisted, exiting...' %
                  values)
            sys.exit(2)


_forbid_publish()

REQUIREMENTS = [
    'cachetools', 'gitpython', 'numpy-quaternion',
    'pytorch-lightning', 'segmentation-models-pytorch', 'torchnet', 'trimesh'
]


setup(
    name='slam-feature',
    version='0.0.1',
    description='Auxillary module for slam-features experiments',
    license='MIT',
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    entry_points={
        'console_scripts': [
            'slamfeat-posesrec=slamfeat.app.posesrec.__main__:_main',
            'slamfeat-posesplay=slamfeat.app.posesplay.__main__:_main']
    },
    cmake_args=[f'-DCMAKE_PREFIX_PATH={TORCH_ROOT}']
)
