"""Load the TUM-RGBD dataset.
"""

from pathlib import Path
import rflow


class LoadTUMRGBD(rflow.Interface):
    """Load the TUM-RGBD dataset
    """

    def evaluate(self, resource):
        from slamtb.data.tumrgbd import load_tumrgbd
        from slamtb.data import TransformCameraDataset

        dataset = load_tumrgbd(resource.filepath, rotate=True)

        base = dataset.get_info(0).rt_cam.matrix.inverse()

        return TransformCameraDataset(dataset, base)


@rflow.graph()
def tumrgbd(g):
    """Load the main TUM-RGBD scenes.
    """
    from slamfeat.viz.nodes import ViewRGBDDataset

    scenes = [("f1_desk", "rgbd_dataset_freiburg1_desk"),
              ("f2_desk", "rgbd_dataset_freiburg2_desk"),
              ("f1_room", "rgbd_dataset_freiburg1_room"),
              ("f3_office", "rgbd_dataset_freiburg3_long_office_household")]

    for scene_id, scene_path in scenes:
        with g.prefix(scene_id + ".") as sub:
            sub.dataset = LoadTUMRGBD(rflow.FSResource(
                Path("TUM-RGBD") / scene_path))
            sub.view = ViewRGBDDataset()
            with sub.view as args:
                args.dataset = sub.dataset
                args.title = "TUM-RGBD {} Ground truth".format(scene_id)
                args.identify = False
