"""Loading and preparation of SceneNN scenes.
"""

from pathlib import Path
import rflow


class LoadSceneNN(rflow.Interface):
    """Loads a SceneNN scene
    """

    def evaluate(self, resource, trajectory_log, mask_dirpath=None):
        """Evaluate.
        """

        from slamtb.data.scenenn import load_scenenn
        dataset = load_scenenn(resource.filepath,
                               trajectory_log.filepath,
                               mask_dirpath=mask_dirpath.filepath)

        self.save_measurement({"Frames": len(dataset)})

        return dataset

    def load(self, resource, trajectory_log, mask_dirpath):
        """Load."""
        return self.evaluate(resource, trajectory_log, mask_dirpath)


class LoadBackgroundSegIDs(rflow.Interface):
    """Load the segmentation integer ids.
    """

    def evaluate(self, resource):
        """Evaluate.
        """
        from slamtb.data.scenenn import load_annotations

        annotations = load_annotations(resource.filepath)

        background_seg = []
        for annot in annotations:
            text = annot['text']
            if text.startswith("wall") or text.startswith("floor"):
                background_seg.append(int(annot['id']))

        return background_seg


class ConvertToFTB(rflow.Interface):
    """Convert the dataset into the FTB format.
    """

    def evaluate(self, resource, dataset, start_at_eye=True):
        """Evaluate.
        """
        from slamtb.data.ftb import write_ftb

        write_ftb(resource.filepath, dataset)
        return self.load(resource, dataset, start_at_eye)

    def load(self, resource, start_at_eye):
        """Load."""
        from slamtb.data.ftb import load_ftb
        from slamtb.data import set_start_at_eye

        dataset = load_ftb(resource.filepath)
        if start_at_eye:
            dataset = set_start_at_eye(dataset)

        return dataset


class GroundtruthDataset(rflow.Interface):
    """Get a dataset where the depth images are from the actual polygonal
    ground-truth surface.
    """

    def evaluate(self, gt_mesh, dataset):
        """Evaluate.
        """
        from slamfeat.data.synthetic import GroundTruthDepthDataset

        mesh_dataset = GroundTruthDepthDataset(gt_mesh, dataset, depth_scale=5000.0,
                                               depth_bias=0.0)

        return mesh_dataset


class DatasetToMeshTransform(rflow.Interface):
    """Returns the transformation matrix to apply to the ground truth
    mesh.

    """

    def evaluate(self, dataset):
        """Evaluate.
        """
        return dataset.get_info(0).rt_cam.matrix


@rflow.graph()
def scenenn(g):
    """Load the evaluated SceneNN scenes.
    """
    # pylint: disable=too-many-statements

    from slamfeat.constants import SCENENN_SCENES
    from slamfeat.viz.nodes import ViewRGBDDataset, ViewPCL
    from slamfeat.data.nodes import (LoadGeo, SamplePCLFromMesh,
                                     CreateDatasetVisibilityGraph)

    in_base_path = Path('SceneNN')
    out_base_path = Path('./SceneNN-ftb')

    for scene_id in SCENENN_SCENES:
        with g.prefix(scene_id + '.') as sub:
            sub.load_raw = LoadSceneNN(rflow.FSResource(
                in_base_path / 'oni' / (scene_id + '.oni')))
            cad_mesh_path = in_base_path / scene_id / (scene_id + '_color.ply')

            with sub.load_raw as args:
                args.trajectory_log = rflow.FSResource(
                    in_base_path / scene_id / 'trajectory.log')
                args.mask_dirpath = rflow.FSResource(
                    in_base_path / scene_id / 'mask')

            sub.load_raw.show = False

            sub.dataset = ConvertToFTB(
                rflow.FSResource(out_base_path / scene_id))
            sub.dataset.args.dataset = sub.load_raw

            sub.dataset_view = ViewRGBDDataset()
            with sub.dataset_view as args:
                args.dataset = sub.dataset
                args.title = f"SceneNN (FTB): {scene_id}"
                args.identify = True

            sub.gt_mesh = LoadGeo(rflow.FSResource(cad_mesh_path))

            sub.gt_dataset_raw = GroundtruthDataset()
            sub.gt_dataset_raw.show = False
            with sub.gt_dataset_raw as args:
                args.gt_mesh = sub.gt_mesh
                args.dataset = sub.load_raw

            sub.gt_dataset_raw_view = ViewRGBDDataset()
            with sub.gt_dataset_raw_view as args:
                args.dataset = sub.gt_dataset_raw
                args.title = f"SceneNN (GT): {scene_id}"

            sub.gt_dataset = ConvertToFTB(rflow.FSResource(
                Path("SceneNN-ftb") / f"{scene_id}-gt"))
            with sub.gt_dataset as args:
                sub.gt_dataset.args.dataset = sub.gt_dataset_raw
                sub.gt_dataset.args.start_at_eye = False

            sub.gt_dataset_view = ViewRGBDDataset()
            with sub.gt_dataset_view as args:
                args.dataset = sub.gt_dataset
                args.title = f"SceneNN (GT): {scene_id}"

            sub.gt_pcl = SamplePCLFromMesh(
                rflow.FSResource((in_base_path / scene_id / 'dense.ply').absolute()))
            with sub.gt_pcl as args:
                args.mesh = sub.gt_mesh
                args.num_points = 1_000_000

            sub.gt_pcl_view = ViewPCL()
            with sub.gt_pcl_view as args:
                args.ref_mesh = sub.gt_mesh
                args.pcl = sub.gt_pcl

            sub.dataset_to_mesh_transform = DatasetToMeshTransform()
            sub.dataset_to_mesh_transform.args.dataset = sub.load_raw

            sub.bg_ids = LoadBackgroundSegIDs(rflow.FSResource(
                in_base_path / scene_id / f'{scene_id}.xml'))

            sub.visibility_graph = CreateDatasetVisibilityGraph(rflow.FSResource(
                in_base_path / f"{scene_id}-visibility-graph.pkl"))
            with sub.visibility_graph as args:
                args.dataset = sub.dataset
                args.max_frames = 5000
