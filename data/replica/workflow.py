"""Set trajectories and export Replica scenes to FTB files.
"""
from pathlib import Path

import rflow

# pylint: disable=too-many-statements


class RecordPose(rflow.Interface):
    def evaluate(self, in_mesh_path, out_traj_path):
        from slamfeat.app.posesrec import run_record_pose

        run_record_pose(in_mesh_path, out_traj_path)


class PlayPose(rflow.Interface):
    def evaluate(self, in_mesh_path, in_traj_path):
        from slamfeat.app.posesplay import run_pose_play
        run_pose_play(in_mesh_path, in_traj_path)


class ToFTBDataset(rflow.Interface):
    def evaluate(self, resource, pose_path, scene_path,
                 sens_width, sens_height, frames_per_sec=32.0):

        import os

        pose_path = Path(pose_path.filepath)
        scene_path = Path(scene_path.filepath)

        Path(resource.filepath).mkdir(exist_ok=True, parents=True)
        cmd = "FTBRender {} {} {} {} {}".format(
            str(scene_path / "mesh.ply"), str(scene_path / "textures"),
            str(pose_path), resource.filepath,
            str(scene_path / "glass.sur"))
        print(cmd)
        exit_code = os.system(cmd)

        if exit_code != 0:
            self.fail("FTBRender exited with non zero error code")

        return self.load(resource)

    def load(self, resource):
        from slamtb.data.ftb import load_ftb

        return load_ftb(str(resource.filepath),
                        info_file=Path(resource.filepath) / "frames.json")


class OutputSegmentation(rflow.Interface):
    def evaluate(self, resource, pose_path, scene_path, frames_per_sec=32.0):
        import time
        import json

        import torch
        import numpy as np
        import cv2

        import tenviz

        from slamtb.camera import KCamera

        from slamfeat.viz.replica import create_draw_prg
        from slamfeat.data.pose import read_pose_json

        import matplotlib.pyplot as plt
        from tqdm import tqdm

        scene_path = Path(scene_path.filepath) / "habitat/mesh_semantic.ply"

        mesh = tenviz.io.read_3dobject(scene_path).torch()

        with open(pose_path.filepath, 'r') as file:
            pose_dict = read_pose_json(file)

        kcam = KCamera(torch.tensor([[544.47329, 0.0, 320],
                                     [0.0, 544.47329, 240],
                                     [0.0, 0.0, 1.0]], dtype=torch.float))
        proj_matrix = kcam.get_opengl_projection_matrix(
            0.1, 1000.0, dtype=torch.float)

        base_ftb_path = Path(resource.filepath)
        base_ftb_path.mkdir(parents=True, exist_ok=True)

        context = tenviz.Context(640, 480)
        with context.current():
            mesh_node = create_draw_prg(
                mesh, segmentation=True)
            framebuffer = tenviz.create_framebuffer({
                0: tenviz.FramebufferTarget.RGBUint8,
                1: tenviz.FramebufferTarget.RGBInt32
            })

        num_frames = int(pose_dict.get_final_time() * frames_per_sec)
        with open(base_ftb_path / "frames.json") as file:
            ftb_dict = json.load(file)

        for i in tqdm(range(num_frames), total=num_frames):
            curr_time = float(i)/frames_per_sec
            curr_pose = pose_dict[curr_time]

            context.render(proj_matrix, curr_pose.to_matrix().float(),
                           framebuffer, [mesh_node])

            with context.current():
                image = framebuffer[1].to_tensor().cpu().numpy()

            image = np.flipud(image)
            image_fname = "frame_{:05d}_seg.png".format(i)
            cv2.imwrite(str(base_ftb_path / image_fname),
                        image)

            ftb_dict["root"][i]["segmentation"] = image_fname

        with open(base_ftb_path / "frames.json", "w") as file:
            json.dump(ftb_dict, file, indent=1)


class LoadBackgroundSegIDs(rflow.Interface):
    def evaluate(self, resource):
        """Evaluate.
        """
        import json

        with open(resource.filepath, 'r') as file:
            semantic_list = json.load(file)["segmentation"]

        seg_by_id = {}
        for seg in semantic_list:
            seg_by_id[int(seg["id"])] = seg

        bg_ids = set()
        for nid, seg in seg_by_id.items():
            if seg["class"] != "wall":
                continue

            bg_ids.add(nid)

            for child in seg["children"]:
                bg_ids.add(int(child))

        return list(bg_ids)

    def load(self, resource):
        """Load.
        """
        return self.evaluate(resource)


class ToPDC(rflow.Interface):
    def evaluate(self, resource, dataset, scene_id):
        from slamfeat.data.pdc import write_pdc

        bg_seg_values = {
            "hotel_0": [34, 17, 154, 94, 19, 5, 4, 68, 52, 3, 8, 1, 85, 71, 146],
            "apartment_0": [255, 58, 199, 188, 20, 38, 85, 22],
        }

        write_pdc(resource.filepath, dataset,
                  mask_seg_values=bg_seg_values[scene_id],
                  visible_mask=True,
                  skip_empty_frames=True)

        return self.load(resource)

    def load(self, resource):
        from slamfeat.data.pdc import load_pdc
        return load_pdc(resource.filepath)


class ToKLG(rflow.Interface):
    def evaluate(self, resource, dataset):
        from slamtb.data.klg import write_klg

        with open(resource.filepath, 'wb') as file:
            write_klg(dataset, file, format_depth_scale=3000)

    def load(self):
        pass


_REPLICA_SCENES = ["room_0", "room_1", "room_2",
                   "apartment_0", "apartment_1", "apartment_2",
                   "frl_apartment_0", "frl_apartment_1", "frl_apartment_2",
                   "frl_apartment_3", "frl_apartment_4",
                   "hotel_0", "office_1"]


@rflow.graph()
def replica(g):
    """Generate trajectories, write RGBD dataset and loading.
    """
    from slamfeat.data.nodes import (LoadGeo, SamplePCLFromDataset,
                                     CreateDatasetVisibilityGraph)
    from slamfeat.viz.nodes import ViewRGBDDataset, ViewPCL

    base_path = Path(__file__).parent

    for scene_id in _REPLICA_SCENES:
        with g.prefix(scene_id + '.') as sub:
            sub.rec_pose = RecordPose()

            mesh_path = base_path / "Replica-Dataset" / scene_id / "mesh.ply"
            traj_path = (
                base_path / "trajectories" / (scene_id + '_0.json'))
            with sub.rec_pose as args:
                args.in_mesh_path = mesh_path
                args.out_traj_path = traj_path

            sub.play_pose = PlayPose()
            with sub.play_pose as args:
                args.in_mesh_path = mesh_path
                args.in_traj_path = traj_path

            sub.dataset = ToFTBDataset(
                rflow.FSResource("replica-ftb/" + scene_id))
            with sub.dataset as args:
                args.pose_path = rflow.FSResource(traj_path)
                args.scene_path = rflow.FSResource(base_path / 'Replica-Dataset'
                                                   / scene_id)
                args.sens_width = 640
                args.sens_height = 480
                args.frames_per_sec = 24

            sub.seg = OutputSegmentation(
                rflow.FSResource("replica-ftb/" + scene_id))
            sub.seg.require(sub.dataset)
            with sub.seg as args:
                args.pose_path = rflow.FSResource(traj_path)
                args.scene_path = rflow.FSResource(base_path / 'Replica-Dataset'
                                                   / scene_id)

            sub.bg_ids = LoadBackgroundSegIDs(rflow.FSResource(
                base_path / 'Replica-Dataset' / scene_id / "semantic.json"))

            sub.pose_graph = CreateDatasetVisibilityGraph(rflow.FSResource(
                f"replica-ftb/{scene_id}-visibility-graph.pkl"))
            with sub.pose_graph as args:
                args.dataset = sub.dataset

            sub.view = ViewRGBDDataset()
            with sub.view as args:
                args.dataset = sub.dataset
                args.title = scene_id

            sub.gt_mesh = LoadGeo(rflow.FSResource(mesh_path))
            with sub.gt_mesh as args:
                args.transform = [[1, 0, 0, 0],
                                  [0, 0, 1, 0],
                                  [0, 1, 0, 0],
                                  [0, 0, 0, 1]]

            sub.gt_pcl = SamplePCLFromDataset(
                rflow.FSResource(f"replica-ftb/{scene_id}.ply"))
            with sub.gt_pcl as args:
                args.dataset = sub.dataset
                args.num_samples = 500
                args.sample_per_snap = 10768

            sub.gt_pcl_view = ViewPCL()
            with sub.gt_pcl_view as args:
                args.ref_mesh = sub.gt_mesh
                args.pcl = sub.gt_pcl

            sub.pdc = ToPDC(rflow.FSResource(
                f"replica-pdc/{scene_id}/processed"))
            with sub.pdc as args:
                args.dataset = sub.dataset
                args.scene_id = scene_id

            sub.view_pdc = ViewRGBDDataset()
            with sub.view_pdc as args:
                args.dataset = sub.pdc
                args.title = f"{scene_id} - pdc"

            sub.to_klg = ToKLG(rflow.FSResource(f"{scene_id}.klg"))
            with sub.to_klg as args:
                args.dataset = sub.dataset
