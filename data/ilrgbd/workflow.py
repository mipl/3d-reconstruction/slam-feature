"""Load Indoor Lidar-RGBD Scan Dataset scenes.
http://redwood-data.org/indoor_lidar_rgbd/download.html
"""

from pathlib import Path

import rflow

from slamfeat.viz.nodes import ViewTrajectory, ViewRGBDDataset


class LoadILRGBD(rflow.Interface):
    """Load a ILRGBD scene
    """

    def evaluate(self, resource):
        from slamtb.data.ilrgbd import load_ilrgbd
        from slamtb.data import set_start_at_eye

        base_dir = Path(resource.filepath)
        traj_filename = base_dir.stem + ".log"

        dataset = load_ilrgbd(base_dir, base_dir / traj_filename)

        return set_start_at_eye(dataset)


ILRGBD_SCENES = ["apartment", "bedroom", "boardroom", "lobby", "loft"]


class ToPDC(rflow.Interface):
    """Converts a dataset into the Dense Object Nets format.

    """

    def evaluate(self, resource, dataset):
        from slamtb.data.pdc import write_pdc

        write_pdc(resource.filepath, dataset, 15)


@rflow.graph()
def ilrgbd(g):
    """Load some scenes from IL-RGBD dataset.
    """
    from slamfeat.data.nodes import LoadGeo, CreateDatasetVisibilityGraph

    in_base_path = Path("IL-RGBD")

    for scene_id in ILRGBD_SCENES:
        with g.prefix(scene_id + '.') as sub:
            sub.dataset = LoadILRGBD(rflow.FSResource(
                in_base_path / scene_id), show=False)

            sub.view = ViewRGBDDataset()
            with sub.view as args:
                args.dataset = sub.dataset
                args.title = scene_id

            sub.view_trajectory = ViewTrajectory()
            with sub.view_trajectory as args:
                args.dataset = sub.dataset
                args.title = f"Trajectory of {scene_id}"

            sub.visibility_graph = CreateDatasetVisibilityGraph(rflow.FSResource(
                in_base_path / f"{scene_id}-visibility-graph.pkl"))
            with sub.visibility_graph as args:
                args.dataset = sub.dataset

            sub.gt_pcl = LoadGeo(rflow.FSResource(
                in_base_path / scene_id / f"{scene_id}_low.ply"))
            with sub.gt_pcl as args:
                args.center = True
                args.is_point_cloud = True

            sub.to_pdc = ToPDC(rflow.FSResource(f"{scene_id}-pdc"))
            with sub.to_pdc as args:
                args.dataset = sub.dataset
