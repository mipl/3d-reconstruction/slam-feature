###
# Main image
##
main-build:
	docker build -t otaviog/slam-feature:cudagl-11.2 .

main-push:
	docker push otaviog/slam-feature:cudagl-11.2

###
# Base image
##
base-build:
	docker build --target base -t otaviog/slam-feature:base .

base-push:
	docker push otaviog/slam-feature:base

###
# Dev image
##
dev-build: base-build
	docker build -t otaviog/slam-feature:dev -f development.dockerfile .

dev-push:
	docker push otaviog/slam-feature:dev
