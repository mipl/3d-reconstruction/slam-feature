FROM otaviog/slam-toolbox:cudagl-11.2 AS base
LABEL maintaner=otavio.b.gomes@gmail.com

USER root

RUN apt install -yq libcgal-dev

ADD environment.yml .
RUN conda env update -n base --file environment.yml

RUN mkdir -p /workspaces
WORKDIR /workspaces

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/miniconda3/lib

###
# Main image
##
FROM base AS slam-feature

ADD . /slam-feature
WORKDIR /slam-feature
RUN python setup.py install -- -- -j5

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display
