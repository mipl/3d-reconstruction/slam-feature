#!/usr/bin/env python
"""
Run reports on the registration experiments
"""

import gc
from itertools import product

import click
from pytablewriter import MarkdownTableWriter
import rflow
import torch


def _flush_garbagge():
    rflow.core._GRAPH_DICT = {}
    torch.cuda.empty_cache()
    gc.collect()
    torch.cuda.empty_cache()


@click.group()
def cli():
    """
    Reconstruction experiments report maker.
    """
    pass


@cli.command()
def feature():
    writer = MarkdownTableWriter()
    # scenes = ["apartment_0", "apartment", "045"]
    scenes = ["045", "apartment", "apartment_0"]
    exps = ["rnd_real_3", "rnd_real_8", "pspd", "rgb"]
    # exps = ["rnd_real_3"]

    writer.headers = ["Scene "] + [
        " ".join((scn, metric))
        for scn, metric in product(scenes, ["NN-ACC"])]

    value_matrix = []

    for exp_name in exps:
        exp_g = rflow.open_graph(".", exp_name)
        exp_row = [exp_name]
        for scene in scenes:
            meas = exp_g[f"{scene}.feat_metrics"].call()
            # exp_row.extend([meas["L2 Mean"], meas["L2 Std"]])
            exp_row.extend([meas["NN-ACC"]])
        value_matrix.append(exp_row)

    writer.value_matrix = value_matrix
    writer.write_table()


@cli.command()
def registration():
    scenes = [
        ("IL-RGBD Bedroom", "bedroom"),
        ("IL-RGBD Loft", "loft"),
        ("IL-RGBD Boardroom", "boardroom"),
        ("SceneNN 021", "021"),
        ("SceneNN 030", "030"),
        ("SceneNN 045", "045"),
        ("SceneNN 081", "081"),
        ("SceneNN 087", "087"),
    ]
    exps = [
        ("ColoredICP", "colored_icp"),
        ("Geom ICP", "geom"),
        ("RGB ICP", "rgb"),
        # ("Dilated ResNet D=8", "rnd_real_8"),
        ("Dilated ResNet D=16", "rnd_real_16"),
        ("Dilated ResNet D=32", "rnd_real_32"),
        # ("Dilated ResNet D=64", "rnd_real_64")
    ]

    value_matrix = []

    for scene_name, scene_id in scenes:
        scene_row = [scene_name]
        for _, exp_id in exps:
            meas = rflow.open_graph(".", exp_id)[
                f"{scene_id}.register_metrics"].call()
            scene_row.extend([meas["MTE"], meas["MRE"]])
            exp_g = None
            _flush_garbagge()

        value_matrix.append(scene_row)

    writer = MarkdownTableWriter()
    writer.headers = ["Algorithm/Metric"] + [
        " ".join((alg_name, metric))
        for alg_name, metric in product([alg_name for alg_name, _ in exps],
                                        ["MTE", "MRE"])]

    writer.value_matrix = value_matrix
    writer.write_table()

    latex_table = ""
    for row in value_matrix:
        latex_row = f"{row[0]}"
        mte_list = [row[i] for i in range(1, len(row), 2)]
        rte_list = [row[i] for i in range(2, len(row), 2)]

        best_mte = min(mte_list)
        best_rte = min(rte_list)

        for mte, rte in zip(mte_list, rte_list):
            if not mte > best_mte:
                latex_row += f" & \\bs{mte:.4f}"
            else:
                latex_row += f" & {mte:.4f}"
            if not rte > best_rte:
                latex_row += f" & \\bs{rte:.4f}"
            else:
                latex_row += f" & {rte:.4f}"
        latex_table = latex_table + "\n" + latex_row + "\\\\"

    print(latex_table)

if __name__ == '__main__':
    cli()
