"""Experiment for registraion point clouds using features.
"""
from pathlib import Path


import rflow


def _build_registration_graph(g, scene_g, scene_id, dataset, max_frames,
                              device):
    # pylint: disable=too-many-statements
    from slamfeat.registration.nodes import RegisterSubmaps, TestRegistration
    from slamfeat.posegraph.nodes import (CreateSurfelSubmapGraph,
                                          PertubTransformationEdges,
                                          PoseGraphTransformationMetrics,
                                          PoseGraphFeaturesMetrics,
                                          GetGroundTruthPoseGraph)
    from slamfeat.viz.nodes import (ViewPoseGraph, ViewPoseGraphFeatures,
                                    ViewPoseGraphDenseMatching)

    traverse_nodes = None

    base_path = Path(scene_g.name) / scene_id
    scene_g.submaps = CreateSurfelSubmapGraph(rflow.FSResource(
        base_path / "submaps.torch"))
    with scene_g.submaps as args:
        args.dataset = dataset
        args.submap_size = 100
        args.stable_time_thresh = 12
        args.stable_conf_thresh = 8
        args.odometry = g.odometry
        args.odo_feature_model = None
        args.feature_model = g.model
        args.max_frames = max_frames

    scene_g.submaps_view = ViewPoseGraph()
    with scene_g.submaps_view as args:
        args.pose_graph = scene_g.submaps
        args.title = "Ground-truth pose graph"

    if scene_g.name == "rgb":
        gt_pose_graph = scene_g.gt_pose_graph = GetGroundTruthPoseGraph(
            rflow.FSResource(base_path / "gt-pose-graph.torch"), show=True)
        with scene_g.gt_pose_graph as args:
            args.submap_pg = scene_g.submaps
            args.gt_odometry_pg = scene_g.submaps
            args.overlap_rate = 0.3
        scene_g.gt_view = ViewPoseGraph()
        with scene_g.gt_view as args:
            args.pose_graph = gt_pose_graph
            args.title = "Perturb pose graph"
            args.traverse_nodes = [2, 0]
    else:
        rgb_g = rflow.open_graph(".", "rgb")
        gt_pose_graph = rgb_g[f"{scene_id}.gt_pose_graph"]

    scene_g.view_features = ViewPoseGraphFeatures()
    with scene_g.view_features as args:
        args.pose_graph = scene_g.submaps
        args.num_color_clusters = 256
        args.num_pca_components = None

    scene_g.view_feature_matches = ViewPoseGraphDenseMatching()
    with scene_g.view_feature_matches as args:
        args.pose_graph = scene_g.submaps
        args.title = f"{scene_g.name}"

    scene_g.feat_metrics = PoseGraphFeaturesMetrics()
    with scene_g.feat_metrics as args:
        args.pose_graph = scene_g.submaps

    base_path = Path(f"{scene_g.name}") / scene_id

    ##
    # Add pertubation
    scene_g.perturb = PertubTransformationEdges(rflow.FSResource(
        base_path / "pertub-pose-graph.torch", make_dirs=True))
    with scene_g.perturb as args:
        args.pose_graph = gt_pose_graph
        args.rand_seed = 19981
        args.max_rot_degrees = 5
        args.max_translation = 0.1
    with scene_g.prefix("perturb_") as sub:
        sub.view = ViewPoseGraph()
        with sub.view as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.perturb
            args.title = "Perturb pose graph"
            args.traverse_nodes = traverse_nodes

        sub.metrics = PoseGraphTransformationMetrics()
        with sub.metrics as args:
            args.gt_pose_graph = gt_pose_graph
            args.pred_pose_graph = scene_g.perturb

    ##
    # Dense registration
    scene_g.register = RegisterSubmaps(
        rflow.FSResource(base_path / "registration-pose-graph.torch", make_dirs=True))
    with scene_g.register as args:
        args.submaps = scene_g.submaps
        args.pose_graph = scene_g.perturb
        args.registration = g.method
        args.device = device

    with scene_g.prefix("register_") as sub:
        sub.test = TestRegistration()
        with sub.test as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.perturb
            args.gt_pose_graph = gt_pose_graph
            args.registration = g.method
            args.nodes = [0, 1, 2]
            args.device = device
            args.ignore_edge_transform = False

        sub.view = ViewPoseGraph()
        with sub.view as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.register
            args.title = scene_g.name
            args.traverse_nodes = traverse_nodes

        sub.metrics = PoseGraphTransformationMetrics()
        with sub.metrics as args:
            args.gt_pose_graph = gt_pose_graph
            args.pred_pose_graph = scene_g.register


def _build_experiments_graph(g, device):
    from slamfeat.registration.nodes import (OdometryFactory, OdometryMethod)

    scenenn_g = rflow.open_graph("../../data/scenenn", "scenenn")
    ilrgbd_g = rflow.open_graph("../../data/ilrgbd", "ilrgbd")

    g.odometry = OdometryFactory(show=False)
    with g.odometry as args:
        args.method = OdometryMethod.GROUND_TRUTH

    for scene_id, dataset_node, max_frames in [
            ("021", scenenn_g['021.dataset'], 3000),
            ("030", scenenn_g['030.dataset'], 3000),
            ("016", scenenn_g['016.dataset'], 3000),
            ("045", scenenn_g['045.dataset'], 3000),
            ("081", scenenn_g['081.dataset'], 3000),
            ("087", scenenn_g['087.dataset'], 3000),
            ("apartment", ilrgbd_g["apartment.dataset"], 3000),
            ("boardroom", ilrgbd_g["boardroom.dataset"], 3000),
            ("bedroom", ilrgbd_g["bedroom.dataset"], 3000),
            ("loft", ilrgbd_g["loft.dataset"], 3000),
            ("lobby", ilrgbd_g["lobby.dataset"], 3000),
            ("test", scenenn_g["030.dataset"], 1000)]:
        with g.prefix(f"{scene_id}.") as sub:
            _build_registration_graph(
                g, sub, scene_id, dataset_node, max_frames, device)


@rflow.graph()
def colored_icp(g):
    """Workflow using the Open3D's pipeline"""
    from slamfeat.colorfeature import color_feature_factory, ColorMode
    from slamfeat.registration.open3d import ColoredICP

    g.model = color_feature_factory(
        color_type=ColorMode.RGB, blur=False, device="cpu:0")
    g.method = rflow.open_graph("..", "pairwise_algorithms").colored_icp

    rflow.make_factory(
        ColoredICP,
        scale_iters=[(0.01, 50), (0.02, 30), (0.03, 15)]
        # scale_iters=[(-1.0, 50), (0.015, 50), (0.03, 50)]
    )

    _build_experiments_graph(g, "cpu:0")


@rflow.graph()
def geom(g):
    """Workflow using only geometric term"""
    g.model = None
    g.method = rflow.open_graph("..", "pairwise_algorithms").geometric_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def gray(g):
    """Workflow using RGB as the features"""
    from slamfeat.colorfeature import color_feature_factory, ColorMode
    g.model = color_feature_factory(
        color_type=ColorMode.GRAY, blur=False, device="cpu:0")
    g.method = rflow.open_graph("..", "pairwise_algorithms").gray_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rgb(g):
    """Workflow using RGB as the features"""
    from slamfeat.colorfeature import color_feature_factory, ColorMode
    g.model = color_feature_factory(
        color_type=ColorMode.RGB, blur=False, device="cpu:0")
    g.method = rflow.open_graph("..", "pairwise_algorithms").rgb_color_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_random_3(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_syn_3").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_syn_3(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_syn_3").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_real_3(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_3").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_syn_8(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_syn_8").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_real_8(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_8").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_real_16(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_16").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_real_32(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_32").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")


@rflow.graph()
def rnd_real_64(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_64").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")

@rflow.graph()
def unet32(g):
    """Workflow using deep features"""
    g.model = rflow.open_graph("../../rgb2rgb", "unet32").train
    g.method = rflow.open_graph("..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g, "cuda:0")
