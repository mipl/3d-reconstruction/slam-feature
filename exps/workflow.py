"""
Algorithm configurations for all the expeirments
"""
import math
import rflow


@rflow.graph()
def pairwise_algorithms(g):
    """
    Configurations for the pairwise registration algorithms.
    """
    from slamtb.registration import MultiscaleRegistration
    from slamtb.registration.icp import ICP
    from slamfeat.registration.open3d import ColoredICP

    g.colored_icp = rflow.make_factory(
        ColoredICP,
        scale_iters=[(0.01, 50), (0.02, 30), (0.03, 15)]
        # This one gives better results for ColoredICP.
        # scale_iters=[(-1.0, 50), (0.015, 50), (0.03, 50)]
    )

    deep_general_options = dict(feat_residual_thresh=0.025)

    g.deep_feature_icp = rflow.make_node(
        lambda: MultiscaleRegistration([
            (-1.00, ICP(50, geom_weight=1.0,
                        feat_weight=3.0,
                        normal_angle_thresh=math.radians(70),
                        verbose=False,
                        **deep_general_options
                        )),
             (0.015, ICP(30, geom_weight=1.0,
                         feat_weight=0.0,
                         distance_threshold=0.5,
                         **deep_general_options)),
             (0.03, ICP(15, geom_weight=1.0,
                        feat_weight=0.0,
                        distance_threshold=0.5,
                        **deep_general_options))
        ]))

    g.geometric_icp = rflow.make_node(
        lambda: MultiscaleRegistration([
            (-1.00, ICP(50, geom_weight=1.0, feat_weight=0.0,
                        normal_angle_thresh=math.radians(70))),
            (0.015, ICP(30, geom_weight=1.0, feat_weight=0.0)),
            (0.03, ICP(15, geom_weight=1.0, feat_weight=0.0))]))

    g.rgb_color_icp = rflow.make_node(
        lambda: MultiscaleRegistration([
            (-1.00, ICP(50, geom_weight=1.0, feat_weight=3.0,
                        normal_angle_thresh=math.radians(70),
                        feat_residual_thresh=0.025)),
            (0.015, ICP(30, geom_weight=1.0, feat_weight=0.0)),
            (0.03, ICP(15, geom_weight=1.0, feat_weight=0.0))]))

    gray_general_options = dict(
        feat_weight=0.1, feat_residual_thresh=0.1)
    g.gray_icp = rflow.make_node(
        lambda: MultiscaleRegistration([
            (-1.00, ICP(50, geom_weight=1.0,
                        normal_angle_thresh=math.radians(70),
                        **gray_general_options)),
            (0.005, ICP(30, geom_weight=1.0,
                        distance_threshold=0.3,
                        **gray_general_options)),
            (0.025, ICP(30, geom_weight=1.0,
                        distance_threshold=0.1,
                        **gray_general_options))], verbose=True))
