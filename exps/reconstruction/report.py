#!/usr/bin/env python

import gc
from itertools import product

import click
from pytablewriter import MarkdownTableWriter, LatexTableWriter
import rflow
import torch


def _flush_garbagge():
    rflow.core._GRAPH_DICT = {}
    torch.cuda.empty_cache()
    gc.collect()
    torch.cuda.empty_cache()


@click.group()
def cli():
    """
    Reconstruction experiments report maker.
    """
    pass


@cli.command()
def features():
    """
    Report the feture metrics from the experiments.
    """
    writer = MarkdownTableWriter()
    scenes = ["045", "apartment", "apartment_0"]
    exps = ["colored_icp", "rgb", "rnd_real_3", "rnd_real_8"]

    writer.headers = ["Scene "] + [
        " ".join((scn, metric))
        for scn, metric in product(scenes, ["NN-ACC"])]

    value_matrix = []

    for exp_name in exps:
        exp_g = rflow.open_graph(".", exp_name)
        exp_row = [exp_name]
        for scene in scenes:
            meas = exp_g[f"{scene}.feat_metrics"].call()
            # exp_row.extend([meas["L2 Mean"], meas["L2 Std"]])
            exp_row.extend([meas["NN-ACC"]])
        value_matrix.append(exp_row)

    writer.value_matrix = value_matrix
    writer.write_table()


@cli.command()
def loop_closure():
    """
    Report the loop closure metrics from the experiments.
    """
    scenes = [
        "loft",
        "lobby", "boardroom", "bedroom",
        "021", "045", "081", "087"
    ]
    exps = [
        "colored_icp",
        "rgb",
        "rnd_real_16",
        "rnd_real_32",
    ]

    transform_headers = ["Algorithm/Metric"]
    roc_headers = ["Algorithm/Metric"]
    for exp_name in exps:
        transform_headers.extend([f"{exp_name}/MTE", "RRE"])
        roc_headers.extend([f"{exp_name}/Precision", "Recall"])

    transform_value_matrix = []
    roc_value_matrix = []

    for scene_name in scenes:
        scene_transform_row = [scene_name]
        scene_roc_row = [scene_name]

        for exp_name in exps:
            exp_g = rflow.open_graph(".", exp_name)
            meas = exp_g[f"{scene_name}.loop_closure_metrics"].call()
            scene_transform_row.extend([meas["MTE"], meas["MRE"]])
            scene_roc_row.extend(
                [meas["L.C. Precision"], meas["L.C. Recall"]])

            exp_g = None
            rflow.core._GRAPH_DICT = {}
            torch.cuda.empty_cache()
            gc.collect()
            torch.cuda.empty_cache()

        transform_value_matrix.append(scene_transform_row)
        roc_value_matrix.append(scene_roc_row)

    MarkdownTableWriter(headers=transform_headers,
                        value_matrix=transform_value_matrix).write_table()
    MarkdownTableWriter(headers=roc_headers,
                        value_matrix=roc_value_matrix).write_table()

    LatexTableWriter(headers=transform_headers,
                     value_matrix=transform_value_matrix).write_table()
    LatexTableWriter(headers=roc_headers,
                     value_matrix=roc_value_matrix).write_table()


@cli.command()
@click.option("--opt", default="local",
              type=click.Choice(["opt1", "local"], case_sensitive=False))
def registration(opt):
    """
    Report the registration (opt2) metrics from the experiments.
    """
    scenes = [
        "loft",
        "lobby", "boardroom", "bedroom",
        "021",
        "045", "081", "087"
    ]
    exps = [
        "colored_icp",
        "geom",
        "gray",
        "rnd_real_16",
        "rnd_real_32",
    ]

    headers = ["Algorithm/Metric"]
    for exp_name in (["Odometry"] + exps):
        headers.extend([f"{exp_name}/ATE", "RRE"])
    value_matrix = []

    for scene_name in scenes:
        scene_row = [scene_name]

        exp_g = rflow.open_graph(".", "rgb")
        meas = exp_g[f"{scene_name}.odometry_metrics"].call()
        scene_row.extend([meas["ATE"], meas["RRE"]])

        for exp_name in exps:
            exp_g = rflow.open_graph(".", exp_name)
            meas = exp_g[f"{scene_name}.{opt}_metrics"].call()
            scene_row.extend([meas["ATE"], meas["RRE"]])

            exp_g = None
            _flush_garbagge()

        value_matrix.append(scene_row)

    MarkdownTableWriter(headers=headers,
                        value_matrix=value_matrix).write_table()
    LatexTableWriter(headers=headers,
                     value_matrix=value_matrix).write_table()


@cli.command()
@click.option("--opt", default="opt2",
              type=click.Choice(["odometry", "opt2", "local"], case_sensitive=False))
def make_point_clouds(opt):
    """
    Generate merge.ply reconstructions.
    """
    scenes = [
        "loft",
        "lobby", "boardroom", "bedroom",
        "021",
        "045", "081", "087"]
    exps = [
        "colored_icp",
        "gray",
        "rnd_real_16",
        "rnd_real_32",
    ]

    if opt == "odometry":
        for scene in scenes:
            exp_g = rflow.open_graph(".", "rgb")
            exp_g[f"{scene}.odometry_merge"].call()
        return

    prefix = ""
    if opt == "local":
        prefix = "local_"
    for exp_name in exps:
        for scene in scenes:
            exp_g = rflow.open_graph(".", exp_name)
            exp_g[f"{scene}.{prefix}merge"].call()
            exp_g = None
            _flush_garbagge()


@cli.command()
def reconstruction():
    """
    Report the reconstruction accuracy metric.
    """
    scenes = [
        "loft",
        "lobby", "boardroom", "bedroom",
        "021",
        "045", "081", "087"]
    exps = [
        "colored_icp",
        "rgb",
        "rnd_real_16",
        "rnd_real_32"
    ]

    value_matrix = []
    for scene in scenes:
        exp_g = rflow.open_graph(".", "rgb")
        meas = exp_g[f"{scene}.odometry_merge"].call()

    for exp_name in exps:
        exp_row = [exp_name]
        for scene in scenes:
            exp_g = rflow.open_graph(".", exp_name)
            meas = exp_g[f"{scene}.eval_accuracy"].call()
            exp_row.extend([meas["Accuracy"]])

            exp_g = None
            _flush_garbagge()

        value_matrix.append(exp_row)

    headers = ["Scene "] + [
        " ".join((scn, metric))
        for scn, metric in product(scenes, ["Accuracy"])]
    MarkdownTableWriter(headers=headers,
                        value_matrix=value_matrix).write_table()
    LatexTableWriter(headers=headers,
                     value_matrix=value_matrix).write_table()


@cli.command()
def large():
    """
    Report the reconstruction accuracy metric.
    """
    scenes = [
        "large",
        "030"
    ]
    exps = [
        "rnd_real_16",
        # "rnd_real_32",
        "colored_icp",
    ]

    for exp_name in exps:
        for scene in scenes:
            exp_g = rflow.open_graph(".", exp_name)
            exp_g[f"{scene}.local_merge"].call()
            exp_g = None
            _flush_garbagge()


if __name__ == '__main__':
    cli()
