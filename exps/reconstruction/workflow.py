"""Experiment for registraion point clouds using features.
"""
from pathlib import Path


import rflow

_VOXEL_SIZE = 0.03


def _build_scene_reconstruction_graph(g, scene_g, scene_id, dataset, device, max_frames=4500):
    # pylint: disable=too-many-statements
    from slamfeat.registration.verifier import (Open3DRegistrationVerifier,
                                                SparseRegistrationVerifier)
    from slamfeat.registration.nodes import RegisterSubmaps, TestRegistration
    from slamfeat.posegraph.nodes import (CreateSurfelSubmapGraph,
                                          GetGroundTruthOdometry,
                                          GetGroundTruthPoseGraph,
                                          PoseGraphTransformationMetrics,
                                          PoseGraphTrajectoryMetrics,
                                          LoopClosure,
                                          MergeSubmaps,
                                          OptimizePoseGraph)
    from slamfeat.viz.nodes import (ViewPoseGraph,
                                    ViewPoseGraphDenseMatching,
                                    ViewPoseGraphSparseMatching,
                                    ViewPoseGraphTrajectory,
                                    ViewPCL)

    base_path = Path(scene_g.name) / scene_id
    traverse_nodes = None

    ###
    # Setup the Submaping
    scene_g.submaps = CreateSurfelSubmapGraph(rflow.FSResource(
        base_path / "submaps.torch"))
    with scene_g.submaps as args:
        args.dataset = dataset
        args.submap_size = 50
        args.stable_time_thresh = 6
        args.stable_conf_thresh = 5
        args.max_frames = max_frames
        args.odometry = g.odometry
        args.odo_feature_model = g.odo_model
        args.feature_model = g.model
        args.sparse_feature_model = g.sparse_feature

    if scene_g.name == "rgb":
        scene_g.gt_odometry = GetGroundTruthOdometry(
            rflow.FSResource(base_path / "gt-odometry.pkl"), show=True)
        with scene_g.gt_odometry as args:
            args.dataset = dataset
            args.submap_size = scene_g.submaps.args.submap_size
            args.max_frames = scene_g.submaps.args.max_frames

        gt_pose_graph = scene_g.gt_pose_graph = GetGroundTruthPoseGraph(
            rflow.FSResource(base_path / "gt-pose-graph.pkl"), show=True)
        with scene_g.gt_pose_graph as args:
            args.submap_pg = scene_g.submaps
            args.gt_odometry_pg = scene_g.gt_odometry
            args.overlap_rate = 0.3
        scene_g.gt_pose_graph_view = ViewPoseGraph()
        with scene_g.gt_pose_graph_view as args:
            args.submaps = scene_g.submaps
            args.pose_graph = gt_pose_graph
            args.title = "Gound truth pose graph"
            args.traverse_nodes = traverse_nodes
    else:
        gt_pose_graph = rflow.open_graph(
            ".", "rgb")[f"{scene_id}.gt_pose_graph"]

    with scene_g.prefix("odometry_") as sub:
        sub.view = ViewPoseGraph()
        with sub.view as args:
            args.pose_graph = scene_g.submaps
            args.title = f"{scene_g.name} - Odometry"
            args.invert_y = True
            args.traverse_nodes = traverse_nodes

        sub.view_traj = ViewPoseGraphTrajectory()
        with sub.view_traj as args:
            args.pose_graph1 = gt_pose_graph
            args.pose_graph2 = scene_g.submaps
            args.title = f"{scene_g.name} - Odometry"

        sub.metrics = PoseGraphTrajectoryMetrics()
        with sub.metrics as args:
            args.gt_pose_graph = gt_pose_graph
            args.pred_pose_graph = scene_g.submaps

        if scene_g.name == "rgb":
            sub.merge = MergeSubmaps(rflow.FSResource(
                base_path / "odometry-merged.ply"))
            with sub.merge as args:
                args.submaps = scene_g.submaps
                args.pose_graph = scene_g.submaps
                args.voxel_size = 0.01

    scene_g.view_dense_matches = ViewPoseGraphDenseMatching()
    with scene_g.view_dense_matches as args:
        args.pose_graph = scene_g.submaps
        args.title = f"{scene_g.name}"
        args.nodes = [0, 1]

    scene_g.view_sparse_matches = ViewPoseGraphSparseMatching()
    with scene_g.view_sparse_matches as args:
        args.pose_graph = scene_g.submaps
        args.title = f"{scene_g.name}"
        args.nodes = [4, 5]
        args.weight_threshold = 0

    base_path = Path(f"{scene_g.name}") / scene_id

    ##
    # Loop closure using global method for registration
    # scene_g.loop_closure_verifier = rflow.make_factory(
    #    SparseRegistrationVerifier, inlier_rmse_threshold=1.0e-1)

    scene_g.loop_closure_verifier = rflow.make_factory(
        SparseRegistrationVerifier, inlier_rmse_threshold=1.0e-2, match_ratio_threshold=5e-1)

    # scene_g.loop_closure_verifier = rflow.make_factory(
    #    RegistrationVerifier, match_ratio_threshold=6e-1, covariance_max_threshold=1e-04,
    #    residual_threshhold=1e-4)
    scene_g.loop_closure = LoopClosure(
        rflow.FSResource(base_path / "loop-closure.pkl", make_dirs=True))
    with scene_g.loop_closure as args:
        args.pose_graph = scene_g.submaps
        args.registration = g.global_method
        args.verifier = scene_g.loop_closure_verifier

    with scene_g.prefix("loop_closure_") as sub:
        sub.view = ViewPoseGraph()
        with sub.view as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.loop_closure
            args.title = f"{scene_g.name} - Loop closure"
            args.traverse_nodes = [18, 6]
            args.invert_y = True

        sub.metrics = PoseGraphTransformationMetrics()
        with sub.metrics as args:
            args.gt_pose_graph = gt_pose_graph
            args.pred_pose_graph = scene_g.loop_closure
            args.loop_closure_eval = True

        sub.test = TestRegistration()
        with sub.test as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.submaps
            args.gt_pose_graph = gt_pose_graph
            args.nodes = [1, 2, 3]
            args.registration = g.global_method
            args.verifier = scene_g.loop_closure_verifier
            args.device = device
            args.ignore_edge_transform = True

    ##
    # Pose graph optimization after loop closure
    scene_g.opt1 = OptimizePoseGraph(rflow.FSResource(
        base_path / "opt1-pose-graph.pkl", make_dirs=True))
    with scene_g.opt1 as args:
        args.pose_graph = scene_g.loop_closure

    with scene_g.prefix("opt1_") as sub:
        sub.view = ViewPoseGraph()
        with sub.view as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.opt1
            args.invert_y = True
            args.title = f"{scene_g.name} - Opt1"

        sub.view_traj = ViewPoseGraphTrajectory()
        with sub.view_traj as args:
            args.pose_graph1 = gt_pose_graph
            args.pose_graph2 = scene_g.opt1
            args.title = f"{scene_g.name} - Opt1"

        sub.metrics = PoseGraphTrajectoryMetrics()
        with sub.metrics as args:
            args.gt_pose_graph = gt_pose_graph
            args.pred_pose_graph = scene_g.opt1
    ##
    # Local registration method

    scene_g.local = RegisterSubmaps(
        rflow.FSResource(base_path / "local-registration.pkl", make_dirs=True))
    with scene_g.local as args:
        args.submaps = scene_g.submaps
        args.pose_graph = scene_g.opt1
        args.registration = g.local_method
        args.device = device
        args.trajectory_only = True
    with scene_g.prefix("local_") as sub:
        sub.view = ViewPoseGraph()
        with sub.view as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.local
            args.invert_y = True
            args.title = f"{scene_g.name} - Local"

        sub.view_traj = ViewPoseGraphTrajectory()
        with sub.view_traj as args:
            args.pose_graph1 = gt_pose_graph
            args.pose_graph2 = scene_g.local
            args.title = f"{scene_g.name} - Local"

        sub.metrics = PoseGraphTrajectoryMetrics()
        with sub.metrics as args:
            args.gt_pose_graph = gt_pose_graph
            args.pred_pose_graph = scene_g.local

        sub.test = TestRegistration()
        with sub.test as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.opt1
            args.gt_pose_graph = gt_pose_graph
            args.nodes = [3, 4]
            args.registration = g.local_method
            args.device = device

        sub.merge = MergeSubmaps(rflow.FSResource(
            base_path / "merged-local.ply"))
        with sub.merge as args:
            args.submaps = scene_g.submaps
            args.pose_graph = scene_g.local
            args.voxel_size = 0.01

        sub.merge_view = ViewPCL()
        with sub.merge_view as args:
            args.pcl = sub.merge
            args.screenshort_prefix = f"{scene_id}-{scene_g.name}-"
            args.title = f"{scene_id} - {scene_g.name}"

    return scene_g.local_merge


def _build_experiments_graph(g, device):
    import torch
    from slamfeat.metrics.nodes import create_evaluation_graph
    from slamfeat.registration.nodes import (OdometryFactory, OdometryMethod)
    from slamfeat.colorfeature import color_feature_factory, ColorMode

    ###
    # Setup the Odometry method
    g.odometry = OdometryFactory(show=False)
    with g.odometry as args:
        args.method = OdometryMethod.MULTISCALE_ICP_FAST
    g.odo_model = color_feature_factory(
        color_type=ColorMode.GRAY, blur=False, device=device)

    ###
    # Setup the experiments
    nn_g = rflow.open_graph("../../data/scenenn", "scenenn")
    il_g = rflow.open_graph("../../data/ilrgbd", "ilrgbd")

    gt_initial_matrices = {
        '045': torch.tensor([[0.9979, -0.0162, -0.0634,  0.0216],
                             [0.0203,  0.9977,  0.0641, -0.0454],
                             [0.0622, -0.0653,  0.9959,  0.0514],
                             [0.0000,  0.0000,  0.0000,  1.0000]])
        @ torch.tensor([[1.0000,  0.0000,  0.0000,  2.0000],
                        [0.0000,  1.0000,  0.0000,  1.9800],
                        [0.0000,  0.0000,  1.0000, -0.3400],
                        [0.0000,  0.0000,  0.0000,  1.0000]]),
        '087': torch.tensor([[0.9979, -0.0233, -0.0605,  0.1438],
                             [0.0219,  0.9995, -0.0225, -0.0175],
                             [0.0610,  0.0211,  0.9979, -0.1257],
                             [0.0000,  0.0000,  0.0000,  1.0000]])
        @ torch.tensor([[1.0000,  0.0000,  0.0000,  1.9500],
                        [0.0000,  0.9962,  0.0872,  1.8400],
                        [0.0000, -0.0872,  0.9962, -0.3800],
                        [0.0000,  0.0000,  0.0000,  1.0000]]),
        'bedroom': torch.tensor([[0.8697, -0.4105, -0.2739, -0.3860],
                                 [0.4341,  0.9004, 0.0288,  0.6583],
                                 [0.2348, -0.1440, 0.9613,  0.3100],
                                 [0.0000,  0.0000,  0.0000,  1.0000]])
        @ torch.tensor([[0.1867,  0.2667, -0.9455, -0.4400],
                        [0.9806,  0.0080,  0.1959,  0.3100],
                        [0.0599, -0.9637, -0.2600, -0.8000],
                        [0.0000,  0.0000,  0.0000,  1.0000]])
    }

    for scene_id, dataset_graph, max_frames in [
            ("021", nn_g, 4500),
            ("030", nn_g, 10000),
            ("032", nn_g, 10000),
            ("045", nn_g, 4500),
            ("081", nn_g, 4500),
            ("087", nn_g, 4500),
            ("bedroom", il_g, 4500),
            ("boardroom", il_g, 4500),
            ("lobby", il_g, 4500),
            ("loft", il_g, 4500),
            ("test", il_g, 1000),
            ("large", nn_g, 10000)]:
        is_scenenn = dataset_graph is nn_g

        prefix = scene_id
        if prefix == "test":
            scene_id = "bedroom"
        elif prefix == "large":
            scene_id = "081"

        dataset_node = dataset_graph[f"{scene_id}.dataset"]
        gt_pcl = dataset_graph[f"{scene_id}.gt_pcl"]
        gt_mesh = dataset_graph[f"{scene_id}.gt_mesh"] if is_scenenn else None

        eval_init_align = gt_initial_matrices.get(scene_id, None)
        if eval_init_align is not None:
            eval_init_align = eval_init_align.tolist()

        with g.prefix(f"{prefix}.") as scene_sub_graph:
            result_node = _build_scene_reconstruction_graph(
                g,  scene_sub_graph, scene_id, dataset_node,
                device, max_frames=max_frames)
            base_path = Path(scene_sub_graph.name) / scene_id
            with scene_sub_graph.prefix("eval_") as eval_sub:
                create_evaluation_graph(
                    eval_sub, result_node, gt_mesh, gt_pcl,  base_path / "eval",
                    init_matrix=eval_init_align)


@rflow.graph()
def colored_icp(g):
    """Workflow using a Open3D/Redwood based pipeline"""
    from slamfeat.registration.open3d import (FPFHSubmapFeatures,
                                              ransac_registration_factory)

    g.model = None
    g.sparse_feature = None
    g.global_method = rflow.make_factory(ransac_registration_factory,
                                         feature_proc=FPFHSubmapFeatures(
                                             _VOXEL_SIZE),
                                         voxel_size=_VOXEL_SIZE)

    g.local_method = rflow.open_graph("..", "pairwise_algorithms").colored_icp
    _build_experiments_graph(
        g, "cpu:0")


def _make_feature_exp(g, lc_use_dense_feats=True):
    from slamtb.feature import SIFT
    from slamfeat.registration.open3d import (SparseSubmapFeatures,
                                              ransac_registration_factory)

    g.sparse_feature = rflow.make_factory(SIFT)
    g.global_method = rflow.make_factory(ransac_registration_factory,
                                         voxel_size=_VOXEL_SIZE,
                                         feature_proc=SparseSubmapFeatures(
                                             0, 10, use_dense_features=lc_use_dense_feats))
    g.local_method = rflow.open_graph(
        "..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g,  "cuda:0")


@rflow.graph()
def geom(g):
    """Workflow using RGB as the features"""
    from slamtb.feature import SIFT
    from slamfeat.registration.open3d import (SparseSubmapFeatures,
                                              ransac_registration_factory)

    g.model = None
    g.sparse_feature = rflow.make_factory(SIFT)
    g.global_method = rflow.make_factory(
        ransac_registration_factory,
        voxel_size=_VOXEL_SIZE,
        feature_proc=SparseSubmapFeatures(
            0, 10, use_dense_features=False))
    g.local_method = rflow.open_graph(
        "..", "pairwise_algorithms").deep_feature_icp
    _build_experiments_graph(g,  "cuda:0")


@rflow.graph()
def gray(g):
    """Workflow using RGB as the features"""
    from slamfeat.colorfeature import color_feature_factory, ColorMode
    g.model = color_feature_factory(
        color_type=ColorMode.GRAY, blur=False, device="cpu:0")

    _make_feature_exp(g, lc_use_dense_feats=False)


@rflow.graph()
def rgb(g):
    """Workflow using RGB as the features"""
    from slamfeat.colorfeature import color_feature_factory, ColorMode
    g.model = color_feature_factory(
        color_type=ColorMode.RGB, blur=False, device="cpu:0")

    _make_feature_exp(g, lc_use_dense_feats=False)


@rflow.graph()
def rnd_random_3(g):
    """Workflow using RGB as the features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_syn_3").train
    _make_feature_exp(g, lc_use_dense_feats=True)


@rflow.graph()
def rnd_syn_3(g):
    """Workflow using the features from the Resnet Dilated trained with
    synthetic dataset and 3 channel features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_syn_3").train
    _make_feature_exp(g, lc_use_dense_feats=True)


@rflow.graph()
def rnd_real_3(g):
    """Workflow using the features from the Resnet Dilated trained with
    synthetic dataset and 3 channel features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_3").train
    _make_feature_exp(g, lc_use_dense_feats=True)


@rflow.graph()
def rnd_syn_8(g):
    """Workflow using the features from the Resnet Dilated trained with
    synthetic dataset and 3 channel features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_syn_8").train
    _make_feature_exp(g, lc_use_dense_feats=True)


@rflow.graph()
def rnd_real_8(g):
    """Workflow using the features from the Resnet Dilated trained with
    synthetic dataset and 8 channel features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_8").train
    _make_feature_exp(g, lc_use_dense_feats=True)


@rflow.graph()
def rnd_real_16(g):
    """Workflow using the features from the Resnet Dilated trained with
    synthetic dataset and 3 channel features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_16").train
    _make_feature_exp(g, lc_use_dense_feats=True)


@rflow.graph()
def rnd_real_32(g):
    """Workflow using the features from the Resnet Dilated trained with
    synthetic dataset and 3 channel features"""

    g.model = rflow.open_graph("../../rgb2rgb", "rnd_real_32").train
    _make_feature_exp(g, lc_use_dense_feats=True)
