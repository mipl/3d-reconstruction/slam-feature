"""Workflow for running common feature matching experiments with common features.
"""

import cyvlfeat
import cv2
import numpy as np
import torch
from scipy.spatial.distance import euclidean
import rflow

##################
# SIFT descriptor


class SIFTExtractor:
    """Dense sift feature extractor.
    """
    @staticmethod
    def extract_features(rgb_image, _):
        """Extract features from a patch
        """

        rgb_image = cv2.cvtColor(
            rgb_image.numpy(), cv2.COLOR_RGB2GRAY).astype(np.float)
        dsift = cyvlfeat.sift.dsift(rgb_image, float_descriptors=True,
                                    step=1,
                                    norm=False)[1]

        height, width = rgb_image.shape[:2]
        featuremap = torch.zeros(
            (128, height, width), dtype=torch.float32)

        featuremap[:, 5:-4, 5:-4] = (torch.from_numpy(dsift)
                                     .reshape(height-9, width-9, 128)
                                     .permute(2, 0, 1))

        return featuremap

    @staticmethod
    def distance(feat1, feat2):
        """Measure two features.
        """
        return np.linalg.norm(feat1 - feat2, axis=1).min()


#######################################
# Grayscale Intensity based descriptor

class IntensityFeature:
    """Extract features using color processing.
    """

    @staticmethod
    def extract_features(rgb_image, _):
        """Extract feature for the given patch.
        """
        rgb_image = cv2.cvtColor(
            rgb_image.numpy(), cv2.COLOR_RGB2GRAY).astype(np.float) / 255.0
        return torch.from_numpy(rgb_image.reshape(1, *rgb_image.shape))

    @staticmethod
    def distance(feat1, feat2):
        """Compute the distance between features.
        """
        return np.sqrt(np.power(feat1 - feat2, 2).sum())


################
# Dataset stuff


@rflow.graph()
def data(g):
    """Graph for creating the data.
    """
    from slamfeat.viz.nodes import ViewCorrespondenceDataset
    from slamfeat.data.nodes import CreateCorrespondenceDataset

    scenenn_g = rflow.open_graph("../../data/scenenn", "scenenn")
    tumrgbd_g = rflow.open_graph("../../data/tumrgbd", "tumrgbd")

    g.test_dataset = CreateCorrespondenceDataset(show=True)
    with g.test_dataset as args:
        args.ds1 = scenenn_g['016.dataset']
        args.ds1_vis_graph = scenenn_g['016.visibility_graph']
        args.point_dist_thresh = 0.003

    g.view_test_dataset = ViewCorrespondenceDataset()
    with g.view_test_dataset as args:
        args.dataset = g.test_dataset


@rflow.graph()
def handcraft(g):
    """Handcraft feature tests.
    """
    from slamfeat.metrics.nodes import (EvaluateFeatureMatching,
                                        EvaluateFeatureDistance)

    data_g = rflow.open_graph(".", "data")

    g.sift = rflow.make_factory(SIFTExtractor)

    g.sift_eval = EvaluateFeatureMatching(rflow.FSResource("sift-metrics.jpg"))
    with g.sift_eval as args:
        args.test_dataset = data_g.test_dataset
        args.feature_extractor = g.sift
        args.alg_name = "SIFT"

    g.intensity = rflow.make_factory(IntensityFeature)
    g.intensity_eval = EvaluateFeatureMatching(
        rflow.FSResource("intensity-metrics.jpg"))
    with g.intensity_eval as args:
        args.test_dataset = data_g.test_dataset
        args.feature_extractor = g.intensity
        args.alg_name = "Intensity"

    g.intensity_dist_eval = EvaluateFeatureDistance(
        rflow.FSResource("intensity-dist-metrics.jpg"))
    with g.intensity_dist_eval as args:
        args.test_dataset = data_g.test_dataset
        args.feature_extractor = g.intensity
        args.alg_name = "Intensity"
